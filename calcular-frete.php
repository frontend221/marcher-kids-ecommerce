<?php
    include ("config/database.php");
    include ("funcao.php");

    session_start();
    $id_sessao = session_id();

    $carrinho =  mysqli_query($conn, "SELECT * FROM `ped-carrinho` WHERE `id_sessao`='".$id_sessao."' AND `status`='S' AND `deleted_at` IS NULL");
    $total_produtos = mysqli_num_rows($carrinho);

    $altura = 0;
    $largura = 0;
    $profundidade = 0;
    $peso = 0;
    $valor = 0;

    while($ln = mysqli_fetch_array($carrinho)) {
        $idProduto = $ln['id_produto'];
        $quantidade = $ln['quantidade'];
        $consultaProduto = mysqli_query($conn, "SELECT p.id,
                                                p.altura,
                                                p.largura,
                                                p.profundidade,
                                                p.peso, 
                                                p.valor
                                        FROM `prod-item` p
                                        WHERE p.id = {$idProduto}");
        if($consultaProduto === FALSE) { 
            die(mysqli_error($conn));
        }
        $resConsultaProduto = mysqli_fetch_assoc($consultaProduto);
        $altura += ($resConsultaProduto['altura'] * $quantidade);
        $largura += ($resConsultaProduto['largura'] * $quantidade);
        $valor = ($resConsultaProduto['valor'] * $quantidade);
        $profundidade += ($resConsultaProduto['profundidade'] * $quantidade);
        $peso += ($resConsultaProduto['peso'] * $quantidade);                                
    }    

    $_SESSION['carrinho']['cepSelecionado'] = $_POST['cep'];

    if ($valor < 20.5) {
        $valor = 20.5;
    }

    if ($valor > 3000) {
        $valor = 3000;
    }

    // $largura = max($arrayLargura);
    // $profundidade = max($arrayProfundidade);

    // if ($peso < 1):
    //     $formato = 3;
    //     $altura = 0;
    // else:
    //     $formato = 1;
    // endif;

    $cep_origem = '87080185';     // Seu CEP , ou CEP da Loja
    $cep_destino = $_POST['cep']; // CEP do cliente, que irá vim via POST
    $tipo_do_frete =  $_POST['nCdServico']; //Sedex: 40010   |  Pac: 41106



    $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
    $url .= "nCdEmpresa="; //'17372860';
    $url .= "&sDsSenha=";   //'11064491';
    $url .= "&sCepOrigem=" . $cep_origem;
    $url .= "&sCepDestino=" . $cep_destino;
    $url .= "&nVlPeso=" . 2;
    $url .= "&nVlLargura=" . $largura;
    $url .= "&nVlAltura=" . $altura;
    $url .= "&nCdFormato=1";
    $url .= "&nVlComprimento=" . $profundidade;
    $url .= "&sCdMaoProria=n";
    $url .= "&nVlValorDeclarado=" . $valor;
    $url .= "&sCdAvisoRecebimento=n";
    $url .= "&nCdServico=" . $tipo_do_frete;
    $url .= "&nVlDiametro=0";
    $url .= "&StrRetorno=xml";


    $xml = simplexml_load_file($url);

    $frete =  $xml->cServico;

    // echo "<h1>Valor PAC: R$ ".$frete->Valor."<br />Prazo: ".$frete->PrazoEntrega." dias</h1>";
  
?>

<?php foreach ($frete as $obj) { ?>
    <?php 
        
        $valor = $obj->Valor;
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);
        $valorFloat = floatval($valor);
        
    ?>
    <div class="row justify-content-center">
        <div class="col-12 col-sm-10 col-md-6">
            <div class="card">
                <table style="width: 100%">
                    <tr>
                        <td align="center" width="10%">
                            <?php if ($obj->Codigo == 4510): ?>
                                <input type="radio" name="opcaofrete" value="PAC" onclick='selecionaFrete(this.value, <?php echo number_format($valorFloat, 2) ?>)'/>
                            <?php elseif ($obj->Codigo == 4014): ?>
                                <input type="radio" name="opcaofrete" value="SEDEX" onclick='selecionaFrete(this.value, <?php echo number_format($valorFloat, 2) ?>)'/>
                            <?php endif ?>    
                        </td>

                        <td align="center" width="20%">
                            <?php if ($obj->Codigo == 4510): ?>
                                PAC
                            <?php elseif ($obj->Codigo == 4014): ?>
                                SEDEX
                            <?php endif ?>
                        </td>
                        <td align="center" width="30%"><?php echo $obj->PrazoEntrega . ' dia(s)'; ?></td>
                        <td align="center" width="40%"><?php echo 'R$ ' . $obj->Valor ; ?></td>
                    </tr>
                </table>
            </div>
        <div class="col-12 col-md-6">
    </div>
<?php } ?>
