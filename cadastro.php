<?php 
	ob_start();

	if(session_status() == PHP_SESSION_NONE){
		session_start([
			"cookie_lifetime" => 3600,
			"read_and_close"  => true,
		]);
		session_name("MARCHERECOMMERCE");
	}
	
	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");
	
	if(file_exists("config.php")) {
		require_once ("config.php");
	} else {
		die("Erro: arquivo de setup não encontrado.");
	}

	if($exp[1] && $_SESSION["usuario_token"]) {

		$sql = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `id`='".$exp[1]."'");

		$dados = mysqli_fetch_array($sql);

	}
?>

<section id="cadastro" class="my-5">
	<div class="container">
		<div class="row">
			<div class="form">
				<h4>Cadastre-se</h4>
				<form action="<?php echo PATH ?>/funcao.php" method="post">
					
					<input type="hidden" name="action" value="cadastro" required>
					
					<?php
						if($exp[1]) {
					?>	
						<input type="hidden" name="_METHOD" value="<?php echo $exp[1]?>"/>
					<?php } ?>
					
					<div class="form-group text-center">
						<ul>
							<li><input type="radio" name="tipopessoa" id="pessoafisica" value="PF" checked> Pessoa Física</li>
							<li><input type="radio" name="tipopessoa" id="pessoajuridica" value="PJ" <?=($exp[1] && $dados['tipopessoa'] == 'PJ' ? "checked" : "");?>> Pessoa Jurídica</li>
						</ul>
					</div>
					
					<div style="margin-bottom: 20px;">
						<span style="font-weight: 900; color: #E71B1F;">*</span> <span style="font-weight: 400; text-transform: uppercase; font-size: 13px;">Campos com preenchimento obrigatório</span>
					</div>
					
					<div id="inputpessoafisica">
						<div class="row">
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="razaosocial1">Nome <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="razaosocial1" name="razaosocial1" value="<?=($exp[1] ? $dados['razaosocial'] : "");?>">
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="nomefantasia1">Sobrenome <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="nomefantasia1" name="nomefantasia1" value="<?=($exp[1] ? $dados['nomefantasia'] : "");?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="cpf">CPF <span style="font-weight: 900; color: #E71B1F;">*</span></label>
							<input type="text" class="form-control" id="cpf" maxlength="14" name="cpf" value="<?=($exp[1] ? $dados['cpfcnpj'] : "");?>">
						</div>
					</div>
										
					<div id="inputpessoajuridica" style="display: none;">
						<div class="row">
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="razaosocial">Razão Social <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="razaosocial" name="razaosocial" value="<?=($exp[1] ? $dados['razaosocial'] : "");?>">
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="nomefantasia">Nome Fantasia <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="nomefantasia" name="nomefantasia" value="<?=($exp[1] ? $dados['nomefantasia'] : "");?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="cnpj">CNPJ <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="cnpj" maxlength="18" name="cnpj" value="<?=($exp[1] ? $dados['cpfcnpj'] : "");?>">
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="inscricaoestadual">Inscrição Estadual</label>
									<input type="text" class="form-control" id="inscricaoestadual" name="inscricaoestadual" value="<?=($exp[1] ? $dados['inscricaoestadual'] : "");?>">
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="email">E-mail <span style="font-weight: 900; color: #E71B1F;">*</span></label>
						<input type="email" class="form-control" id="email" name="email" required value="<?=($exp[1] ? $dados['email'] : "");?>">
					</div>
					<div class="row">
						<div class="col-md-3 col-lg-3 col-12">
							<div class="form-group">
								<label for="telefone">Telefone <span style="font-weight: 900; color: #E71B1F;">*</span></label>
								<input type="tel" class="form-control" id="telefone" name="telefone" required maxlength="14" value="<?=($exp[1] ? $dados['telefone'] : "");?>">
							</div>
						</div>
						<div class="col-md-3 col-lg-3 col-12">
							<div class="form-group">
								<label for="celular">Celular</label>
								<input type="tel" class="form-control" id="celular" name="celular" maxlength="15" value="<?=($exp[1] ? $dados['celular'] : "");?>">
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-12">
							<div class="form-group">
								<label for="pessoacontato">Pessoa de Contato</label>
								<input type="text" class="form-control" id="pessoacontato" name="pessoacontato" value="<?=($exp[1] ? $dados['pessoacontato'] : "");?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-6 col-12">
							<div class="form-group">
								<label for="senha1">Senha <span style="font-weight: 900; color: #E71B1F;">*</span></label>
								<input type="password" class="form-control" id="senha1" name="senha1" <?=($exp[1] ? "" : "required");?>>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-12">
							<div class="form-group">
								<label for="senha2">Confirmação de Senha <span style="font-weight: 900; color: #E71B1F;">*</span></label>
								<input type="password" class="form-control" id="senha2" name="senha2" <?=($exp[1] ? "" : "required");?>>
							</div>
						</div>
					</div>
					<?php
						if($exp[1] == "") {
					?>	
						<p>Já possui um usuário e senha? <a href="<?php echo PATH ?>/login">Entrar</a></p>
					<?php } ?>	
					<button type="submit" name="enviar" class="btn btn-primary"><?=($exp[1] ? "Editar" : "Cadastrar");?></button>
				</form>
			</div>
		</div>
	</div>
</section>
