<?php  
	if($exp[1]) {

		$sql = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `id`='".$exp[1]."'");

		$endereco = mysqli_fetch_array($sql);
	}
?>

<section id="cadastro" class="my-5">
	<div class="container">
		<div class="row">
			<div class="form">
				<h4>Cadastrar Endereço</h4>
				<form action="<?php echo PATH ?>/funcao.php" method="POST">

					<input type="hidden" name="action" value="adicionar-endereco" required>	
					
					<?php
						if($exp[1]) {
					?>	
						<input type="hidden" name="_METHOD" value="<?php echo $exp[1]?>"/>
					<?php } ?>

					<div style="margin-bottom: 20px;">
						<span style="font-weight: 900; color: #E71B1F;">*</span> <span style="font-weight: 400; text-transform: uppercase; font-size: 13px;">Campos com preenchimento obrigatório</span>
					</div>
															
					<div>
						<div class="row">
							<div class="col-md-6 col-lg-3 col-12">
								<div class="form-group">
									<label for="cep">CEP <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="cep" name="cep" value="<?=($exp[1] ? $endereco['cep'] : "");?>" onchange="buscarCep($(this).val())" required>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-12">
								<div class="form-group">
									<label for="logradouro">Logradouro <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="text" class="form-control" id="logradouro" name="logradouro" value="<?=($exp[1] ? $endereco['logradouro'] : "");?>" required>
								</div>
							</div>
							<div class="col-md-6 col-lg-3 col-12">
								<div class="form-group">
									<label for="numero">Número <span style="font-weight: 900; color: #E71B1F;">*</span></label>
									<input type="number" class="form-control" id="numero" maxlength="6" name="numero" value="<?=($exp[1] ? $endereco['numero'] : "");?>" required>
								</div>
							</div>
							<div class="col-md-6 col-lg-12 col-12">
								<div class="form-group">
									<label for="complemento">Complemento</label>
									<input type="text" class="form-control" id="complemento" value="<?=($exp[1] ? $endereco['complemento'] : "");?>" name="complemento">
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-md-3 col-lg-5 col-12">
                                <div class="form-group">
                                    <label for="bairro">Bairro <span style="font-weight: 900; color: #E71B1F;">*</span></label>
                                    <input type="text" class="form-control" id="bairro" name="bairro" value="<?=($exp[1] ? $endereco['bairro'] : "");?>" required>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-4 col-12">
                                <div class="form-group">
                                    <label for="cidade">Cidade <span style="font-weight: 900; color: #E71B1F;">*</span></label>
                                    <input type="text" class="form-control" id="cidade" name="cidade" value="<?=($exp[1] ? $endereco['cidade'] : "");?>" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3 col-12">
                                <div class="form-group">
                                    <label for="estado">Estado <span style="font-weight: 900; color: #E71B1F;">*</span></label>
                                    <input type="text" class="form-control" id="estado" name="estado" value="<?=($exp[1] ? $endereco['estado'] : "");?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
							<ul>
								<li>Endereço Principal: </li>
								<li><input type="radio" name="enderecoprincipal" value="S" <?=($exp[1] && $endereco['principal'] == 'S' ? "checked" : "");?>> Sim</li>
								<li><input type="radio" name="enderecoprincipal" value="N"<?=($exp[1] && $endereco['principal'] == 'S' ? "" : "checked");?>> Não</li>
							</ul>
						</div>
					</div>
					<button type="submit" name="enviar" class="btn btn-primary">Cadastrar</button>
				</form>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	
	function buscarCep(cep) {
		$.get(`https://api.postmon.com.br/v1/cep/${cep}`, (retorno) => {
			const { bairro, cidade, logradouro, estado } = retorno;

			$("#logradouro").val(logradouro);
			$("#cidade").val(cidade);
			$("#estado").val(estado);
			$("#bairro").val(bairro);
		});
	}
	 
</script>
