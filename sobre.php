<section id="sobre">
	<div class="container">
		<?php 

		$texto = mysqli_query($conn, "SELECT `titulo`, `texto` FROM `txt-sobre-empresa` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `posicao` ASC"); 
		if(mysqli_num_rows($texto) > 0) {

		?>
		
		<?php while($row = mysqli_fetch_array($texto)) { ?>

		<div class="row titulo">
			<h1><?php echo $row["titulo"]; ?></h1>
		</div>
		<div class="row">
			<?php echo $row["texto"]; ?>
		</div>
			
		<?php } ?>
		
		<?php } else { ?>
		
		<div class="row">
			<div class="alert alert-secondary" style="width: 100%; border-radius: 0; border: 1px solid #fff; height: 64px;">
				<p style="text-align: center !important; vertical-align: middle !important; color: #333 !important; text-transform: uppercase; font-size: 15px; line-height: 38px;">Não há conteúdo publicado para está seção do website.</p>
			</div>
		</div>
			
		<?php } ?>
		
	</div>
</section>