<section id="recuperar-senha">
	<div class="container">
		<div class="row">
			<div class="form">
				<h4>Recuperação de senha</h4>
				<form action="" method="post">
					<div class="form-group">
						<label for="usuario">E-mail, CPF ou CNPJ</label>
						<input type="text" class="form-control" id="usuario" name="usuario" required>
					</div>
					<p>Já possui um usuário e senha? <a href="<?php echo PATH ?>/login">Entrar</a></p>
					<button type="submit" class="btn btn-primary">Recuperar senha</button>

					<div class="divider">
						<h2>Deseja se cadastrar?</h2>
					</div>
					<p class="link">
						<a href="<?php echo PATH ?>/cadastro" class="btn btn-secondary">Cadastrar</a>
					</p>
				</form>
			</div>
		</div>
	</div>
</section>

<section id="login-social">
	<div class="container">
		<h4>Conectar com:</h4>
		<ul>
			<li class="facebook"><a href="#"><i class="fab fa-facebook-f fa-fw"></i> Facebook</a></li>
			<li class="gmail"><a href="#"><i class="fab fa-google fa-fw"></i> Gmail</a></li>
		</ul>
	</div>	
</section>