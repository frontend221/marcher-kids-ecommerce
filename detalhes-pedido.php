<?php

$id_sessao = session_id();

?>

<section id="carrinho">
	<div class="container">
	
		<?php
            $pedido =  mysqli_query($conn, "SELECT * FROM `ped-item` WHERE `pedido`='$exp[1]' AND `status`='S' AND `deleted_at` IS NULL");
            $total_produtos = mysqli_num_rows($pedido);
		?>
		
		<div class="row">
			<span class="count-carrinho">Você tem <strong><?php echo $total_produtos; ?> produto(s)</strong> no pedido</span>
		</div>
		
		<div class="row">
			<div class="table-responsive info-carrinho">
				<table class="table table-condensed">
					<thead>
						<tr class="table-header">
							<td class="imagem" width="10%"></td>
							<td class="descricao" width="40%">Produto</td>
							<td class="valor" width="10%">Valor Unitário</td>
							<td class="quantidade" width="15%">Quantidade</td>
							<td class="valor" width="15%">Total</td>
							<td width="10%"></td>
						</tr>
					</thead>
					<tbody>
					
					<?php $valor_total = 0.00; ?>	
						
					<?php if($total_produtos > 0) { ?>
							
						<?php while($row = mysqli_fetch_array($pedido)) { ?>
						
						<?php $valor_total += $row["valor_unitario"]; ?>
						
						<tr>
							<td class="carrinho-produto" style="text-align: center;">
							<?php
                            $produtos = mysqli_query($conn, "SELECT * FROM `prod-item` WHERE `id`='".$row["produto"]."' ");
                            $produto = mysqli_fetch_array($produtos);	

							$imagem = mysqli_query($conn, "SELECT * FROM `prod-imagem` WHERE `produto`='".$row["produto"]."' AND `status`='S' AND `deleted_at` IS NULL ORDER BY `id` ASC");
							if(mysqli_num_rows($imagem) > 0) {
							$img = mysqli_fetch_array($imagem);	
							?>
								<img src="<?php echo IMAGE ?>/produto/<?php echo $img["imagem"]; ?>" alt="<?php echo $produto["nome"]; ?>" class="img-fluid">
							<?php } else { ?>
								<img src="<?php echo IMAGE ?>/produto/produto.jpg" alt="" class="img-fluid">
							<?php } ?> 
							</td>
							<td class="carrinho-descricao">
                                <a href="<?php echo PATH ?>/produto/<?php echo $produto["id"]; ?>/<?php echo slugit($produto["nome"]); ?>/<?php echo base64_encode($produto["referencia"]); ?>">
									<h4><?php echo $produto["nome"]; ?></h4>
									<p><strong>Ref.:</strong> <span><?php echo $produto["referencia"]; ?></span></p>
								</a>
							</td>
							<td class="carrinho-preco">
								<p>R$ <?php echo number_format($row["valor_unitario"], 2, ",", "."); ?></p>
							</td>
							<td class="carrinho-qtde">
                                <p><?php echo $row["quantidade"]; ?></p>
							</td>
							<td class="carrinho-preco">
								<p>R$ <?php echo number_format(($row["valor_unitario"] * $row["quantidade"]), 2, ",", "."); ?></p>
							</td>
						</tr>
								
						<?php } ?>
					<?php } ?>
						
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">

							</td>
						</tr>
					</tfoot>
				</table>
			</div>
            <div class="col-lg-12">
						
			</div>
		</div>
	</div>
</section>
