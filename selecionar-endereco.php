<?php
    include ("funcao.php");
    session_start();
    $_SESSION['endereco'] = $_POST['endereco'];
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title" style="text-align: center">SELECIONADO</h4>
        <h4 class="card-title" style="text-align: center"><?php echo $_SESSION['endereco']['logradouro']?>, <?php echo $_SESSION['endereco']['numero']?></h4>
        <ul style="list-style: circle">
            <li><strong>CEP: </strong><?php echo $_SESSION['endereco']['cep'] ?></li>
            <li><strong>Cidade: </strong><?php echo $_SESSION['endereco']['cidade'] ?>, <?php echo $_SESSION['endereco']['estado'] ?></li>
            <li><strong>Endereço Principal: </strong><?php echo $_SESSION['endereco']['principal'] ?></li>
            <li><strong>Bairro: </strong><?php echo $_SESSION['endereco']['bairro'] ?></li>
            <li><strong>Complemento: </strong><?php echo $_SESSION['endereco']['complemento'] ?></li>
        </ul>
    </div>
</div>    