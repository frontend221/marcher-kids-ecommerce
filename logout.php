<?php

	ob_start();

	if(session_status() == PHP_SESSION_NONE){
		session_start([
			"cookie_lifetime" => 3600,
			"read_and_close"  => true,
		]);
		session_name("MARCHERECOMMERCE");
	}

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	$id = NULL;
	$id = (!empty($_GET["id"]) ? $_GET["id"] : $_SESSION["id"]);

	require_once("config/path.php");
	require_once("config/device.php");
	require_once("config/database.php");

	$sql = mysqli_query($conn, "UPDATE `cli-cadastro` SET `token_acesso`='' WHERE `id`='".$id."' AND `updated_at`=NOW()");

	if($sql) {

		$query = mysqli_query($conn, "INSERT INTO `log-acesso-cms`(`id`, `usuario`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','".utf8_encode('Encerrou a sessão na loja virtual.')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
		
	} else {

		header("Location: " . PATH . "/minha-conta");

	}

	session_unset();
	session_destroy();

	header("Location: " . PATH . "/login/logout");

?>