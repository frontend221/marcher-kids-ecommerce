<style>
    .loader {
      border: 4px solid #f3f3f3; /* Light grey */
      border-top: 4px solid #3498db; /* Blue */
      border-radius: 50%;
      width: 30px;
      height: 30px;
      animation: spin 2s linear infinite;
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>

 <?php
    session_start();
    $id_sessao = session_id();
    $id_usuario = mysqli_real_escape_string($conn, trim($_SESSION["usuario_id"]));
	
    $sql = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `id` = $id_usuario");
    $row = mysqli_fetch_array($sql);

	$carrinho =  mysqli_query($conn, "SELECT * FROM `ped-carrinho` WHERE `id_sessao`='".$id_sessao."' AND `status`='S' AND `deleted_at` IS NULL");
    $total_produtos = mysqli_num_rows($carrinho);

    $sqlEndereco = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente`='".$id_usuario."' AND `status`='S' AND `principal`='S' AND `deleted_at` IS NULL");
    $endereco = mysqli_fetch_array($sqlEndereco);
    
    $carrinho2 =  mysqli_query($conn, "SELECT * FROM `ped-carrinho` WHERE `id_sessao`='".$id_sessao."' AND `status`='S' AND `deleted_at` IS NULL");
    
    $_SESSION['frete'] = 0.00;
    $_SESSION['endereco'] = null;
  ?>

<section id="checkout">
	<div class="col">
		<div class="containerCheckout">
    
      <div class="row justify-content-center">
      
        <div class="col-12 col-md-8 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Seu carrinho</span>
            <span class="badge badge-secondary badge-pill"><?php echo $total_produtos; ?></span>
          </h4>
          <ul class="list-group mb-3">
            <?php $valor_total = 0.00; ?>
            <?php while($ln = mysqli_fetch_array($carrinho)) { ?>
            <?php $valor_total += ($ln["valor_produto"] * $ln["quantidade"]); ?>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0"><?php echo $ln["nome_produto"]; ?></h6>
                <small class="text-muted">Ref.:<?php echo $ln["ref_produto"]; ?></small>
                <small class="text-muted">Qtde. <?php echo $ln["quantidade"]; ?></small>
              </div>
              <span class="text-muted">R$ <?php echo number_format(($ln["valor_produto"] * $ln["quantidade"]), 2, ",", "."); ?></span>
            </li>
            <?php } ?>
            <div class="freteSelecionado">
              <li class="list-group-item d-flex justify-content-between">
                <span>Frete</span>
                <?php if ($_SESSION['frete'] == null) { ?>
                  <strong> SELECIONE O FRETE</strong>
                <?php } else { ?>
                  <strong>R$ <?php echo number_format(($_SESSION['frete']), 2, ",", "."); ?></strong>
                  <?php } ?>  
              </li>
            </div>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total</span>
              <strong>R$ <?php echo number_format(($valor_total), 2, ",", "."); ?></strong>
            </li>
          </ul>
          
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Calcule o Frete:</span>
          </h4>
          <div class="row align-items-center justify-content-center my-3">
            <div class="col-12 col-md-6">
              <label for="cep">CEP <span class="text-muted"></span></label>
              <input type="text" class="form-control" id="cep" placeholder="00000-000" name="cep" value="<?php echo $endereco['cep']?>"> 
            </div>
          </div>
          <div class="row align-items-center justify-content-center my-3">
            <div class="col-12 col-md-6">
              <button type="button" class="btn btn-primary btn-lg btn-block" onclick="calcularFrete($('#cep').val())">CALCULAR</button>
            </div>
          </div>
          <div class="retornoFretePac"></div>
          <div id="pac"style="display:none;">
            <div>Calculando PAC</div>
            <div class="loader"></div>
          </div>
          <div class="retornoFreteSedex"></div>
          <div id="sedex" style="display:none;">
            <div>Calculando Sedex</div>
            <div class="loader"></div>
          </div>
          
        </div>

        <div class="col-12 col-md-8 order-md-1 my-5">
			<div class="card">
                <div class="card-body">
					<h3 class="mb-3">Endere&ccedil;o de cobran&ccedil;a</h3>
					<form class="needs-validation" novalidate>
						<div class="row">
							<div class="mb-3 col-12 col-md-5">
								<label for="firstNameCobranca">Nome Completo</label>
								<input type="text" class="form-control" id="firstNameCobranca" placeholder="" value="<?php echo $row['razaosocial']?> <?php echo $row['nomefantasia']?>" required>
								<div class="invalid-feedback">
									Valid first name is required.
								</div>
							</div>

							<div class="mb-3 col-12 col-md-3">
								<label for="cpfCobranca">CPF/CNPJ <span class="text-muted"></span></label>
								<input type="text" class="form-control" id="cpfCobranca" placeholder="000.000.000-00 ou 00.000.000/0000-00" value="<?php echo $row['cpfcnpj']?>" required>
								<div class="invalid-feedback">
									Please enter a valid cpfcnpj.
								</div>
							</div>
							
							<div class="mb-3 col-12 col-md-4">
								<label for="emailCobranca">Email <span class="text-muted"></span></label>
								<input type="email" class="form-control" id="emailCobranca" placeholder="you@example.com" value="<?php echo $row['email']?>" required>
								<div class="invalid-feedback">
									Please enter a valid email address for shipping updates.
								</div>
							</div>

							<div class="mb-3 col-12 col-md-3">
								<label for="dataCobranca">Data nascimento <span class="text-muted"></span></label>
								<input type="text" class="form-control" id="dataCobranca" name="dataCobranca" onfocus="selecionarData()" value="" required>
								<div class="invalid-feedback">
									Informe a data de nascimento
								</div>
							</div>

							<div class="mb-3 col-12 col-md-6">
								<label for="addressCobranca">Endere&ccedil;o</label>
								<input type="text" class="form-control" id="addressCobranca" placeholder="Exemplo. Rua Brasil" value="<?php echo $endereco['logradouro']?>" required>
								<div class="invalid-feedback">
									Please enter your shipping address.
								</div>
							</div>

							<div class="mb-3 col-12 col-md-3">
								<label for="addressNumeroCobranca">N&uacute;mero</label>
								<input type="text" class="form-control" id="addressNumeroCobranca" value="<?php echo $endereco['numero']?>" placeholder="Exemplo. 234" required>
								<div class="invalid-feedback">
									Please enter your shipping address number.
								</div>
							</div>

							<div class="mb-3 col-12">
								<label for="complementoCobranca">Complemento<span class="text-muted">(Opcional)</span></label>
								<input type="text" class="form-control" id="complementoCobranca" placeholder="Exemplo. Apartamento 11" value="<?php echo $endereco['complemento']?>">
							</div>

							<div class="col-12 col-md-3 mb-3">
								<label for="cidadeCobranca">Cidade<span class="text-muted"></span></label>
								<input type="text" class="form-control" id="cidadeCobranca" placeholder="Exemplo. São Paulo" value="<?php echo $endereco['cidade']?>" required>
							</div>

							<div class="col-12 col-md-3 mb-3">
								<label for="zipCobranca">CEP</label>
								<input type="text" class="form-control" id="zipCobranca" placeholder="" value="<?php echo $endereco['cep']?>" required>
								<div class="invalid-feedback">
									Zip code required.
								</div>
							</div>


							<div class="col-12 col-md-3 mb-3">
								<label for="countryCobranca">Pais</label>
								<select class="custom-select d-block w-100" id="countryCobranca" required>
								<option value="br" selected>Brasil</option>
								</select>
								<div class="invalid-feedback">
									Please select a valid country.
								</div>
							</div>
							<div class="col-12 col-md-3 mb-3">
								<label for="stateCobranca">Estado</label>
								<select class="custom-select d-block w-100" id="stateCobranca" required>
								<option value="AC" <?php echo $endereco['estado'] == 'AC'?'selected':'';?>>Acre</option>
								<option value="AL" <?php echo $endereco['estado'] == 'AL'?'selected':'';?>>Alagoas</option>
								<option value="AP" <?php echo $endereco['estado'] == 'AP'?'selected':'';?>>Amapá</option>
								<option value="AM" <?php echo $endereco['estado'] == 'AM'?'selected':'';?>>Amazonas</option>
								<option value="BA" <?php echo $endereco['estado'] == 'BA'?'selected':'';?>>Bahia</option>
								<option value="CE" <?php echo $endereco['estado'] == 'CE'?'selected':'';?>>Ceará</option>
								<option value="DF" <?php echo $endereco['estado'] == 'DF'?'selected':'';?>>Distrito Federal</option>
								<option value="ES" <?php echo $endereco['estado'] == 'ES'?'selected':'';?>>Espirito Santo</option>
								<option value="GO" <?php echo $endereco['estado'] == 'GO'?'selected':'';?>>Goiás</option>
								<option value="MA" <?php echo $endereco['estado'] == 'MA'?'selected':'';?>>Maranhão</option>
								<option value="MS" <?php echo $endereco['estado'] == 'MS'?'selected':'';?>>Mato Grosso do Sul</option>
								<option value="MT" <?php echo $endereco['estado'] == 'MT'?'selected':'';?>>Mato Grosso</option>
								<option value="MG" <?php echo $endereco['estado'] == 'MG'?'selected':'';?>>Minas Gerais</option>
								<option value="PA" <?php echo $endereco['estado'] == 'PA'?'selected':'';?>>Pará</option>
								<option value="PB" <?php echo $endereco['estado'] == 'PB'?'selected':'';?>>Paraíba</option>
								<option value="PR" <?php echo $endereco['estado'] == 'PR'?'selected':'';?>>Paraná</option>
								<option value="PE" <?php echo $endereco['estado'] == 'PE'?'selected':'';?>>Pernambuco</option>
								<option value="PI" <?php echo $endereco['estado'] == 'PI'?'selected':'';?>>Piauí</option>
								<option value="RJ" <?php echo $endereco['estado'] == 'RJ'?'selected':'';?>>Rio de Janeiro</option>
								<option value="RN" <?php echo $endereco['estado'] == 'RN'?'selected':'';?>>Rio Grande do Norte</option>
								<option value="RS" <?php echo $endereco['estado'] == 'RS'?'selected':'';?>>Rio Grande do Sul</option>
								<option value="RO" <?php echo $endereco['estado'] == 'RO'?'selected':'';?>>Rondônia</option>
								<option value="RR" <?php echo $endereco['estado'] == 'RR'?'selected':'';?>>Roraima</option>
								<option value="SC" <?php echo $endereco['estado'] == 'SC'?'selected':'';?>>Santa Catarina</option>
								<option value="SP" <?php echo $endereco['estado'] == 'SP'?'selected':'';?>>São Paulo</option>
								<option value="SE" <?php echo $endereco['estado'] == 'SE'?'selected':'';?>>Sergipe</option>
								<option value="TO" <?php echo $endereco['estado'] == 'TO'?'selected':'';?>>Tocantins</option>
								</select>
								<div class="invalid-feedback">
									Please provide a valid state.
								</div>
							</div>
						</div>
						<hr class="mb-4">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" onclick='handleClickSameAddress(this);' class="custom-control-input" id="same-address">
							<label class="custom-control-label" for="same-address">Endere&ccedil;o de entrega vai ser o mesmo de cobran&ccedil;a?</label>
						</div>
						<hr class="mb-4">
					</form>
				</div>
			</div>
        </div>

        <div class="col-12 col-md-8 order-md-1 mb-5">
			<div class="card">
                <div class="card-body">
					<h3 class="mb-3">Endere&ccedil;o de entrega</h3>

					<div class="enderecoSelecionado"></div>
					
					<?php 
						$sql = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente`='".$id_usuario."' AND `status`='S' AND `deleted_at` IS NULL");
						if(mysqli_num_rows($sql) > 0) {
					?>
						<section id="meus-dados" class="my-4">
							<div class="container">
								<div class="row justify-content-center align-items-center my-3">
									<div class="col-3">
										<a href="<?php echo PATH ?>/adicionar-endereco" class="btn btn-comprar">Adicionar novo endere&ccedil;o</a>
									</div>
								</div>
								<div class="row justify-content-center align-items-center my-3">
									<?php $enderecoArray = array(); while($endereco = mysqli_fetch_array($sql)) { ?>
										<?php
										$enderecoArray['id'] = $endereco['id'];
										$enderecoArray['logradouro'] = $endereco['logradouro'];
										$enderecoArray['numero'] = $endereco['numero'];
										$enderecoArray['cep'] = $endereco['cep'];
										$enderecoArray['cidade'] = $endereco['cidade'];
										$enderecoArray['estado'] = $endereco['estado'];
										$enderecoArray['principal'] = $endereco['principal'];
										$enderecoArray['bairro'] = $endereco['bairro'];
										$enderecoArray['complemento'] = $endereco['complemento'];
										$enderecoArray['pais'] = 'br';
										?>
										<div class="col-12 col-sm-6 mb-4">
											<div class="card">
												<div class="card-body">
													<h4 class="card-title" style="text-align: center"><?php echo $endereco['logradouro']?>, <?php echo $endereco['numero']?></h1>
													<ul style="list-style: circle">
														<li><strong>CEP: </strong><?php echo $endereco['cep'] ?></li>
														<li><strong>Cidade: </strong><?php echo $endereco['cidade'] ?>, <?php echo $endereco['estado'] ?></li>
														<li><strong>Endereço Principal: </strong><?php echo $endereco['principal'] ?></li>
														<li><strong>Bairro: </strong><?php echo $endereco['bairro'] ?></li>
														<li><strong>Complemento: </strong><?php echo $endereco['complemento'] ?></li>
													</ul>
													<button type="button" class="btn btn-primary btn-lg btn-block" onclick='selecionarEndereco(<?php echo json_encode($enderecoArray); ?>)' >SELECIONAR</button>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</section>

					<?php } else { ?>
						<section id="meus-dados" class="my-4">
							<div class="container">
								<h4 class="mb-3">Sem Endereços Cadastrados</h3>
								<div class="row justify-content-end align-items-end">
									<div class="col-3">
										<a href="<?php echo PATH ?>/adicionar-endereco" class="btn btn-comprar">Adicionar novo endere&ccedil;o</a>
									</div>
								
								</div>
							</div>
						</section>
					<?php } ?>
				</div>
			</div>
		</div>

        
    </div>
    <div class="row justify-content-center align-items-end my-3">
        <div class="col-3">
      		<button name="pagar" id="pay-button" onclick="checkout()" class="btn btn-primary btn-lg btn-block">PAGAR</button>
		</div>
	</div>
 
	<script type="text/javascript">
    

    var valorFrete;
    var enderecoEntrega;

    function selecionarEndereco(endereco) {
      enderecoEntrega = endereco;
      valorFrete = null;
      document.getElementById('cep').value = endereco['cep'];
      $("#wait").css("display", "block");
      $.post('<?php echo PATH ?>/selecionar-endereco.php', {endereco: endereco}, function (retorno) {
          $('.enderecoSelecionado').html(retorno);
          $('.retornoFretePac').html('<div></div>');
          $('.retornoFreteSedex').html('<div></div>');
          $('.freteSelecionado').html('<li class="list-group-item d-flex justify-content-between"><span>Frete</span><strong> SELECIONE O FRETE</strong></li>');

          $("#wait").css("display", "none");
      });
    }

    function selecionarData() {
      $('input[name="dataCobranca"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10)
      }, function(start, end, label) {
        
      });
    };

    function calcularFrete(cep) {
		  const cepFormatado = cep.replace(/[^\w\s]/gi, '');

        $("#pac").css("display", "block");
        $.post('<?php echo PATH ?>/calcular-frete.php', {cep: cepFormatado, nCdServico: '04510'}, function (retorno) {
            $('.retornoFretePac').html(retorno);
            $("#pac").css("display", "none");
        });

        $("#sedex").css("display", "block");
        $.post('<?php echo PATH ?>/calcular-frete.php', {cep: cep, nCdServico: '04014'}, function (retorno) {
            $('.retornoFreteSedex').html(retorno);
            $("#sedex").css("display", "none");
        });
    }

    function selecionaFrete(opcao, valor) {
      valorFrete = valor;
      $("#wait").css("display", "block");
      $.post('<?php echo PATH ?>/selecionar-frete.php', {opcao: opcao, valor: valor, valorProdutos: <?php echo $valor_total ?>}, function (retorno) {
          $('.freteSelecionado').html(retorno);
          $("#wait").css("display", "none");
      });
    }

    function validaFormulario() {
      var _nome = document.getElementById('firstNameCobranca').value;
      var _cpf = document.getElementById('cpfCobranca').value;
      var _email = document.getElementById('emailCobranca').value;
      var _endereco = document.getElementById('addressCobranca').value;
      var _numero = document.getElementById('addressNumeroCobranca').value;
      var _cidade = document.getElementById('cidadeCobranca').value;
      var _pais = document.getElementById('countryCobranca').value;
      var _estado = document.getElementById('stateCobranca').value;
      var _cep = document.getElementById('zipCobranca').value;
      var _dataNascimento = document.getElementById('dataCobranca').value;


      if (_nome === '' || _cpf === '' || _email === '' || _endereco === '' || _numero === '' ||
          _cidade === '' || _pais === '' || _estado === '' || _cep === '' || _dataNascimento === '') {
        return false;
      }
      return true;
    }

	function checkout() {
      if ( !validaFormulario()) {
        alert('Favor preencher os campos de cobrança e entrega.');
      } else if ( !valorFrete) {
        alert('Selecione o frete');
      } else {

        var valorTotal = <?php echo $valor_total * 100?> + valorFrete * 100;

        var _nome = document.getElementById('firstNameCobranca').value;
        var _cpf = document.getElementById('cpfCobranca').value;
        var _email = document.getElementById('emailCobranca').value;
        var _endereco = document.getElementById('addressCobranca').value;
        var _numero = document.getElementById('addressNumeroCobranca').value;
        var _complemento = document.getElementById('complementoCobranca').value;
        var _cidade = document.getElementById('cidadeCobranca').value;
        var _pais = document.getElementById('countryCobranca').value;
        var _estado = document.getElementById('stateCobranca').value;
        var _cep = document.getElementById('zipCobranca').value;
        _cep = _cep.replace(/[^\w\s]/gi, '');
        var _dataNascimento = document.getElementById('dataCobranca').value;
        var msec = Date.parse(_dataNascimento);
        const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' }); 
        const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(msec);

        _dataNascimento = `${year}-${month}-${day}`;

        var _enderecoEntrega = enderecoEntrega['logradouro'];
        var _numeroEntrega = enderecoEntrega['numero'];
        var _complementoEntrega = enderecoEntrega['complemento'];
        var _cidadeEntrega = enderecoEntrega['cidade'];
        var _paisEntrega = enderecoEntrega['pais'];
        var _estadoEntrega = enderecoEntrega['estado'];
        var _cepEntrega = enderecoEntrega['cep'];
		_cepEntrega = _cepEntrega.replace(/[^\w\s]/gi, '');

        var checkout = new PagarMeCheckout.Checkout({
          // encryption_key: 'ek_live_d8ON0KSyMc4sEvOLHhSQ0UFiQ1jCwa',
          encryption_key: 'ek_test_uU6vlX2HrWUckUWH0kq9t0rsz0ZJ03',
          success: function(data) {
            $("#wait").css("display", "block");
            $.post('<?php echo PATH ?>/capture_transaction.php', {data: data, valor: valorTotal}, function (retorno) {
              retorno = JSON.parse(retorno);
              if (retorno['payment_method'] === 'boleto') {
                swal("Pagamento!", "Boleto criado!\n" + retorno['boleto_url'] + "\n" + retorno['boleto_barcode'], "success");
              } else if (retorno['payment_method'] === 'credit_card') {
                swal("Pagamento!", "Pagamento Efetuado!\nObrigado!", "success").then((value) => {
                  window.location.href = "<?php echo PATH ?>/";
                });
              }
            });
          },
          error: function(err) {
            console.log(err);
          },
          close: function() {
            console.log('The modal has been closed.');
          }
        });
        checkout.open({
          amount: valorTotal,
          free_installments: 1,
          max_installments: 6,
          interest_rate: 2,
          buttonText: 'Pagar',
          buttonClass: 'botao-pagamento',
          customerData: 'false',
          createToken: 'true',
          paymentMethods: 'credit_card, boleto',
          customer: {
            external_id: <?php echo $row['id'] ?>,
            name: _nome,
            type: 'individual',
            country: _pais,
            email: _email,
            documents: [
              {
                type: 'cpf',
                number: _cpf,
              },
            ],
            phone_numbers: ['+55<?php echo str_replace(array("(", ")","-"," "),"",$row['telefone']) ?>', '+55<?php echo str_replace(array("(", ")","-"," "),"",$row['celular'])?>'],
            birthday: _dataNascimento
          },
          billing: {
            name: "Cobrança",
            address: {
              country: _pais,
              state: _estado,
              city: _cidade,
              street: _endereco,
              street_number: _numero,
              zipcode: _cep
            }
          },
          shipping: {
            name: "Entrega",
            fee: valorFrete * 100,
            expedited: true,
            address: {
              country: _paisEntrega,
              state: _estadoEntrega,
              city: _cidadeEntrega,
              street: _enderecoEntrega,
              street_number: _numeroEntrega,
              zipcode: _cepEntrega
            }
          },
          items: [
            <?php while($item = mysqli_fetch_array($carrinho2)) { ?>
              {
                id: <?php echo $item["id_produto"]?>,
                title: "<?php echo  $item["nome_produto"]?>",
                unit_price: <?php echo  $item["valor_produto"] * 100?>,
                quantity: <?php echo  $item["quantidade"]?>,
                
                tangible: true
              },
            <?php } ?>
          ]
        });
      }  
    };
    
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
</section>