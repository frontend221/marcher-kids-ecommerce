<?php
$categoria = mysqli_query($conn, "SELECT * FROM `prod-categoria` WHERE `id`='".$exp[1]."' AND `deleted_at` IS NULL");
if(mysqli_num_rows($categoria) > 0) {
?>	
	<?php while($cat = mysqli_fetch_array($categoria)) { ?>
		<section id="categoria">
			<div class="container">
				<div class="row">
					<h1><?php echo $cat["nome"]; ?></h1>
				</div>
				<div class="row">
					
					<?php
						$subcategoria = mysqli_query($conn, "SELECT * FROM `prod-grupo` WHERE `categoria`='".$cat['id']."'");
						while($subcat = mysqli_fetch_array($subcategoria)) {
					?>
					
						<div class="col-lg-4 col-md-4 col-sm-12 img">
							<a href="<?php echo PATH ?>/produtos-grupo/<?php echo slugit($subcat['id'])?>" style="position: relative">
							<?php 
								$categoriaNome = slugit($subcat['nome']);
								$filename = "upload/subcategoria/{$categoriaNome}.png";

								if(file_exists($filename)) {
							?>
								<img src="<?php echo IMAGE ?>/subcategoria/<?php echo slugit($subcat['nome'])?>.png">
							<?php } else { ?>
								<img src="<?php echo IMAGE ?>/subcategoria/sapatilhas.png">
									<h2 style="position: absolute; top:50%"><?php echo $subcat['nome'] ?></h2>
							<?php } ?>
							</a>
						</div>
					
					<?php } ?>
					
				</div>
			</div>
		</section>
	<?php } ?>

<?php } ?>