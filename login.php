<section id="acesso" class="py-4">
	<div class="container">
		<div class="row">
			<div class="form">
				<h4>Acessar sua conta</h4>
				<form action="<?php echo PATH ?>/funcao.php" method="post">
					
					<input type="hidden" name="action" value="login" required>
					
					<div class="form-group">
						<label for="usuario">CPF ou CNPJ</label>
						<input type="text" class="form-control" id="usuario" name="usuario" required>
					</div>
					<div class="form-group">
						<label for="senha">Senha</label>
						<input type="password" class="form-control" id="senha" name="senha" required>
					</div>
					<p><a href="<?php echo PATH ?>/recuperar-senha">Esqueceu sua senha?</a></p>
					<button type="submit" name="enviar" class="btn btn-primary">Entrar</button>

				</form>
				<div class="divider">
					<h2>Deseja se cadastrar?</h2>
				</div>
				<p class="link">
					<a href="<?php echo PATH ?>/cadastro" class="btn btn-secondary">Cadastrar</a>
				</p>
			</div>
		</div>
	</div>
</section>