<?php include("../config/path.php"); ?>

<!doctype html>
<html lang="en">
<head>
	
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="<?php echo OUT ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo OUT ?>/assets/css/estilo.css">
	
<title>Estamos em manutenção!</title>

<link rel="icon" href="<?php echo ASSETS ?>/img/favicon.png">

</head>
<body>
	
	<div class="wrapper">
		<div>
			<img src="<?php echo OUT ?>/assets/img/logo.png" alt="Auto Peças 19" class="img-fluid">
			<h4>Estamos em manutenção</h4>
		</div>
	</div>
	
	<script src="<?php echo OUT ?>/assets/js/jquery-3.4.1.min.js"></script>
	<script src="<?php echo OUT ?>/assets/js/popper.min.js"></script>
	<script src="<?php echo OUT ?>/assets/js/tooltip.min.js"></script>
	
</body>
</html>