	 <?php

		if(isset($_SESSION["id"]) && isset($_SESSION["token"])) {

		ob_start();
		
	?>	

		<header id="navbar">
            <div id="navbar-container" class="boxed">

				<div class="navbar-header">
                    <a href="<?php echo URL ?>/dashboard/listar/1" class="navbar-brand">
						<img src="<?php echo URL ?>/assets/img/cms.png" alt="Administração do Conteúdo" class="brand-icon">
                        <div class="brand-title">
							<span class="gc white">Administração do Conteúdo</span>
                        </div>
                    </a>
                </div>

                <div class="navbar-content">
                    <ul class="nav navbar-top-links">

                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#">
                                <i class="demo-pli-list-view"></i>
                            </a>
                        </li>

					</ul>
					<ul class="nav navbar-top-links">
					
					</ul>
                </div>

            </div>
        </header>

	<?php

		ob_end_flush();

		}

	?>