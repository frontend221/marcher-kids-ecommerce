	<?php

		if(isset($_SESSION["id"]) && isset($_SESSION["token"])) {

		ob_start();
		
			function CopyRight($year = 'auto'){ 
				if(intval($year) == 'auto') {
					$year = date('Y'); 
				} elseif(intval($year) == date('Y')) {
					echo intval($year);
				} elseif(intval($year) < date('Y')) {
					echo intval($year) . ' - ' . date('Y'); 
				} elseif(intval($year) > date('Y')) {
					echo date('Y');
				}
			}

	?>	

		<footer id="footer">
			<p class="pad-lft">Administração do Conteúdo - <strong>Oxygen Lab - Web e Mobile &copy; <?=CopyRight("2019");?></strong></p>
		</footer>

		<button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>

	<?php

		ob_end_flush();

		}

	?>