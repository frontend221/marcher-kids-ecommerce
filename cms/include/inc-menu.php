	 <?php

		if(isset($_SESSION["id"]) && isset($_SESSION["token"])) {

		ob_start();

	?>	

           <nav id="mainnav-container">
                <div id="mainnav">

                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap text-center">
                                        <div class="pad-btm">
                                            <img class="img-circle img-md" src="<?php echo URL ?>/assets/img/avatar-m.png" alt="<?=$_SESSION["nome"];?>">
                                        </div>
                                        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                            <p class="mnp-name"><?=$_SESSION["nome"];?></p>
                                            <span class="mnp-desc"><?=$_SESSION["email"];?></span>
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                        <a href="<?php echo URL ?>/usuario-do-cms/perfil/<?=$_SESSION["id"];?>" class="list-group-item">
                                            <i class="demo-pli-male icon-lg icon-fw"></i> Perfil
                                        </a>
                                        <a href="<?php echo URL ?>/logout.php?tkn=<?=$_SESSION["token"];?>" class="list-group-item">
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                        </a>
                                    </div>
                                </div>

                                <ul id="mainnav-menu" class="list-group">
									
									<?php 
			
									// inserir o tipo de menu e comparar sessão com o que estiver no módulo
									
									if($_SESSION["login"] == "masteradmin") {
										
										$sec = mysqli_query($conn, "SELECT g.* FROM `conf-grupo-modulo` AS g WHERE EXISTS (SELECT m.* FROM `conf-modulo` AS m WHERE m.`grupo` = g.`id` AND m.`status`='S' AND m.`deleted_at` IS NULL) AND g.`deleted_at` IS NULL ORDER BY g.`posicao` ASC, g.`nome` ASC");
										
									} else {
		
										$sec = mysqli_query($conn, "SELECT g.* FROM `conf-grupo-modulo` AS g INNER JOIN `usu-grupo-tipo` AS t ON t.`grupo`=g.`id` WHERE EXISTS (SELECT m.* FROM `conf-modulo` AS m WHERE m.`grupo`=g.`id` AND m.`status`='S' AND m.`deleted_at` IS NULL) AND t.`tipo`='".$_SESSION["tipo"]."' AND t.`status`='S' AND g.`status`='S' AND g.`deleted_at` IS NULL ORDER BY g.`posicao` ASC, g.`nome` ASC");
										
									}
		
									if(mysqli_num_rows($sec) > 0) {
									
									?>
									
										<?php while($ln = mysqli_fetch_array($sec)) { ?>
									
											<li class="list-header"><?=$ln["nome"];?></li>
									
											<?php 
											
											$mod = mysqli_query($conn, "SELECT l.* FROM `conf-modulo` AS l WHERE EXISTS (SELECT p.* FROM `usu-permissao-tipo` AS p WHERE p.`tipo`='".$_SESSION["tipo"]."' AND p.`habilitado`='S') AND l.`grupo`='".$ln["id"]."' AND l.`status`='S' AND l.`deleted_at` IS NULL ORDER BY l.`posicao` ASC");
												
											if(mysqli_num_rows($mod) > 0) {
																					 
											?>
									
											<?php while($nav = mysqli_fetch_array($mod)) { ?>
									
											<li <?=($exp[0] == $nav["diretorio"] ? "class='active-link'" : ''); ?>>
												<a href="<?php echo URL ?>/<?=$nav["diretorio"];?>/listar/1">
													<i class="<?=$nav["icone"];?> fa-fw"></i>
													<span class="menu-title"><?=$nav["titulo"];?></span>
												</a>
											</li>
												
											<?php } ?>
												
						            		<li class="list-divider"></li>

											<?php } ?>
									
										<?php } ?> 
									
									<?php } ?>
																								
                                </ul>

                            </div>
                        </div>
                    </div>

                </div>
            </nav>

	<?php

		ob_end_flush();

		}

	?>