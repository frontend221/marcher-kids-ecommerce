<?php

	if(isset($_SESSION["id"]) && isset($_SESSION["token"])) {
		
		ob_start();
		
		if(isset($exp[0]) && is_string($exp[0])) {
						
			if($exp[1] == "listar") {
				
				if ($exp[3] != "") {
					
					if($exp[3] == 1) {

						echo '<div class="alert alert-success" role="alert" id="info"> Operação realizada com sucesso. </div>';
						
					} else if ($exp[3] == "2") {

						echo '<div class="alert alert-danger" role="alert" id="info"> Ocorreu um erro ao realizar sua operação. </div>';
						
					}

				}
				
			} else if($exp[1] == "adicionar") {
				
				if ($exp[3] != "") {
					
					if($exp[3] == 1) {

						echo '<div class="alert alert-success" role="alert" id="info"> Operação realizada com sucesso. </div>';
						
					} else if ($exp[3] == "2") {

						echo '<div class="alert alert-danger" role="alert" id="info"> Ocorreu um erro ao realizar sua operação. </div>';
						
					}

				}
				
			} else if($exp[1] == "editar") {
				
				if ($exp[4] != "") {
					
					if($exp[4] == 1) {

						echo '<div class="alert alert-success" role="alert" id="info"> Operação realizada com sucesso. </div>';
						
					} else if ($exp[4] == "2") {

						echo '<div class="alert alert-danger" role="alert" id="info"> Ocorreu um erro ao realizar sua operação. </div>';
						
					}

				}
				
			} else if($exp[1] == "apagar") {
				
				if ($exp[4] != "") {
					
					if($exp[4] == 1) {

						echo '<div class="alert alert-success" role="alert" id="info"> Operação realizada com sucesso. </div>';
						
					} else if ($exp[4] == "2") {

						echo '<div class="alert alert-danger" role="alert" id="info"> Ocorreu um erro ao realizar sua operação. </div>';
						
					}

				}
				
			}
			
		}

		ob_end_flush();

	}

?>