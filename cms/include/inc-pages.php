<?php

	if(isset($_SESSION["id"]) && isset($_SESSION["token"])) {
		
		ob_start();
		session_name("LINSPAPEISECOMMERCEOXYGEN");
		session_start();

		$menos = $exp[2] - 1;
		$mais = $exp[2] + 1;

		$pgs = ceil($total / $maximo);

		if($pgs > 1 ) {

			echo "<nav>";
				echo "<ul class='pagination'>";
		
				if($menos > 0) {
					echo "<li class='page-item'>";
						echo "<a class='page-link' href=" . URL . "/".$exp[0]."/listar/".$menos.">";
							echo "<i class='fas fa-angle-double-left'></i>";
						echo "</a>";
					echo "</li>";
				}

				echo "<li class='page-item active'>";
					echo "<a class='page-link'>".$exp[2]."</a>";
				echo "</li>";

				if($mais <= $pgs) {
					echo "<li class='page-item'>";
						echo "<a class='page-link' href=" . URL . "/".$exp[0]."/listar/".$mais.">";
							echo "<i class='fas fa-angle-double-right'></i>";
						echo "</a>";
					echo "</li>";
				}

				echo "</ul>";
			echo "</nav>";

		}

		ob_end_flush();

	}

?>