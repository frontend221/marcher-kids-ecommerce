<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	$tkn = NULL;
	$tkn = (!empty($_GET["tkn"]) ? $_GET["tkn"] : $_SESSION["token"]);

	require_once ("config/cfg-database.php");
	require_once ("config/cfg-path.php");
	require_once ("config/cfg-device.php");

	$sql = mysqli_query($conn, "UPDATE `usu-sistema` SET `token`='' WHERE `token`='".$tkn."' AND `updated_at`=NOW()");

	if($sql) {

		$query = mysqli_query($conn, "INSERT INTO `log-acesso-cms`(`id`, `usuario`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','".utf8_encode('Encerrou a sessão')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

	} else {

		header("Location: " . URL . "/dashboard/listar/1");

	}

	session_unset();
	session_destroy();
		
	header("Location: " . URL . "/login.php?msg=_logout");

	ob_end_flush();

?>