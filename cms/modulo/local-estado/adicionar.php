	<?php

		if (preg_match("/adicionar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
															   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_c); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
								
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
										
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="store" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_c;?>" required>

					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="pais">País</label>
							<div class="col-sm-8">
								<select name="pais" id="pais" data-placeholder="Selecione o país" tabindex="0" data-width="100%">
									<?php
									$pais = mysqli_query($conn, "SELECT * FROM `local-pais` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($pais) > 0) {
									?>
									<option value=""></option>
									<?php
									while($ln = mysqli_fetch_array($pais)){
									?>	
									<option value="<?=$ln["id"];?>"><?=$ln["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um país para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nome">Estado</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Estado" id="nome" name="nome" class="form-control" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Status do Registro</label>
							<div class="col-md-9">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" checked>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N">
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
							</div>
						</div>
					</div>
					
				</form>

				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>