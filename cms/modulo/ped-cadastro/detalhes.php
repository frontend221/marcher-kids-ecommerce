	<?php

		if (preg_match("/galeria.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
															   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_c); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<?php if(isset($exp[3]) && $exp[3] != "") { ?>			

				<?php $sql = mysqli_query($conn, "SELECT p.*, c.*, ce.* FROM `ped-cadastro` as p INNER JOIN `cli-cadastro` as c on c.`id` = p.`cliente` INNER JOIN `cli-endereco` as ce on ce.id = p.`endereco` WHERE p.`id`='".$exp[3]."' AND p.`deleted_at` IS NULL"); ?>
				<?php if(mysqli_num_rows($sql) == 1) { ?>
				<?php $ln = mysqli_fetch_array($sql); ?>
				
				<!-- BODY -->
					<div class="pad-btm form-inline">
						<div class="row">
							<h4><?=($ln["tipopessoa"] == "PF" ? "".$ln["razaosocial"]." ".$ln["nomefantasia"]."" : $ln["razaosocial"]); ?></h4>
							<ul class="my-3" style="list-style: circle; column-count: 4;">
								<li><?=($ln["tipopessoa"] == "PF" ? "<strong>CPF: </strong>". $ln["cpfcnpj"]."" : "<strong>CNPJ: </strong>". $ln["cpfcnpj"].""); ?></li>
								<li><strong>Telefone: </strong><?php echo $ln["telefone"]; ?></li>
								<li><strong>Email: </strong><?php echo $ln["email"]; ?></li>
								<li><strong>Celular: </strong><?php echo $ln["celular"]; ?></li>
							</ul>
							<h4>Endereço de Entrega</h4>
							<ul class="my-3" style="list-style: circle; column-count: 4;">
								<li><?php echo $ln['logradouro']?>, <?php echo $ln['numero']?></li>
								<li><strong>CEP: </strong><?php echo $ln['cep'] ?></li>
								<li><strong>Cidade: </strong><?php echo $ln['cidade'] ?>, <?php echo $ln['estado'] ?></li>
								<li><strong>Endereço Principal: </strong><?php echo $ln['principal'] ?></li>
								<li><strong>Bairro: </strong><?php echo $ln['bairro'] ?></li>
								<li><strong>Complemento: </strong><?php echo $ln['complemento'] ?></li>
							</ul>
							<h4>Informações Pedido</h4>
							<ul class="my-3" style="list-style: circle; column-count: 4;">
								<li><strong>Opção Frete: </strong><?php echo $ln['opcaofrete'] ?></li>
								<li><strong>Valor Frete: </strong><small>R$</small> <?=number_format($ln["valorfrete"], 2, ",", ".");?></li>
								<li><strong>Forma Pagamento: </strong><?=($ln["formapagamento"] == "boleto" ? "Boleto" : "Cartão de Crédito"); ?></li>
								<li><strong>Qtde Parcelas: </strong><?php echo $ln['quantidadeparcelas'] ?></li>
							</ul>
						</div>
					</div>

					<div class="table-responsive my-5">
						<table id="item" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th class="text-center" width="6%" style="text-align: center !important;">#</th>
									<th width="12%">Imagem</th>
									<th width="56%">Produto</th>
									<th width="12%">Quantidade</th>
									<th width="14%">Valor</th>
								</tr>
							</thead>
							<tbody id="myTable">													
								<?php $resultado = mysqli_query($conn, "SELECT pi.*, poi.nome FROM `ped-item` as pi INNER JOIN `prod-item` poi on poi.id = pi.produto WHERE pi.`pedido` = '".$exp[3]."'"); ?>
								
								<?php while ($ln2 = mysqli_fetch_array($resultado)) { ?>
															
								<tr>
									<td style="text-align: center !important; vertical-align: middle !important;"><?=$ln2["id"];?></td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<?php
											$imagem = mysqli_query($conn, "SELECT * FROM `prod-imagem` WHERE `produto`='".$ln2["produto"]."' AND `status`='S' AND `deleted_at` IS NULL ORDER BY `id` LIMIT 1");
											if(mysqli_num_rows($imagem)) {
												$img = mysqli_fetch_array($imagem)	
												?>
											<img src="<?php echo IMG ?>/produto/<?php echo $img["imagem"]; ?>" alt="<?php echo $ln2["nome"]; ?>" width="120" height="auto" class="img-responsive">
											<?php } else { ?>
											<img src="<?php echo IMG ?>/produto/default.png" alt="<?php echo $ln2["nome"]; ?>" width="120" height="auto" class="img-responsive">
										<?php } ?>									
									</td>
									<td style="vertical-align: middle !important;"><?=$ln2["nome"];?></td>
									<td style="text-align: center !important; vertical-align: middle !important;"><strong><?=$ln2["quantidade"];?></strong></td>
									<td style="vertical-align: middle !important;"><small>R$</small> <?=number_format($ln2["valor_unitario"], 2, ",", ".");?></td>
								</tr>
								
								<?php } ?>

							</tbody> 
							<tfoot>
								<tr>
									<td colspan="4">#</td>
									<td><small>R$</small> <?=number_format($ln["valortotal"], 2, ",", ".");?></td>
								</tr>				
							</tfoot>
						</table>
					</div>					

				<?php } else { ?>
							
				    <div class="alert alert-warning" role="alert">
					    Não há pedido cadastrado com este identificador. Verifique.
				    </div>						
																																		
			    <?php } ?>
				
				<?php } else { ?>
							
				    <div class="alert alert-warning" role="alert">
					    Nenhum identificador foi informado para esta consulta. Verifique.
				    </div>						
																																		
			    <?php } ?>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-8">
						<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset">	<i class="fas fa-undo-alt"></i> Voltar</a>
					</div>
				</div>
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>