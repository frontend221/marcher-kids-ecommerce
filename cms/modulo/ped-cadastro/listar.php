	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
			
            <div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->

                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="24%" style="text-align: center !important;">Cliente</th>
					            <th width="12%">Data</th>
					            <th width="14%">Pagamento</th>
					            <th width="16%">Quantidade Itens</th>
					            <th width="12%">Valor</th>
					            <th width="8%" style="text-align: center !important;">Detalhes</th>
					            <th width="8%" style="text-align: center !important;">Status</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "p.`id`,
								p.`valortotal`,
								c.`razaosocial`,
								(CASE WHEN p.`formapagamento` = 'boleto' THEN 'Boleto'
									ELSE 'Cartão Crédito' END) as formapagamento,
								COUNT(p.`id`) as quantidade,
									(CASE WHEN p.`status` = 'ANA' THEN 'EM ANÁLISE'
								ELSE 'APROVADO' END) as status,
							p.`created_at`";
							
							$where = "WHERE p.`deleted_at` IS NULL";
							
							$final = "FROM `ped-cadastro` as p
								INNER JOIN `cli-cadastro` as c on c.`id` = p.`cliente`
								INNER JOIN `ped-item` as pi on pi.`pedido` = p.`id`
								GROUP BY p.`id`";
							
							$sql = mysqli_query($conn, "SELECT $campos $final");
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["razaosocial"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=date("d/m/Y", strtotime($ln["created_at"]));?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["formapagamento"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><strong><?=$ln["quantidade"];?></strong></td>
					            <td style="vertical-align: middle !important;"><small>R$</small> <?=number_format($ln["valortotal"], 2, ",", ".");?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<div class="btn-group">
										<a href="<?php echo URL ?>/<?=$exp[0];?>/detalhes/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-purple add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Detalhes do Pedido">
											<i class="fas fa-eye"></i>										</a>
									</div>
								</td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<?=($ln["status"] == "ANA" ? "<div class='label label-table label-success'>Confirmado</div>" : "<div class='label label-table label-danger'>Em Análise</div>");?>
								</td>
					        </tr>
							
							<?php } ?>

						</tbody> 
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>