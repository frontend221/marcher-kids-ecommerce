<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");
				
				/* INSERIR REGISTRO */
				
				if ($act == "store") {
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome da rede social."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
						
					if($_POST["icone"] == "") {
						echo('<script language = "javascript"> alert("Escolha e informe o ícone da rede social."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$icone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["icone"])));
					}
					
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					/* ********** VERIFICA SE HÁ REGISTRO COM O MESMO NOME ********** */
					
					$qry = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `social-rede` WHERE `nome`='".$nome."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/3");

					} else {
						
						$qry = mysqli_query($conn, "SELECT COUNT(icone) AS TOTAL FROM `social-rede` WHERE `icone`='".$icone."' AND `deleted_at` IS NULL");
						$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

							header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

						} else {
																					
							$sql = mysqli_query($conn, "INSERT INTO `social-rede`(`id`, `nome`, `icone`, `status`, `created_at`) VALUES (0,'".$nome."','".$icone."','".$status."',NOW())");

							if ($sql) {

								$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','social-rede','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

							} else {

								header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

							}

						}
						
					}
					
				/* ALTERAR REGISTRO */	

				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome da rede social."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
						
					if($_POST["icone"] == "") {
						echo('<script language = "javascript"> alert("Escolha e informe o ícone da rede social."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$icone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["icone"])));
					}
					
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					/* ********** VERIFICA SE HÁ REGISTRO COM O MESMO NOME ********** */
					
					$qry = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `social-rede` WHERE `nome`='".$nome."' AND `id` <> '".$id_registro."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {
						
						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/3");

					} else {
						
						$qry = mysqli_query($conn, "SELECT COUNT(icone) AS TOTAL FROM `social-rede` WHERE `icone`='".$icone."' AND `id` <> '".$id_registro."' AND `deleted_at` IS NULL");
						$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

							header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/4");

						} else {
							
							$sql = mysqli_query($conn, "UPDATE `social-rede` SET `nome`='".$nome."',`icone`='".$icone."',`status`='".$status."' WHERE `id`='".$id_registro."'");

							if ($sql) {

								$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','social-rede','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

							} else {

								header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/2");

							}

						}
						
					}
					
				/* APAGAR REGISTRO */	
					
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `social-rede` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','social-rede','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>