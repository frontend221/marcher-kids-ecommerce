<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");

				if($_POST["chaveapi"] == "") {
					echo('<script language = "javascript"> alert("Informe a chave da API."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$chaveapi = mysqli_real_escape_string($conn, stripslashes(trim($_POST["chaveapi"])));
				}
						
				if($_POST["chavecriptografia"] == "") {
					echo('<script language = "javascript"> alert("Informe a chave de criptografia."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$chavecriptografia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["chavecriptografia"])));
				}
						
				if($_POST["banco"] == "") {
					echo('<script language = "javascript"> alert("Informe o número do banco."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$banco = mysqli_real_escape_string($conn, stripslashes(trim($_POST["banco"])));
				}
					
				if($_POST["agencia"] == "") {
					echo('<script language = "javascript"> alert("Informe o número da agência bancária."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$agencia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["agencia"])));
				}
				
				$agenciadv = mysqli_real_escape_string($conn, stripslashes(trim($_POST["agenciadv"])));
					
				if($_POST["conta"] == "") {
					echo('<script language = "javascript"> alert("Informe o número da conta."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$conta = base64_encode(mysqli_real_escape_string($conn, stripslashes(trim($_POST["conta"]))));
				}
										
				if($_POST["contadv"] == "") {
					echo('<script language = "javascript"> alert("Informe o dígito da conta."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$contadv = mysqli_real_escape_string($conn, stripslashes(trim($_POST["contadv"])));
				}

				if($_POST["tipoconta"] == "") {
					echo('<script language = "javascript"> alert("Selecione o tipo da conta."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$tipoconta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tipoconta"])));
				}

				if($_POST["cpfcnpj"] == "") {
					echo('<script language = "javascript"> alert("Informe o CPF ou CNPJ do titular da conta."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$cpfcnpj = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cpfcnpj"])));
				}

				if($_POST["nome"] == "") {
					echo('<script language = "javascript"> alert("Informe o nome do títular da conta."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
				}
				
				if($_POST["idconta"] == "") {
					echo('<script language = "javascript"> alert("Informe o ID da conta no gateway."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$idconta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["idconta"])));
				}
				
				if($_POST["idrecebedor"] == "") {
					echo('<script language = "javascript"> alert("Informe o ID do recebedor no gateway."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$idrecebedor = mysqli_real_escape_string($conn, stripslashes(trim($_POST["idrecebedor"])));
				}
				
				/* INSERIR REGISTRO */
				
				if ($act == "store") {
										
					$sql = mysqli_query($conn, "INSERT INTO `conf-gateway`(`id`, `chaveapi`, `chavecriptografia`, `banco`, `agencia`, `agenciadv`, `conta`, `contadv`, `tipoconta`, `cpfcnpj`, `nome`, `idconta`, `idrecebedor`, `created_at`) VALUES (1,'".$chaveapi."','".$chavecriptografia."','".$banco."','".$agencia."','".$agenciadv."','".$conta."','".$contadv."','".$tipoconta."','".$cpfcnpj."','".$nome."','".$idconta."','".$idrecebedor."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-gateway','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				/* ALTERAR REGISTRO */
				
				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];
															
					$sql = mysqli_query($conn, "UPDATE `conf-gateway` SET `chaveapi`='".$chaveapi."',`chavecriptografia`='".$chavecriptografia."',`banco`='".$banco."',`agencia`='".$agencia."',`agenciadv`='".$agenciadv."',`conta`='".$conta."',`contadv`='".$contadv."',`tipoconta`='".$tipoconta."',`cpfcnpj`='".$cpfcnpj."',`nome`='".$nome."',`idconta`='".$idconta."',`idrecebedor`='".$idrecebedor."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-gateway','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				}
					
			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>