	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `conf-gateway`");

					$row = mysqli_fetch_array($sql);

					$id_registro = $row["id"];

				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="act" value="update" required>' : '<input type="hidden" name="act" value="store" required>');?>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="reg" value="'.$permissao_c.'" required>' : '<input type="hidden" name="reg" value="'.$permissao_u.'" required>');?>
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="chaveapi">Chave da API</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Chave da API" id="chaveapi" name="chaveapi" class="form-control" value="<?=$row["chaveapi"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="chavecriptografia">Chave da Criptografia</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Chave da Criptografia" id="chavecriptografia" name="chavecriptografia" class="form-control" value="<?=$row["chavecriptografia"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="banco">Número do Banco</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Número do Banco" id="banco" name="banco" class="form-control" value="<?=$row["banco"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="agencia">Número da Agência</label>
							<div class="col-sm-2">
								<input type="text" placeholder="Número da Agência" id="agencia" name="agencia" class="form-control" value="<?=$row["agencia"];?>" required>
							</div>
							<label class="col-sm-1 control-label" for="agenciadv">Dígito</label>
							<div class="col-sm-2">
								<input type="text" placeholder="Dígito Verificador" id="agenciadv" name="agenciadv" class="form-control" value="<?=$row["agenciadv"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="conta">Número da Conta</label>
							<div class="col-sm-2">
								<input type="text" placeholder="Número da Conta" id="conta" name="conta" class="form-control" value="<?=$row["conta"];?>" required>
							</div>
							<label class="col-sm-1 control-label" for="contadv">Dígito</label>
							<div class="col-sm-2">
								<input type="text" placeholder="Dígito Verificador" id="contadv" name="contadv" class="form-control" value="<?=$row["contadv"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tipoconta">Tipo de Conta</label>
							<div class="col-sm-8">
								<select name="tipoconta" id="tipoconta" data-placeholder="Selecione o tipo da conta" tabindex="0" data-width="100%">
									<option value="">Selecione o tipo da conta</option>
									<option value="conta_corrente" <?=($row["tipoconta"] == "conta_corrente" ? "selected" : "");?>>Conta Corrente</option>
									<option value="conta_poupanca" <?=($row["tipoconta"] == "conta_poupanca" ? "selected" : "");?>>Conta Poupança</option>
									<option value="conta_corrente_conjunta" <?=($row["tipoconta"] == "conta_corrente_conjunta" ? "selected" : "");?>>Conta Corrente Conjunta</option>
									<option value="conta_poupanca_conjunta" <?=($row["tipoconta"] == "conta_poupanca_conjunta" ? "selected" : "");?>>Conta Poupança Conjunta</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="cpfcnpj">CPF ou CNPJ</label>
							<div class="col-sm-8">
								<input type="text" placeholder="CPF ou CNPJ" id="cpfcnpj" name="cpfcnpj" class="form-control" value="<?=$row["cpfcnpj"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nome">Nome da Conta</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome da Conta" id="nome" name="nome" class="form-control" value="<?=$row["nome"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="idconta">ID da Conta do Gateway</label>
							<div class="col-sm-8">
								<input type="text" placeholder="ID da Conta do Gateway" id="idconta" name="idconta" class="form-control" value="<?=$row["idconta"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="idrecebedor">ID do Recebedor</label>
							<div class="col-sm-8">
								<input type="text" placeholder="ID do Recebedor" id="idrecebedor" name="idrecebedor" class="form-control" value="<?=$row["idrecebedor"];?>" required>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
							</div>
						</div>
					</div>
					
				</form>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>