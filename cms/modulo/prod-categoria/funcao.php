<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
												
				$diretorio = "../../../upload/banner/";
				
				$extensoes = array('jpeg','jpg','png');
								
				if ($act == "store") {
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe a descrição do categoria."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2000000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 2MB. O arquivo deve ser menor ou igual a 2MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}
					
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
										
					$qry = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `prod-categoria` WHERE `nome`='".$nome."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/3");

					} else {
						$sql = mysqli_query($conn, "INSERT INTO `prod-categoria`(`id`, `nome`, `imagem`, `status`, `created_at`) VALUES (0,'".$nome."','".$arquivo."','".$status."',NOW())");
						
						$targetDir = '../../../upload/produto/';

						if (!empty($_FILES)) {

							$targetFile = $targetDir . $arquivo;

							if(move_uploaded_file($temp, $targetFile)) {

								echo "<script>console.log('teste');</script>";
							}
						}

						if ($sql) {

							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-categoria','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
							header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
						} else {

							header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

						}
						
					}
										
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe a descrição do categoria."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2000000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 2MB. O arquivo deve ser menor ou igual a 2MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$qry = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `prod-categoria` WHERE `nome`='".$nome."' AND `id` <> '".$id_registro."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/3");

					} else {
						
						if($arquivo == "") {
							
							$sql = mysqli_query($conn, "UPDATE `prod-categoria` SET `nome`='".$nome."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
						} else {
												
							$sql = mysqli_query($conn, "UPDATE `prod-categoria` SET `nome`='".$nome."',`imagem`='".$arquivo."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
						}

						if ($sql) {
							
							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-categoria','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

							header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

						} else {

							header("Location: " . URL . "/".$mod."/listar/".$pag."/3");

						}
						
					}
					
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `prod-categoria` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-categoria','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>