<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");
				
				/* INSERIR REGISTRO */
				
				if ($act == "store") {

					if($_POST["pais"] == "") {
						echo('<script language = "javascript"> alert("Selecione o país."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$pais = mysqli_real_escape_string($conn, stripslashes(trim($_POST["pais"])));
					}

					if($_POST["estado"] == "") {
						echo('<script language = "javascript"> alert("Selecione a estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$estado = mysqli_real_escape_string($conn, stripslashes(trim($_POST["estado"])));
					}

					if($_POST["cidade"] == "") {
						echo('<script language = "javascript"> alert("Selecione a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cidade"])));
					}

					if($_POST["cnpj"] == "") {
						echo('<script language = "javascript"> alert("Informe o CNPJ da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cnpj = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cnpj"])));
					}
						
					if($_POST["inscricao"] == "") {
						$inscricao = mysqli_real_escape_string($conn, "ISENTO");
					} else {
						$inscricao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricao"])));
					}

					if($_POST["razaosocial"] == "") {
						echo('<script language = "javascript"> alert("Informe a razao social da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial"])));
					}
												
					if($_POST["fantasia"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome fantasia da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$fantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["fantasia"])));
					}
					
					$responsavel = mysqli_real_escape_string($conn, stripslashes(trim($_POST["responsavel"])));
						
					if($_POST["cep"] == "") {
						echo('<script language = "javascript"> alert("Informe o cep."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cep = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cep"])));
					}
						
					if($_POST["logradouro"] == "") {
						echo('<script language = "javascript"> alert("Informe o endereço."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$logradouro = mysqli_real_escape_string($conn, stripslashes(trim($_POST["logradouro"])));
					}
						
					if($_POST["numero"] == "") {
						echo('<script language = "javascript"> alert("Informe o número do endereço."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$numero = mysqli_real_escape_string($conn, stripslashes(trim($_POST["numero"])));
					}
						
					$complemento = mysqli_real_escape_string($conn, stripslashes(trim($_POST["complemento"])));
						
					if($_POST["bairro"] == "") {
						echo('<script language = "javascript"> alert("Informe o bairro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$bairro = mysqli_real_escape_string($conn, stripslashes(trim($_POST["bairro"])));
					}
						
					if($_POST["nomecidade"] == "") {
						echo('<script language = "javascript"> alert("Informe a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nomecidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomecidade"])));
					}
						
					if($_POST["nomeestado"] == "") {
						echo('<script language = "javascript"> alert("Informe o estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nomeestado = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomeestado"])));
					}
						
					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Informe um e-mail válido."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
					}
					
					$telefone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["telefone"])));
						
					$celular = mysqli_real_escape_string($conn, stripslashes(trim($_POST["celular"])));
						
					$whatsapp = mysqli_real_escape_string($conn, stripslashes(trim($_POST["whatsapp"])));
												
					$areamapa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["areamapa"])));
					
					$urlmapa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["urlmapa"])));
					
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$sql = mysqli_query($conn, "INSERT INTO `cad-info-contato`(`id`, `idpais`, `idestado`, `idcidade`, `cnpj`, `inscricao`, `razaosocial`, `fantasia`, `responsavel`, `cep`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `email`, `telefone`, `celular`, `whatsapp`, `areamapa`, `urlmapa`, `status`, `created_at`) VALUES (0,'".$pais."','".$estado."','".$cidade."','".$cnpj."','".$inscricao."','".$razaosocial."','".$fantasia."','".$responsavel."','".$cep."','".$logradouro."','".$numero."','".$complemento."','".$bairro."','".$nomecidade."','".$nomeestado."','".$email."','".$telefone."','".$celular."','".$whatsapp."','".$areamapa."','".$urlmapa."','".$status."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','cad-info-contato','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
											
				/* ALTERAR REGISTRO */	

				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];

					if($_POST["pais"] == "") {
						echo('<script language = "javascript"> alert("Selecione o país."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$pais = mysqli_real_escape_string($conn, stripslashes(trim($_POST["pais"])));
					}

					if($_POST["estado"] == "") {
						echo('<script language = "javascript"> alert("Selecione a estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$estado = mysqli_real_escape_string($conn, stripslashes(trim($_POST["estado"])));
					}

					if($_POST["cidade"] == "") {
						echo('<script language = "javascript"> alert("Selecione a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cidade"])));
					}

					if($_POST["cnpj"] == "") {
						echo('<script language = "javascript"> alert("Informe o CNPJ da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cnpj = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cnpj"])));
					}
						
					if($_POST["inscricao"] == "") {
						$inscricao = mysqli_real_escape_string($conn, "ISENTO");
					} else {
						$inscricao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricao"])));
					}

					if($_POST["razaosocial"] == "") {
						echo('<script language = "javascript"> alert("Informe a razao social da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial"])));
					}
												
					if($_POST["fantasia"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome fantasia da empresa."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$fantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["fantasia"])));
					}
					
					$responsavel = mysqli_real_escape_string($conn, stripslashes(trim($_POST["responsavel"])));
						
					if($_POST["cep"] == "") {
						echo('<script language = "javascript"> alert("Informe o cep."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cep = mysqli_real_escape_string($conn, stripslashes(trim($_POST["cep"])));
					}
						
					if($_POST["logradouro"] == "") {
						echo('<script language = "javascript"> alert("Informe o endereço."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$logradouro = mysqli_real_escape_string($conn, stripslashes(trim($_POST["logradouro"])));
					}
						
					if($_POST["numero"] == "") {
						echo('<script language = "javascript"> alert("Informe o número do endereço."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$numero = mysqli_real_escape_string($conn, stripslashes(trim($_POST["numero"])));
					}
						
					$complemento = mysqli_real_escape_string($conn, stripslashes(trim($_POST["complemento"])));
						
					if($_POST["bairro"] == "") {
						echo('<script language = "javascript"> alert("Informe o bairro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$bairro = mysqli_real_escape_string($conn, stripslashes(trim($_POST["bairro"])));
					}
						
					if($_POST["nomecidade"] == "") {
						echo('<script language = "javascript"> alert("Informe a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nomecidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomecidade"])));
					}
						
					if($_POST["nomeestado"] == "") {
						echo('<script language = "javascript"> alert("Informe o estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nomeestado = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomeestado"])));
					}
						
					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Informe um e-mail válido."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
					}
					
					$telefone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["telefone"])));
						
					$celular = mysqli_real_escape_string($conn, stripslashes(trim($_POST["celular"])));
						
					$whatsapp = mysqli_real_escape_string($conn, stripslashes(trim($_POST["whatsapp"])));
												
					$areamapa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["areamapa"])));
					
					$urlmapa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["urlmapa"])));
					
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$sql = mysqli_query($conn, "UPDATE `cad-info-contato` SET `idpais`='".$pais."',`idestado`='".$estado."',`idcidade`='".$cidade."',`cnpj`='".$cnpj."',`inscricao`='".$inscricao."',`razaosocial`='".$razaosocial."',`fantasia`='".$fantasia."',`responsavel`='".$responsavel."',`cep`='".$cep."',`logradouro`='".$logradouro."',`numero`='".$numero."',`complemento`='".$complemento."',`bairro`='".$bairro."',`cidade`='".$nomecidade."',`estado`='".$nomeestado."',`email`='".$email."',`telefone`='".$telefone."',`celular`='".$celular."',`whatsapp`='".$whatsapp."',`areamapa`='".$areamapa."',`urlmapa`='".$urlmapa."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','cad-info-contato','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/2");

					}
					
				/* APAGAR REGISTRO */	
					
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `cad-info-contato` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','cad-info-contato','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>