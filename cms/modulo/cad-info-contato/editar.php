	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_u); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `cad-info-contato` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="update" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_u;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="pais">País</label>
							<div class="col-sm-8">
								<select name="pais" id="pais" data-placeholder="Selecione o país" tabindex="0" data-width="100%">
									<?php
									$pais = mysqli_query($conn, "SELECT * FROM `local-pais` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($pais) > 0) {
									?>
									<option value=""></option>
									<?php
									while($ln1 = mysqli_fetch_array($pais)){
									?>	
									<option value="<?=$ln1["id"];?>" <?=($ln1["id"] == $row["idpais"] ? "selected" : "");?>><?=$ln1["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um país para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="estado">Estado</label>
							<div class="col-sm-8">
								<select name="estado" id="estado" data-placeholder="Selecione o estado" tabindex="0" data-width="100%">
									<?php
									$estado = mysqli_query($conn, "SELECT * FROM `local-estado` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($estado) > 0) {
									?>
									<option value=""></option>
									<?php
									while($ln2 = mysqli_fetch_array($estado)){
									?>	
									<option value="<?=$ln2["id"];?>" <?=($ln2["id"] == $row["idestado"] ? "selected" : "");?>><?=$ln2["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um estado para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="cidade">Cidade</label>
							<div class="col-sm-8">
								<select name="cidade" id="cidade" data-placeholder="Selecione a cidade" tabindex="0" data-width="100%">
									<?php
									$cidade = mysqli_query($conn, "SELECT * FROM `local-cidade` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($cidade) > 0) {
									?>
									<option value=""></option>
									<?php
									while($ln3 = mysqli_fetch_array($cidade)){
									?>	
									<option value="<?=$ln3["id"];?>" <?=($ln3["id"] == $row["idcidade"] ? "selected" : "");?>><?=$ln3["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos uma cidade para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						
						<div id="statuscnpj"></div>

						<div class="form-group">
							<label class="col-sm-2 control-label" for="cnpj">CNPJ</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o CNPJ da empresa, para validação dos dados abaixo" id="cnpj" name="cnpj" value="<?=$row["cnpj"];?>" class="form-control" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="inscricao">I.E. ou I.M.</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a inscrição estadual ou municipal da empresa, caso exista" id="inscricao" name="inscricao" value="<?=$row["inscricao"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="razaosocial">Razão Social</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a razão social da empresa" id="razaosocial" name="razaosocial" class="form-control" value="<?=$row["razaosocial"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="fantasia">Nome da Fantasia</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o nome fantasia da empresa" id="fantasia" name="fantasia" class="form-control" value="<?=$row["fantasia"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="responsavel">Responsável</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o nome do responsável pela empresa" id="responsavel" name="responsavel" class="form-control" value="<?=$row["responsavel"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="cep">CEP</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o CEP do endereço" id="cep" name="cep" maxlength="9" class="form-control" value="<?=$row["cep"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="logradouro">Logradouro</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o endereço a ser cadastrado" id="logradouro" name="logradouro" class="form-control" value="<?=$row["logradouro"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="numero">Número</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o número do estabelecimento" id="numero" name="numero" class="form-control" value="<?=$row["numero"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="complemento">Complemento</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o complemento, se existir" id="complemento" name="complemento" value="<?=$row["complemento"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="bairro">Bairro</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o bairro" id="bairro" name="bairro" class="form-control" value="<?=$row["bairro"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nomecidade">Cidade</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a cidade" id="nomecidade" name="nomecidade" class="form-control" value="<?=$row["cidade"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nomeestado">Estado</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o estado" id="nomeestado" name="nomeestado" class="form-control" value="<?=$row["estado"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="email">E-mail</label>
							<div class="col-sm-8">
								<input type="email" placeholder="Informe o endereço de e-mail principal" id="email" name="email" value="<?=$row["email"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="telefone">Fone Principal</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o número de telefone principal" id="telefone" name="telefone" value="<?=$row["telefone"];?>" class="form-control" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="celular">Celular</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe um número de celular" id="celular" name="celular" value="<?=$row["celular"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="whatsapp">WhatsApp</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe um número para contato através do WhatsApp" id="whatsapp" name="whatsapp" value="<?=$row["whatsapp"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="areamapa">Mapa</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o código do mapa gerado pelo Google Maps" id="areamapa" name="areamapa" value="<?=$row["areamapa"];?>" class="form-control">
							</div>
						</div>
						<?php if(isset($row["areamapa"]) && $row["areamapa"] != "" &&  $row["areamapa"] != NULL) { ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="areamapa"></label>
							<div class="col-sm-8">
								<iframe src="<?=$row["areamapa"];?>" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="urlmapa">URL do Mapa</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a URL minificada do mapa gerado pelo Google Maps" id="urlmapa" name="urlmapa" value="<?=$row["urlmapa"];?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Status do Registro</label>
							<div class="col-md-9">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" <?=($row["status"] == "S" ? "checked" : "");?>>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N" <?=($row["status"] == "N" ? "checked" : "");?>>
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
							</div>
						</div>
					</div>
					
				</form>
				
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>