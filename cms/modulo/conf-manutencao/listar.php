	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `conf-manutencao`");

					$row = mysqli_fetch_array($sql);

					$id_registro = $row["id"];

				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="act" value="update" required>' : '<input type="hidden" name="act" value="store" required>');?>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="reg" value="'.$permissao_c.'" required>' : '<input type="hidden" name="reg" value="'.$permissao_u.'" required>');?>
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Modo de Manutenção</label>
							<div class="col-md-9">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" <?=($row["status"] == "S" ? "checked" : "");?>>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N" <?=($row["status"] == "N" ? "checked" : "");?>>
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
							</div>
						</div>
					</div>
					
				</form>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>