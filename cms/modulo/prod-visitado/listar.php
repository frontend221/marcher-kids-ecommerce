	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				                
				<!-- BODY -->
				
				<div class="pad-btm form-inline">
					<div class="row">
					    <div class="col-sm-6 table-toolbar-left">

						</div>
					    <div class="col-sm-6 table-toolbar-right"></div>
					</div>
				</div>
				
                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="22%">Produto</th>
					            <th class="text-center" width="12%" style="text-align: center !important;">Visualizações</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "t.*, SUM(t.produto) AS totalproduto, p.`nome` AS nomeproduto";
							
							$where = "";
							
							$final = "FROM `prod-visitado` AS t INNER JOIN `prod-item` AS p ON t.`produto`=p.`id` $where GROUP BY t.`produto` DESC";
							
							$maximo	= 60;

							$inicio = $exp[2] - 1;
							$inicio = $maximo * $inicio;

							$count = "SELECT COUNT(*) AS 'NUM_ROWS' $final";
							$query = mysqli_query($conn, $count);
							$row = mysqli_fetch_array($query);
							$total = (($row["NUM_ROWS"] == "" || $row["NUM_ROWS"] ==  NULL) ? "0" : $row["NUM_ROWS"]);

							$sql = mysqli_query($conn, "SELECT $campos $final");
							
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["nomeproduto"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["totalproduto"];?></td>
					        </tr>
							
							<?php } ?>

						</tbody>
						
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>