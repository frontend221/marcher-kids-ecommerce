	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `conf-smtp`");

					$row = mysqli_fetch_array($sql);

					$id_registro = $row["id"];

				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="act" value="update" required>' : '<input type="hidden" name="act" value="store" required>');?>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="reg" value="'.$permissao_c.'" required>' : '<input type="hidden" name="reg" value="'.$permissao_u.'" required>');?>
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nome">Nome para Exibição</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome para Exibição no Remetente" id="nome" name="nome" class="form-control" value="<?=$row["nome"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="host">Host</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome do host ou IP para conexão" id="host" name="host" class="form-control" value="<?=$row["host"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="porta">Porta</label>
							<div class="col-sm-8">
								<input type="number" min="0" placeholder="Porta para conexão" id="porta" name="porta" class="form-control" maxlength="5" value="<?=$row["porta"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="usuario">Usuário</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Usuário para autenticação no servidor" id="usuario" name="usuario" class="form-control" value="<?=$row["usuario"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="senha">Senha</label>
							<div class="col-sm-8">
								<input type="password" placeholder="Senha do usuário" id="senha" name="senha" class="form-control" value="<?=base64_decode($row["senha"]);?>" required>
								<small class="help-block"><a style="color: #C90D10;" href="#" onclick="verSenhaSMTP()">[ ver senha digitada ]</a></small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="seguranca">Segurança</label>
							<div class="col-sm-8">
								<select name="seguranca" id="seguranca" data-placeholder="Selecione o tipo de segurança" tabindex="0" data-width="100%">
									<option value="0" <?=($row["seguranca"] == "" ? "selected" : "");?>>Nenhuma</option>
									<option value="ssl" <?=($row["seguranca"] == "ssl" ? "selected" : "");?>>SSL</option>
									<option value="tls" <?=($row["seguranca"] == "tls" ? "selected" : "");?>>TLS</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
							</div>
						</div>
					</div>
					
				</form>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>