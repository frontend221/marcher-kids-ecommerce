<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");
				
				if($_POST["nome"] == "") {
					echo('<script language = "javascript"> alert("Informe o nome para exibição no remetente."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
				}
						
				if($_POST["host"] == "") {
					echo('<script language = "javascript"> alert("Informe o host para conexão."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$host = mysqli_real_escape_string($conn, stripslashes(trim($_POST["host"])));
				}
						
				if($_POST["porta"] == "") {
					echo('<script language = "javascript"> alert("Informe a porta para conexão."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$porta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["porta"])));
				}
					
				if($_POST["usuario"] == "") {
					echo('<script language = "javascript"> alert("Informe o usuário para autenticação no servidor remoto."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$usuario = mysqli_real_escape_string($conn, stripslashes(trim($_POST["usuario"])));
				}
					
				if($_POST["senha"] == "") {
					echo('<script language = "javascript"> alert("Informe a senha do usuário para autenticação."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$senha = base64_encode(mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha"]))));
				}
										
				if($_POST["seguranca"] == "") {
					echo('<script language = "javascript"> alert("Selecione o tipo de segurança."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$seguranca = mysqli_real_escape_string($conn, stripslashes(trim($_POST["seguranca"])));
				}

				
				/* INSERIR REGISTRO */
				
				if ($act == "store") {
										
					$sql = mysqli_query($conn, "INSERT INTO `conf-smtp`(`id`, `nome`, `host`, `porta`, `usuario`, `senha`, `seguranca`, `created_at`) VALUES (0,'".$nome."','".$host."','".$porta."','".$usuario."','".$senha."','".$seguranca."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-smtp','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				/* ALTERAR REGISTRO */
				
				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];
															
					$sql = mysqli_query($conn, "UPDATE `conf-smtp` SET `nome`='".$nome."',`host`='".$host."',`porta`='".$porta."',`usuario`='".$usuario."',`senha`='".$senha."',`seguranca`='".$seguranca."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-smtp','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					}
					
				}
					
			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>