	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_c); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `usu-sistema` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="update" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_u;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tipo">Tipo do Usuário</label>
							<div class="col-sm-8">
								<select name="tipo" id="tipo" data-placeholder="Selecione o tipo de usuário" tabindex="0" data-width="100%">
									<?php
									$menu = mysqli_query($conn, "SELECT * FROM `usu-tipo` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($menu) > 0) {
									?>
									<option value=""></option>
									<?php
									while($ln = mysqli_fetch_array($menu)){
									?>	
									<option value="<?=$ln["id"];?>" <?=($ln["id"] == $row["tipo"] ? "selected" : "");?>><?=$ln["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um tipo de usuário para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nome">Nome</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome" id="nome" name="nome" class="form-control" value="<?=$row["nome"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="email">E-mail</label>
							<div class="col-sm-8">
								<input type="email" placeholder="E-mail do Usuário" id="email" name="email" class="form-control" value="<?=$row["email"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="login">Login</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Login do Usuário" id="login" name="login" class="form-control" value="<?=$row["login"];?>" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Status do Registro</label>
							<div class="col-md-9">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" <?=($row["status"] == "S" ? "checked" : "");?>>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N" <?=($row["status"] == "N" ? "checked" : "");?>>
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
							</div>
						</div>
					</div>
					
				</form>
				
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>