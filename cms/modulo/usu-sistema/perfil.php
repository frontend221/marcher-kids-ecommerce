	<?php

		if (preg_match("/perfil.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
												   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
								
				<!-- BODY -->
				
				<?php

				$sql = mysqli_query($conn, "SELECT u.*, t.`nome` AS nometipo FROM `usu-sistema` AS u INNER JOIN `usu-tipo` AS t ON u.`tipo`=t.`id` WHERE u.`id`='".$exp[2]."' AND u.`token`='".$_SESSION["token"]."' AND u.`deleted_at` IS NULL");
													
				if(mysqli_num_rows($sql) == 1) {
						
				$row = mysqli_fetch_array($sql);
						
				$id_registro = $row["id"];
                
				?>
				
				<div class="container">
													
					<div class="table-responsive">

						<table id="item" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th colspan="2" align="center" valign="middle" style="text-align: center;">Perfil do Usuário</th>
								</tr>
							</thead>
							<tbody id="myTable">
								<tr>
									<td width="17%"><strong>ID do Usuário</strong></td>
									<td><?=$row["id"];?></td>
								</tr>
								<tr>
									<td><strong>Nome do Usuário</strong></td>
									<td><?=$row["nome"];?></td>
								</tr>
								<tr>
									<td><strong>Tipo do Usuário</strong></td>
									<td><?=$row["nometipo"];?></td>
								</tr>
								<tr>
									<td><strong>E-mail do Usuário</strong></td>
									<td><?=$row["email"];?></td>
								</tr>
								<tr>
									<td><strong>Login do Usuário</strong></td>
									<td><?=$row["login"];?></td>
								</tr>
								<tr>
									<td><strong>Status do Usuário</strong></td>
									<td><?=($row["status"] == "S" ? "<div class='label label-table label-success'>Ativo</div>" : "<div class='label label-table label-danger'>Inativo</div>");?></td>
								</tr>
							</tbody>
						</table>
						<tfoot>
							<tr>
								<td colspan="2"><a href="<?php echo URL ?>/<?=$exp[0];?>/listar/1" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a></td>
							</tr>
						</tfoot>					

					</div>
					
				</div>
										
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/1" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>