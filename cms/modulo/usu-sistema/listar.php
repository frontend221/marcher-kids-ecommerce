	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<div class="pad-btm form-inline">
					<div class="row">
					    <div class="col-sm-6 table-toolbar-left">
					        <?php if(in_array($permissao_c, $permissao)){ ?>
							<a href="<?php echo URL ?>/<?=$exp[0];?>/adicionar/<?=$exp[2];?>" id="demo-btn-addrow" class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Adicionar novo registro">
								<i class="fas fa-plus-circle"></i> Adicionar
                            </a>
							<?php } ?>
					    </div>
					    <div class="col-sm-6 table-toolbar-right"></div>
					</div>
				</div>
				
                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="18%">Nome</th>
					            <th width="18%">E-mail</th>
					            <th width="18%">Login</th>
					            <th width="18%" style="text-align: center !important;">Tipo de Usuário</th>
					            <th width="8%" style="text-align: center !important;">Status</th>
					            <th width="12%" style="text-align: center !important;">Ações</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "u.*, t.`nome` AS tipo";
							
							$where = "WHERE u.`deleted_at` IS NULL";
							
							$final = "FROM `usu-sistema` AS u INNER JOIN `usu-tipo` AS t ON u.`tipo`=t.`id` $where ORDER BY u.`id` DESC";
							
							$sql = mysqli_query($conn, "SELECT $campos $final");
							
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["nome"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["email"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["login"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<span class="label label-default"><?=$ln["tipo"];?></span>
								</td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<?=($ln["status"] == "S" ? "<div class='label label-table label-success'>Ativo</div>" : "<div class='label label-table label-danger'>Inativo</div>");?>
								</td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<div class="btn-group">
										
										<!-- Troca de senha -->
										
										<?php if($_SESSION["login"] != $ln["login"]){ ?>
										
										<a href="<?php echo URL ?>/<?=$exp[0];?>/senha/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-info add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Informações do registro">
											<i class="fas fa-key"></i>
										</a>
										
										<?php } else { ?>
										
										<a href="#" class="btn btn-default">
											<i class="fas fa-lock"></i>
										</a>
										
										<?php } ?>
										
										<!-- Editar Perfil -->
										
										<?php if($_SESSION["login"] != $ln["login"]){ ?>
										
										<a href="<?php echo URL ?>/<?=$exp[0];?>/editar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Editar registro">
											<i class="far fa-edit"></i>
										</a>
										
										<?php } else { ?>
										
										<a href="#" class="btn btn-default">
											<i class="fas fa-lock"></i>
										</a>
										
										<?php } ?>
										
										<!-- Apagar Usuário -->
										
										<?php if($_SESSION["login"] == "masteradmin"){ ?>
										
											<?php if($_SESSION["login"] != $ln["login"]){ ?>
										
											<a href="<?php echo URL ?>/<?=$exp[0];?>/apagar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Apagar registro">
												<i class="far fa-trash-alt"></i>
											</a>

											<?php } else { ?>

											<a href="#" class="btn btn-default">
												<i class="fas fa-lock"></i>
											</a>

											<?php } ?>

										<?php } else { ?>
										
										<a href="#" class="btn btn-default">
											<i class="fas fa-lock"></i>
										</a>
										
										<?php } ?>
									</div>
								</td>
					        </tr>
							
							<?php } ?>

						</tbody>
						
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>