<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn == $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");
				
				/* VALIDA E-MAIL */
				
				function ValidaEmail($email) {
					if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $email)) {
						return true;
					} else {
						return false;
					}
				}
				
				/* INSERIR REGISTRO */
				
				if ($act == "store") {
					
					if($_POST["tipo"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tipo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tipo"])));
					}
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Digite o nome do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
					
					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Digite o e-mail do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
					}
						
					if($_POST["login"] == "") {
						echo('<script language = "javascript"> alert("Digite um login para o usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$login = mysqli_real_escape_string($conn, stripslashes(trim($_POST["login"])));
					}
						
					if($_POST["senha1"] == "") {
						echo('<script language = "javascript"> alert("Digite a senha para o usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha1 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha1"])));
					}
						
					if($_POST["senha2"] == "") {
						echo('<script language = "javascript"> alert("Confirme a senha digitada para este usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha2 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha2"])));
					}
						
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					if($senha1 === $senha2) {
						
						if(ValidaEmail($email)) { 
							
							/* ********** VERIFICA SE HÁ REGISTRO COM O MESMO NOME ********** */

							$qry = mysqli_query($conn, "SELECT COUNT(email) AS TOTAL FROM `usu-sistema` WHERE `email`='".$email."' AND `deleted_at` IS NULL");
							$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

								header("Location: " . URL . "/".$mod."/adicionar/".$pag."/1/3");

							} else {

								$qry = mysqli_query($conn, "SELECT COUNT(login) AS TOTAL FROM `usu-sistema` WHERE `login`='".$login."' AND `deleted_at` IS NULL");
								$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

									header("Location: " . URL . "/".$mod."/adicionar/".$pag."/1/4");

								} else {
																		
									$sql = mysqli_query($conn, "INSERT INTO `usu-sistema`(`id`, `tipo`, `nome`, `email`, `login`, `senha`, `token`, `status`, `created_at`) VALUES (0,'".$tipo."','".$nome."','".$email."','".$login."','".sha1($senha2)."','','".$status."',NOW())");

									if ($sql) {

										$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usu-sistema`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','usuario','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
										header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
									} else {

										header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

									}
									
								}

							}
					
						} else {
							echo('<script language = "javascript">alert("O e-mail informado não é um endereço válido.")</script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
						
					} else {
						echo('<script language = "javascript">alert("As senhas digitadas não coincidem.")</script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					}
					
				/* ALTERAR REGISTRO */	

				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];
					
					if($_POST["tipo"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tipo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tipo"])));
					}
					
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Digite o nome do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}

					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Digite o e-mail do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
					}
																		
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					if(ValidaEmail($email)) { 
							
						/* ********** VERIFICA SE HÁ REGISTRO COM O MESMO NOME ********** */

						$qry = mysqli_query($conn, "SELECT COUNT(email) AS TOTAL FROM `usu-sistema` WHERE `email`='".$email."' AND `deleted_at` IS NULL AND `id` <> '".$id_registro."'");
						$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

							header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/3");

						} else {

							$sql = mysqli_query($conn, "UPDATE `usu-sistema` SET `tipo`='".$tipo."',`nome`='".$nome."',`email`='".$email."',`status`='".$status."' WHERE `id`='".$id_registro."'");

							if ($sql) {
								
								$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usu-sistema`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','usuario','".utf8_encode("Alteração do registro ID: ".$id_registro."")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
								header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
							} else {

								header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/2");

							}

						}
					
					} else {
						echo('<script language = "javascript">alert("O e-mail informado não é um endereço válido.")</script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					}
					
				/* ALTERAR SENHA DESTE REGISTRO */	

				} else if ($act == "password") {
					
					$id_registro = $_POST["rid"];
					
					if($_POST["atual"] == "") {
						echo('<script language = "javascript"> alert("Digite a senha atual do usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$atual = mysqli_real_escape_string($conn, stripslashes(trim($_POST["atual"])));
					}
						
					if($_POST["senha1"] == "") {
						echo('<script language = "javascript"> alert("Digite a nova senha para o usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha1 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha1"])));
					}
						
					if($_POST["senha2"] == "") {
						echo('<script language = "javascript"> alert("Confirme a nova senha digitada para este usuário."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha2 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha2"])));
					}
					
					/* ********** VERIFICA SENHA ATUAL ********** */

					$qry = mysqli_query($conn, "SELECT COUNT(senha) AS TOTAL FROM `usu-sistema` WHERE `senha`='".sha1($atual)."' AND `status`='S' AND `deleted_at` IS NULL AND `id`='".$id_registro."'");
					$tot = mysqli_num_rows($qry);

					if($tot == 0) {

						header("Location: " . URL . "/".$mod."/senha/".$pag."/1/".$id_registro."/3");

					} else {
						
						if($atual === $senha2) {
							
							header("Location: " . URL . "/".$mod."/senha/".$pag."/1/".$id_registro."/4");
							
						} else {
							
							if($senha1 === $senha2) {
								
								$sql = mysqli_query($conn, "UPDATE `usu-sistema` SET `senha`='".sha1($senha2)."' WHERE `id`='".$id_registro."'");

								if ($sql) {
									
									$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usu-sistema`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','usuario','".utf8_encode("Alterada a senha do usuário ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
									header("Location: " . URL . "/".$mod."/senha/".$pag."/".$id_registro."/1");
									
								} else {

									header("Location: " . URL . "/".$mod."/senha/".$pag."/".$id_registro."/2");

								}
								
							} else {
								echo('<script language = "javascript">alert("As senhas digitadas não coincidem.")</script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
							
						}
						
					}					
					
				/* APAGAR REGISTRO */	
					
				} else if ($act == "delete") {
					
					if($_SESSION["login"] == "masteradmin") {

						$id_registro = $_POST["rid"];

						$sql = mysqli_query($conn, "UPDATE `usu-sistema` SET `status`='N',`deleted_at`=NOW() WHERE `id`='".$id_registro."'");

						if ($sql) {

							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usu-sistema`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','usuario','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
							header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
						} else {
						
							header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

						}
						
					} else {
						
						header("Location: " . URL_GERAL . "/?mod=".$mod."&act=lst&tkn=".$tkn."&pag=".$pag."&msg=_permission_error");
						
					}
					
				}
					
			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>