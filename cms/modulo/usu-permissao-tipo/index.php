<?php

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	require_once ("../../config/cfg-path.php");

	if(!(isset($_SESSION["id"]) and isset($_SESSION["nome"]) and isset($_SESSION["email"]) and isset($_SESSION["login"]) and isset($_SESSION["tipo"]) and isset($_SESSION["imagem"]) and isset($_SESSION["token"]) and isset($_SESSION["status"])  and isset($_SESSION["time"]))) {
		
		session_destroy();
		header("Location: " . URL . "/login.php?msg=_autenticar");
  	
	} else {
		
		header("Location: " . URL . "/dashboard/listar/1"); 
	}

?>