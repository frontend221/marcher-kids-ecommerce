<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");

				if ($act == "update") {
					
					$id = mysqli_real_escape_string($conn, stripslashes(trim($_POST["rid"])));
					
					foreach($_POST["diretorio-nome"] as $diretorio) { 
						
						if($_POST[$diretorio."-h"] == true) { $habilitado = "S"; } else { $habilitado = "N" ;}
						if($_POST[$diretorio."-c"] == true) { $criar = "S"; } else { $criar = "N" ;}
						if($_POST[$diretorio."-r"] == true) { $ler = "S"; } else { $ler = "N"; }
						if($_POST[$diretorio."-u"] == true) { $editar = "S"; } else { $editar = "N"; }
						if($_POST[$diretorio."-d"] == true) { $apagar = "S"; } else { $apagar = "N"; }
						
						$sql = mysqli_query($conn, "SELECT * FROM `usu-permissao-tipo` WHERE `tipo`='".$id."'");

						while($ln = mysqli_fetch_array($sql)){
							
							if($ln["diretorio"] == $diretorio) {
																
								$exec = mysqli_query($conn, "UPDATE `usu-permissao-tipo` SET `habilitado`='".$habilitado."',`criar`='".$criar."',`ler`='".$ler."',`alterar`='".$editar."',`apagar`='".$apagar."' WHERE `id`='".$ln["id"]."'");
								
							}

						}

						if($exec) {
							
							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','usu-permissao-tipo','".utf8_encode("Alterando permissões do usuário ID: ".$id."")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

							header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id."/1");

						} else {

							header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id."/2");

						}
						
					}
						
				} else {
					
					header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id."/3");
						
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>