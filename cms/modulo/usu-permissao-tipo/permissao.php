	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
			
            <div class="panel-body">

			<?php

			if($tkn === $_SESSION["token"]){
				
				include ("../../config/cfg-path.php");

				include ("../../config/cfg-database.php");

				$sql = mysqli_query($conn, "SELECT `id`, `diretorio` FROM `conf-modulo`");

				while($ln = mysqli_fetch_array($sql)) {

					$sql1 = mysqli_query($conn, "SELECT DISTINCT `id` FROM `usu-tipo` WHERE `id`='".$exp[3]."'");

					while($ln1 = mysqli_fetch_array($sql1)) {
						
						$q = mysqli_query($conn, "INSERT INTO `usu-permissao-tipo`(`id`, `tipo`, `modulo`, `diretorio`, `habilitado`, `criar`, `ler`, `alterar`, `apagar`) VALUES (0,'".$ln1["id"]."','".$ln["id"]."','".$ln["diretorio"]."','N','N','N','N','N')");

					}

				}

				if($q) {
					
				?>

				<div class="alert alert-success" role="alert">
					Permissões executadas com <strong>sucesso</strong>!
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/editar/<?=$exp[2];?>/<?=$ln1["id"];?>" class="btn btn-warning" type="reset"><i class="fas fa-undo-alt"></i> Voltar</a>
				
				<?php

				} else {
					
				?>
				
				<div class="alert alert-danger" role="alert">
					Ocorreu um <strong>erro</strong> ao processar o lote de permissões.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning" type="reset"><i class="fas fa-undo-alt"></i> Voltar</a>
				
				<?php

				}

			} else {
			
			?>
				
			<div class="alert alert-danger" role="alert">
				Acesso <strong>negado</strong>! Token inválido.
			</div>
				
			<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning" type="reset"><i class="fas fa-undo-alt"></i> Voltar</a>
				
			<?php	
				
			}
			
			?>

			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>