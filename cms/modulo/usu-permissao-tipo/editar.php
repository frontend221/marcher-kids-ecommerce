	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
			
            <div class="panel-body">
				
				<?php $permissao = array($permissao_u); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
					
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="act" value="update" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_u;?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="rid" value="<?=$exp[3];?>" required>
					
					<div class="table-responsive">

						<table id="item" class="table table-striped" style="width:100%">
							<thead>

								<?php

									$query = mysqli_query($conn, "SELECT `nome` FROM `usu-tipo` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
									$linha = mysqli_fetch_array($query);

								?>

								<tr>
									<th colspan="7"><span style="font-size: 11px; text-transform: uppercase">Tipo do Usuário:</span> <span style="font-size: 14px; text-transform: uppercase"><strong><?=$linha["nome"]; ?></strong></span></th>
								</tr>
								<tr>
									<th colspan="2" align="center" valign="middle" style="text-align: center;"><span style="font-size: 11px; text-transform: uppercase">Dados do Módulo</span></th>
									<th colspan="5" align="center" valign="middle" style="text-align: center;"><span style="font-size: 11px; text-transform: uppercase">Permissões</span></th>
								</tr>
								<tr>
									<th width="21%"><span style="font-size: 11px; text-transform: uppercase">Menu</span></th>
									<th width="21%"><span style="font-size: 11px; text-transform: uppercase">Descrição</span></th>
									<th width="13%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">ID do Módulo</span></th>
									<th width="13%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">Habilitado?</span></th>
									<th width="8%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">Criar</span></th>
									<th width="8%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">Ler</span></th>
									<th width="8%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">Editar</span></th>
									<th width="8%" style="text-align: center !important;"><span style="font-size: 11px; text-transform: uppercase">Apagar</span></th>
								</tr>
							</thead>
							<tbody id="myTable">

								<?php

								$sql = mysqli_query($conn, "SELECT t.*, m.`id` AS idmodulo, m.`descricao` AS descricao, m.`titulo` AS titulo FROM `usu-permissao-tipo` AS t INNER JOIN `conf-modulo` AS m ON t.`diretorio`=m.`diretorio` WHERE t.`tipo`='".$exp[3]."' ORDER BY m.`titulo` ASC");

								if(mysqli_num_rows($sql) > 0) {

									while($row = mysqli_fetch_array($sql)) {	

								?>

								<tr>
									<input type="hidden" id="diretorio-nome" name="diretorio-nome[]" value="<?=$row["diretorio"];?>" required>
									<td style="vertical-align: middle !important;"><?=$row["titulo"];?></td>
									<td style="vertical-align: middle !important;"><?=$row["descricao"];?></td>
									<td style="text-align: center !important; vertical-align: middle !important;"><?=$row["idmodulo"];?></td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<div class="checkbox required">
											<input id="<?=$row["diretorio"]."-h";?>" class="magic-checkbox" type="checkbox" name="<?=$row["diretorio"]."-h";?>"<?=($row["habilitado"] == "S" ? "checked" : "");?>>
											<label for="<?=$row["diretorio"]."-h";?>"></label>
										</div>
									</td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<div class="checkbox required">
											<input id="<?=$row["diretorio"]."-c";?>" class="magic-checkbox" type="checkbox" name="<?=$row["diretorio"]."-c";?>"<?=($row["criar"] == "S" ? "checked" : "");?>>
											<label for="<?=$row["diretorio"]."-c";?>"></label>
										</div>
									</td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<div class="checkbox required">
											<input id="<?=$row["diretorio"]."-r";?>" class="magic-checkbox" type="checkbox" name="<?=$row["diretorio"]."-r";?>"<?=($row["ler"] == "S" ? "checked" : "");?>>
											<label for="<?=$row["diretorio"]."-r";?>"></label>
										</div>
									</td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<div class="checkbox required">
											<input id="<?=$row["diretorio"]."-u";?>" class="magic-checkbox" type="checkbox" name="<?=$row["diretorio"]."-u";?>"<?=($row["alterar"] == "S" ? "checked" : "");?>>
											<label for="<?=$row["diretorio"]."-u";?>"></label>
										</div>
									</td>
									<td style="text-align: center !important; vertical-align: middle !important;">
										<div class="checkbox required">
											<input id="<?=$row["diretorio"]."-d";?>" class="magic-checkbox" type="checkbox" name="<?=$row["diretorio"]."-d";?>"<?=($row["apagar"] == "S" ? "checked" : "");?>>
											<label for="<?=$row["diretorio"]."-d";?>"></label>
										</div>
									</td>
								</tr>

								<?php

									}

								} else {

								?>

								<tr>
									<td colspan="8" style="text-align: center;">Não existem registros cadastrados para exibição.</td>
								</tr>
							
								<?php if($_SESSION["login"] == "masteradmin") { ?>
								<tr>
									<td colspan="7" style="text-align: center !important; padding: 30px 0px !important;"><a href="<?php echo URL ?>/<?=$exp[0];?>/permissao/<?=$exp[2];?>/<?=$exp[3];?>" class="btn btn-primary">Gerar Permissões</a></td>
								</tr>
								<?php } ?>

								<?php

								}

								?>

							</tbody>
						</table>
						<tfoot>
							<tr>
								<td colspan="8">
									<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
									<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
								</td>
							</tr>
						</tfoot>

	                </div>
					
				</form>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>