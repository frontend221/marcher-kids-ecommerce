<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
												
				$diretorio = "../../../upload/carrossel/";
				
				$extensoes = array('jpeg','jpg','png');
								
				if ($act == "store") {
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2200000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 3MB. O arquivo deve ser menor ou igual a 3MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					} else {

						echo('<script language = "javascript"> alert("Selecione uma imagem para o fundo do carrossel."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;

					}
					
					if($_POST["somenteimagem"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo de carrossel."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$somenteimagem = mysqli_real_escape_string($conn, stripslashes(trim($_POST["somenteimagem"])));
					}
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título para a identificação da imagem."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
										
					$subtitulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["subtitulo"])));
										
					$url = mysqli_real_escape_string($conn, stripslashes(trim($_POST["urlimg"])));
										
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a posição em que o slide aparecerá no carrossel."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
										
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
															
					$sql = mysqli_query($conn, "INSERT INTO `arq-carrossel`(`id`, `somenteimagem`, `titulo`, `subtitulo`, `imagem`, `url`, `posicao`, `status`, `created_at`) VALUES (0,'".$somenteimagem."','".$titulo."','".$subtitulo."','".$arquivo."','".$url."','".$posicao."','".$status."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-carrossel','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2200000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 3MB. O arquivo deve ser menor ou igual a 3MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}
					
					if($_POST["somenteimagem"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo de carrossel."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$somenteimagem = mysqli_real_escape_string($conn, stripslashes(trim($_POST["somenteimagem"])));
					}
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título para a identificação da imagem."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
										
					$subtitulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["subtitulo"])));
										
					$url = mysqli_real_escape_string($conn, stripslashes(trim($_POST["urlimg"])));
										
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a posição em que o slide aparecerá no carrossel."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
										
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					if($arquivo == "") {
												
						$sql = mysqli_query($conn, "UPDATE `arq-carrossel` SET `somenteimagem`='".$somenteimagem."',`titulo`='".$titulo."',`subtitulo`='".$subtitulo."',`url`='".$url."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
					} else {
												
						$sql = mysqli_query($conn, "UPDATE `arq-carrossel` SET `somenteimagem`='".$somenteimagem."',`titulo`='".$titulo."',`subtitulo`='".$subtitulo."',`imagem`='".$arquivo."',`url`='".$url."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
					}

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-carrossel','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					}
						
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `arq-carrossel` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-carrossel','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>