	<?php

		if (preg_match("/detalhar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

	<?php $permissao = array($permissao_u); ?>
	<?php if(in_array("S", $permissao)){ ?>
	<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
	<!-- BODY -->
				
	<?php

	$sql = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
	if(mysqli_num_rows($sql) == 1) {
	$row = mysqli_fetch_array($sql);
	$id_registro = $row["id"];
                
	?>
				
	<div class="tab-base">
					
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#demo-lft-tab-1">Informações Gerais</a>
			</li>
			<li>
				<a data-toggle="tab" href="#demo-lft-tab-2">Endereços</a>
			</li>
			<?php
			$tab = mysqli_query($conn, "SELECT * FROM `ped-cadastro` WHERE `cliente`='".$id_registro."'");
			if(mysqli_num_rows($tab) > 0) {
			?>
			<li>
				<a data-toggle="tab" href="#demo-lft-tab-3">Pedidos</a>
			</li>
			<?php } ?>
		</ul>
		
		<div class="tab-content" style="padding: 25px;">
			<div id="demo-lft-tab-1" class="tab-pane fade active in">
				<p class="text-main text-semibold">Tipo do Cadastro</p>
				<p><?php echo ($row["tipopessoa"] == "PF" ? "Pessoa Física" : "Pessoa Jurídica"); ?></p>
				<p class="text-main text-semibold">Nome ou Razão Social</p>
				<p><?php echo $row["razaosocial"]; ?></p>
				<?php if($row["nomefantasia"] != "") { ?>
				<p class="text-main text-semibold">Nome Fantasia</p>
				<p><?php echo $row["nomefantasia"]; ?></p>
				<?php } ?>
				<p class="text-main text-semibold">CPF ou CNPJ</p>
				<p><?php echo $row["cpfcnpj"]; ?></p>
				<?php if($row["inscricaoestadual"] != "") { ?>
				<p class="text-main text-semibold">Inscrição Municipal ou Inscrição Estadual</p>
				<p><?php echo $row["inscricaoestadual"]; ?></p>
				<?php } ?>
				<p class="text-main text-semibold">E-mail</p>
				<p><?php echo $row["email"]; ?></p>
				<p class="text-main text-semibold">Telefone(s)</p>
				<p><?php echo $row["telefone"]; ?> (P)</p>
				<?php echo ($row["celular"] != "" ? "<p>" . $row["celular"] . " (C)</p>" : ""); ?>
				<?php if($row["pessoacontato"] != "") { ?>
				<p class="text-main text-semibold">Pessoa de Contato</p>
				<p><?php echo $row["pessoacontato"]; ?></p>
				<?php } ?>
				<p class="text-main text-semibold">Status do Registro</p>
				<p><?php echo ($row["status"] == "S" ? "<div class='label label-table label-success'>Ativo</div>" : "<div class='label label-table label-danger'>Inativo</div>"); ?></p>
				<p class="text-main text-semibold">Data de Criação</p>
				<p><?php echo date("d/m/Y H:i:s", strtotime($row["created_at"])); ?></p>
				<?php if($row["updated_at"] != NULL) { ?>
				<p class="text-main text-semibold">Última Alteração</p>
				<p><?php echo $row["updated_at"]; ?></p>
				<?php } ?>
				<?php if($row["deleted_at"] != NULL) { ?>
				<p class="text-main text-semibold">Data da Exclusão</p>
				<p><?php echo $row["deleted_at"]; ?></p>
				<?php } ?>
			</div>
			<div id="demo-lft-tab-2" class="tab-pane fade">
				<?php 
				$endereco = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente`='".$id_registro."' AND `status`='S' AND `deleted_at` IS NULL");
				if(mysqli_num_rows($endereco) > 0) {
				?>
				
				<?php while($end = mysqli_fetch_array($endereco)) { ?>
				<p class="text-main text-semibold"><?php echo ($end["principal"] == "S" ? "Principal" : "Outros Endereços"); ?></p>
				<p><?php echo $end["logradouro"] . ", Nº.: " . $end["numero"]; ?></p>
				<?php if($end["complemento"] != "") { ?>
				<p><?php echo $end["complemento"]; ?></p>
				<?php } ?>
				<p><?php echo $end["bairro"]; ?></p>
				<p><?php echo $end["cidade"] . " - " . $end["estado"]; ?></p>
				<hr>
				<?php } ?>
				
				<?php } else { ?>
				<p class="text-main text-semibold">Atenção!</p>
				<p>Até o momento nenhum endereço foi cadastrado para este cliente.</p>
				<?php } ?>
			</div>
			<?php
			$tab = mysqli_query($conn, "SELECT * FROM `ped-cadastro` WHERE `cliente`='".$id_registro."'");
			if(mysqli_num_rows($tab) > 0) {
			?>
			<div id="demo-lft-tab-3" class="tab-pane fade">
				<p class="text-main text-semibold">Third Tab Content</p>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
			</div>
			<?php } ?>
		</div>
		
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1">
					<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
				</div>
			</div>
		</div>

	</div>

	<?php } else { ?>

	<div class="alert alert-warning" role="alert"> 
		Não existe registro com este identificador.
	</div>

	<?php } ?>
				
	<!-- END BODY -->
				
	<?php } else { ?>
	
		<div class="alert alert-danger" role="alert">
			Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
		</div>

	<?php } ?>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>