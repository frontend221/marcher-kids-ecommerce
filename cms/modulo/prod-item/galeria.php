	<?php

		if (preg_match("/galeria.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
															   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_c); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<?php if(isset($exp[3]) && $exp[3] != "") { ?>			

				<?php $sql = mysqli_query($conn, "SELECT * FROM `prod-item` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL"); ?>
				<?php if(mysqli_num_rows($sql) == 1) { ?>
				<?php $ln = mysqli_fetch_array($sql); ?>
				
				<!-- BODY -->
								
				<form id="formulario" class="form-horizontal dropzone" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" enctype="multipart/form-data">
					
					<input type="hidden" name="rid" value="<?=$exp[3];?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="gallery" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_c;?>" required>
					
					<div class="dz-message" data-dz-message>
						<span><i class="far fa-images"></i> Clique para selecionar ou arraste as imagens do produto para este local</span>
					</div>
															
				</form>
				
				<?php
				$imagem = mysqli_query($conn, "SELECT * FROM `prod-imagem` WHERE `produto`='".$ln["id"]."' AND `status`='S' AND `deleted_at` IS NULL");
				if(mysqli_num_rows($imagem) > 0) {		
				?>
				<div class="row" style="margin-top: 30px;">
					<?php while($img = mysqli_fetch_array($imagem)) { ?>
						<?php $id = $img["id"]; ?>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<img src="<?php echo IMG ?>/produto/<?=$img["imagem"];?>" width="100%" height="auto" alt="<?=$img["imagem"];?>" class="img-responsive">
							<br>
							<a href="<?php echo URL ?>/<?=$exp[0];?>/apagar-imagem/<?=$exp[2];?>/<?=$exp[3];?>/<?=$id;?>" class="btn btn-danger" style="width: 100%; margin: 10px auto !important; text-align: center; text-transform: uppercase;"><i class="far fa-trash-alt fa-fw"></i> Apagar</a>
						</div>
					<?php } ?>								
				</div>
				<?php } ?>

				<?php } else { ?>
							
				    <div class="alert alert-warning" role="alert">
					    Não há produto cadastrado com este identificador. Verifique.
				    </div>						
																																		
			    <?php } ?>
				
				<?php } else { ?>
							
				    <div class="alert alert-warning" role="alert">
					    Nenhum identificador foi informado para esta consulta. Verifique.
				    </div>						
																																		
			    <?php } ?>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-8">
						<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset">	<i class="fas fa-undo-alt"></i> Voltar</a>
					</div>
				</div>
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>