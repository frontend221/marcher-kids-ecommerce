<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
												
				$capa_dir = "../../../upload/catalogo/capa/";
				$arquivo_dir = "../../../upload/catalogo/arquivo/";
				
				$capa_exts = array('jpeg','jpg','png');
				$arquivo_exts = array('pdf');
								
				if ($act == "store") {
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título para identificação do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
					
					if(isset($_FILES["capa"]) && $_FILES["capa"]["size"] > 0) {

						$capa_arq = $_FILES['capa']['name'];
						$capa_tam = $_FILES['capa']['size'];
						$capa_tmp = $_FILES['capa']['tmp_name'];
						$capa_ext = strtolower(end(explode('.',$capa_arq)));

						$capa_up = $capa_dir . basename($capa_arq); 

						if (in_array($capa_ext, $capa_exts)) {
									
							if ($capa_tam < 2200000) {
										
								move_uploaded_file($capa_tmp, $capa_up);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 2MB. O arquivo deve ser menor ou igual a 2MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					} else {

						echo('<script language = "javascript"> alert("Selecione uma imagem para capa do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;

					}
										
					if(isset($_FILES["arquivo"]) && $_FILES["arquivo"]["size"] > 0) {

						$arquivo_arq = $_FILES['arquivo']['name'];
						$arquivo_tam = $_FILES['arquivo']['size'];
						$arquivo_tmp = $_FILES['arquivo']['tmp_name'];
						$arquivo_ext = strtolower(end(explode('.',$arquivo_arq)));

						$arquivo_up = $arquivo_dir . basename($arquivo_arq); 

						if (in_array($arquivo_ext, $arquivo_exts)) {
									
							if ($arquivo_tam < 22000000) {
										
								move_uploaded_file($arquivo_tmp, $arquivo_up);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 20MB. O arquivo deve ser menor ou igual a 20MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo PDF."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					} else {

						echo('<script language = "javascript"> alert("Selecione o arquivo para apresentação do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;

					}
										
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Informe o texto ou a descrição do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}
										
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a posição que o catálogo será mostrado na página."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
										
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
															
					$sql = mysqli_query($conn, "INSERT INTO `arq-catalogo`(`id`, `titulo`, `capa`, `arquivo`, `texto`, `posicao`, `status`, `created_at`) VALUES (0,'".$titulo."','".$capa_arq."','".$arquivo_arq."','".$texto."','".$posicao."','".$status."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-catalogo','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título para identificação do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
					
					if(isset($_FILES["capa"]) && $_FILES["capa"]["size"] > 0) {

						$capa_arq = $_FILES['capa']['name'];
						$capa_tam = $_FILES['capa']['size'];
						$capa_tmp = $_FILES['capa']['tmp_name'];
						$capa_ext = strtolower(end(explode('.',$capa_arq)));

						$capa_up = $capa_dir . basename($capa_arq); 

						if (in_array($capa_ext, $capa_exts)) {
									
							if ($capa_tam < 2200000) {
										
								move_uploaded_file($capa_tmp, $capa_up);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 2MB. O arquivo deve ser menor ou igual a 2MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}
										
					if(isset($_FILES["arquivo"]) && $_FILES["arquivo"]["size"] > 0) {

						$arquivo_arq = $_FILES['arquivo']['name'];
						$arquivo_tam = $_FILES['arquivo']['size'];
						$arquivo_tmp = $_FILES['arquivo']['tmp_name'];
						$arquivo_ext = strtolower(end(explode('.',$arquivo_arq)));

						$arquivo_up = $arquivo_dir . basename($arquivo_arq); 

						if (in_array($arquivo_ext, $arquivo_exts)) {
									
							if ($arquivo_tam < 22000000) {
										
								move_uploaded_file($arquivo_tmp, $arquivo_up);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 20MB. O arquivo deve ser menor ou igual a 20MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo PDF."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}
										
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Informe o texto ou a descrição do catálogo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}
										
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a posição que o catálogo será mostrado na página."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
										
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					if($capa_arq == "" && $arquivo_arq == "") {
												
						$sql = mysqli_query($conn, "UPDATE `arq-catalogo` SET `titulo`='".$titulo."',`texto`='".$texto."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
					
					} else if($capa_arq != "" && $arquivo_arq == "") {
												
						$sql = mysqli_query($conn, "UPDATE `arq-catalogo` SET `titulo`='".$titulo."',`capa`='".$capa_arq."',`texto`='".$texto."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
					
					} else if($capa_arq == "" && $arquivo_arq != "") {
												
						$sql = mysqli_query($conn, "UPDATE `arq-catalogo` SET `titulo`='".$titulo."',`arquivo`='".$arquivo_arq."',`texto`='".$texto."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
					
					} else if($capa_arq != "" && $arquivo_arq != "") {
												
						$sql = mysqli_query($conn, "UPDATE `arq-catalogo` SET `titulo`='".$titulo."',`capa`='".$capa_arq."',`arquivo`='".$arquivo_arq."',`texto`='".$texto."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
					}

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-catalogo','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					}
						
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `arq-catalogo` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','arq-catalogo','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>