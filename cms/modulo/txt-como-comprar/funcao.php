<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
																
				if ($act == "store") {
					
					// nome, imagem e textos dos produtos
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe o título para o texto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
																	
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Digite o texto com o conteúdo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}
																	
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a ordem em que o texto será mostrado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
																	
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$sql = mysqli_query($conn, "INSERT INTO `txt-como-comprar`(`id`, `titulo`, `texto`, `posicao`, `status`, `created_at`) VALUES (0,'".$titulo."','".$texto."','".$posicao."','".$status."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','txt-como-comprar','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
										
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					// nome, imagem e textos dos produtos
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe o título para o texto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
																	
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Digite o texto com o conteúdo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}
																	
					if($_POST["posicao"] == "") {
						echo('<script language = "javascript"> alert("Informe a ordem em que o texto será mostrado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}

					$sql = mysqli_query($conn, "UPDATE `txt-como-comprar` SET `titulo`='".$titulo."',`texto`='".$texto."',`posicao`='".$posicao."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','txt-como-comprar','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/2");

					}	
															
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `txt-como-comprar` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','txt-como-comprar','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>