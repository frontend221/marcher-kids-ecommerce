<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
												
				$diretorio = UPLOAD . "/blog/";
				
				$extensoes = array('jpeg','jpg','png');
								
				if ($act == "store") {
					
					if($_POST["usuario"] == "") {
						echo('<script language = "javascript"> alert("O usuário para a postagem não foi informado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$usuario = mysqli_real_escape_string($conn, stripslashes(trim($_POST["usuario"])));
					}
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título o post do blog."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2200000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 3MB. O arquivo deve ser menor ou igual a 3MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					} else {

						echo('<script language = "javascript"> alert("Selecione uma imagem para o post do blog."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;

					}
															
					$tags = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tags"])));
										
					if($_POST["categoria"] == "") {
						echo('<script language = "javascript"> alert("Selecione ao menos uma categoria para o post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$categoria = implode(",", $_POST["categoria"]);
					}
										
					if($_POST["destaque"] == "") {
						echo('<script language = "javascript"> alert("Digite um texto que aparecerá como destaque do post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$destaque = mysqli_real_escape_string($conn, stripslashes(trim($_POST["destaque"])));
					}
					
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Digite o texto completo do post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$sql = mysqli_query($conn, "INSERT INTO `blog-post`(`id`, `usuario`, `titulo`, `imagem`, `tags`, `categoria`, `destaque`, `texto`, `views`, `status`, `created_at`) VALUES (0,'".$usuario."','".$titulo."','".$arquivo."','".$tags."','".$categoria."','".$destaque."','".$texto."','0','".$status."',NOW())");
					$ultimo_id = mysqli_insert_id($conn);
										
					if ($sql) {
					
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','blog-post','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe um título o post do blog."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
					
					if(isset($_FILES["imagem"]) && $_FILES["imagem"]["size"] > 0) {

						$arquivo = $_FILES['imagem']['name'];
						$tamanho = $_FILES['imagem']['size'];
						$temp  = $_FILES['imagem']['tmp_name'];
						$tipo = $_FILES['imagem']['type'];
						$extensao = strtolower(end(explode('.',$arquivo)));

						$upload = $diretorio . basename($arquivo); 

						if (in_array($extensao, $extensoes)) {
									
							if ($tamanho < 2200000) {
										
								move_uploaded_file($temp, $upload);
										
							} else {
								echo('<script language = "javascript"> alert("Este arquivo tem mais de 3MB. O arquivo deve ser menor ou igual a 3MB."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
									
						} else {
							echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
								
					}
															
					$tags = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tags"])));
										
					if($_POST["categoria"] == "") {
						echo('<script language = "javascript"> alert("Selecione ao menos uma categoria para o post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$categoria = implode(",", $_POST["categoria"]);
					}
										
					if($_POST["destaque"] == "") {
						echo('<script language = "javascript"> alert("Digite um texto que aparecerá como destaque do post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$destaque = mysqli_real_escape_string($conn, stripslashes(trim($_POST["destaque"])));
					}
					
					if($_POST["texto"] == "") {
						echo('<script language = "javascript"> alert("Digite o texto completo do post."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$texto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["texto"])));
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					if($arquivo == "") {
												
						$sql = mysqli_query($conn, "UPDATE `blog-post` SET `titulo`='".$titulo."',`tags`='".$tags."',`categoria`='".$categoria."',`destaque`='".$destaque."',`texto`='".$texto."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
					} else {
												
						$sql = mysqli_query($conn, "UPDATE `blog-post` SET `titulo`='".$titulo."',`imagem`='".$arquivo."',`tags`='".$tags."',`categoria`='".$categoria."',`destaque`='".$destaque."',`texto`='".$texto."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
							
					}

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','blog-post','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					}
						
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `blog-post` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','blog-post','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>