	<?php

		if (preg_match("/apagar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
												   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_d); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
					
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `blog-post` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>

				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
										
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="delete" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_c;?>" required>
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					
					<div class="row">
						<div class="col-lg-12 text-center" style="margin-bottom: 100px; margin-top: 30px;">
							<h2 style="font-size: 20px; text-transform: uppercase; margin-bottom: 30px !important;">Deseja realmente apagar este registro?</h2>
							<div class="btn-group btn-group-lg" role="group">
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Cancelar exclusão" type="reset">
									<i class="far fa-times-circle"></i> Não
								</a>
								<button type="submit" class="btn btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Confirmar a exclusão">
									<i class="far fa-check-circle"></i> Sim
								</button>
							</div>
						</div>
					</div>
                                   
				</form>
                                    
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>

				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>