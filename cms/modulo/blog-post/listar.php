	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
			
            <div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<div class="pad-btm form-inline">
					<div class="row">
					    <div class="col-sm-6 table-toolbar-left">
					        <?php if(in_array($permissao_c, $permissao)){ ?>
							<a href="<?php echo URL ?>/<?=$exp[0];?>/adicionar/<?=$exp[2];?>" id="demo-btn-addrow" class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="right" data-original-title="Adicionar novo registro">
								<i class="fas fa-plus-circle"></i> Adicionar
                            </a>
							<?php } ?>
					    </div>
					    <div class="col-sm-6 table-toolbar-right"></div>
					</div>
				</div>
				
                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="12%" style="text-align: center !important;">Imagem</th>
					            <th width="33%">Título</th>
					            <th width="20%">Categoria(s)</th>
					            <th width="9%" style="text-align: center !important;">Views</th>
					            <th width="8%" style="text-align: center !important;">Status</th>
					            <th width="12%" style="text-align: center !important;">Ações</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "b.*, u.`nome` AS nomeusuario";
							
							$where = "WHERE b.`deleted_at` IS NULL";
							
							$final = "FROM `blog-post` AS b INNER JOIN `usu-sistema` AS u ON b.`usuario`=u.`id` " . $where . " ORDER BY b.`id` DESC";
							
							$sql = mysqli_query($conn, "SELECT $campos $final");
							
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<a href="<?php echo IMG ?>/blog/<?=$ln["imagem"];?>" data-fancybox data-caption="<?=$ln["titulo"];?>">
										<img src="<?php echo IMG ?>/blog/<?=$ln["imagem"];?>" alt="<?=$ln["titulo"];?>" width="100px" height="auto">
									</a>
								</td>
					            <td style="vertical-align: middle !important;"><strong><?=$ln["titulo"];?></strong><br><small>Autor: <?=$ln["nomeusuario"];?></small></td>
								<td style="vertical-align: middle !important;">
								
									<?php
									
									$categoria = explode(",", $ln["categoria"]);
									
									foreach($categoria as $c) {
										$categorias = mysqli_query($conn, "SELECT * FROM `blog-categoria` WHERE `id`='".$c."' AND `status`='S' AND `deleted_at` IS NULL");
										while($cat = mysqli_fetch_array($categorias)) {
											echo $cat["nome"] . "<br>";
										}
									}
																		
									?>
								
								</td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["views"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<?=($ln["status"] == "S" ? "<div class='label label-table label-success'>Ativo</div>" : "<div class='label label-table label-danger'>Inativo</div>");?>
								</td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<div class="btn-group">
										<a href="<?php echo URL ?>/<?=$exp[0];?>/editar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Editar registro">
											<i class="far fa-edit"></i>
										</a>
										<a href="<?php echo URL ?>/<?=$exp[0];?>/apagar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Apagar registro">
											<i class="far fa-trash-alt"></i>
										</a>
									</div>
								</td>
					        </tr>
							
							<?php } ?>

						</tbody>
						
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>