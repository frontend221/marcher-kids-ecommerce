	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
												   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_u); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `prod-item` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>
								
				<form id="formulario" class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
										
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="update" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_u;?>" required>
										
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="categoria">Categoria</label>
							<div class="col-sm-8">
								<select name="categoria" id="categoria" data-placeholder="Selecione uma categoria" tabindex="0" data-width="100%">
									<?php
									$categoria = mysqli_query($conn, "SELECT * FROM `prod-categoria` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($categoria) > 0) {
									?>
									<option value=""></option>
									<?php
									while($cat = mysqli_fetch_array($categoria)){
									?>	
									<option value="<?=$cat["id"];?>" <?=($cat["id"] == $row["categoria"] ? "selected" : "");?>><?=$cat["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos uma categoria para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="grupo">Grupo</label>
							<div class="col-sm-8">
								<select name="grupo" id="grupo" data-placeholder="Selecione um grupo" tabindex="0" data-width="100%">
									<?php
									$grupo = mysqli_query($conn, "SELECT * FROM `prod-grupo` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($grupo) > 0) {
									?>
									<option value=""></option>
									<?php
									while($grp = mysqli_fetch_array($grupo)){
									?>	
									<option value="<?=$grp["id"];?>" <?=($grp["id"] == $row["grupo"] ? "selected" : "");?>><?=$grp["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um grupo para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="subgrupo">Subgrupo</label>
							<div class="col-sm-8">
								<select name="subgrupo" id="subgrupo" data-placeholder="Selecione um subgrupo" tabindex="0" data-width="100%">
									<?php
									$subgrupo = mysqli_query($conn, "SELECT * FROM `prod-subgrupo` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($subgrupo) > 0) {
									?>
									<option value="0"></option>
									<?php
									while($sub = mysqli_fetch_array($subgrupo)){
									?>	
									<option value="<?=$sub["id"];?>" <?=($sub["id"] == $row["subgrupo"] ? "selected" : "");?>><?=$sub["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um subgrupo para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="marca">Marca</label>
							<div class="col-sm-8">
								<select name="marca" id="marca" data-placeholder="Selecione uma marca" tabindex="0" data-width="100%">
									<?php
									$marca = mysqli_query($conn, "SELECT * FROM `prod-marca` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($marca) > 0) {
									?>
									<option value="0"></option>
									<?php
									while($mar = mysqli_fetch_array($marca)){
									?>	
									<option value="<?=$mar["id"];?>" <?=($mar["id"] == $row["marca"] ? "selected" : "");?>><?=$mar["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos uma marca para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="modelo">Modelo</label>
							<div class="col-sm-8">
								<select name="modelo" id="modelo" data-placeholder="Selecione um modelo" tabindex="0" data-width="100%">
									<?php
									$modelo = mysqli_query($conn, "SELECT * FROM `prod-modelo` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($modelo) > 0) {
									?>
									<option value="0"></option>
									<?php
									while($mod = mysqli_fetch_array($modelo)){
									?>	
									<option value="<?=$mod["id"];?>" <?=($mod["id"] == $row["modelo"] ? "selected" : "");?>><?=$mod["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um modelo para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prodcor">Cor</label>
							<div class="col-sm-8">
								<select name="prodcor[]" id="prodcor" multiple data-placeholder="Selecione uma ou mais cores" tabindex="0" data-width="100%">
									<?php
									$cores = mysqli_query($conn, "SELECT * FROM `prod-cor` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($cores) > 0) {
										
									$arr_cor = array();
									$prod_cor = explode(",",$row["cor"]);
									foreach($prod_cor as $c) {
										array_push($arr_cor, $c);
									}	
										
									?>
									<option value=""></option>
									<?php										
									while($cor = mysqli_fetch_array($cores)){
									?>	
									<option value="<?=$cor["id"];?>" <?php echo (in_array($cor["id"], $arr_cor) ? "selected" : "");?>><?=$cor["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos uma cor para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="prodtam">Tamanho</label>
							<div class="col-sm-8">
								<select name="prodtam[]" id="prodtam" multiple data-placeholder="Selecione um ou mais tamanhos" tabindex="0" data-width="100%">
									<?php
									$tamanho = mysqli_query($conn, "SELECT * FROM `prod-tamanho` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($tamanho) > 0) {
										
									$arr_tam = array();
									$prod_tam = explode(",",$row["tamanho"]);
									foreach($prod_tam as $c) {
										array_push($arr_tam, $c);
									}	
										
									?>
									<option value=""></option>
									<?php
									while($tam = mysqli_fetch_array($tamanho)){
									?>	
									<option value="<?=$tam["id"];?>" <?php echo (in_array($tam["id"], $arr_tam) ? "selected" : "");?>><?=$tam["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um tamanho para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="gramatura">Gramatura</label>
							<div class="col-sm-8">
								<select name="gramatura[]" id="gramatura" multiple data-placeholder="Selecione uma ou mais gramaturas" tabindex="0" data-width="100%">
									<?php
									$gramatura = mysqli_query($conn, "SELECT * FROM `prod-gramatura` WHERE `status`='S' AND `deleted_at` IS NULL");
									if(mysqli_num_rows($gramatura) > 0) {
										
									$arr_gra = array();
									$prod_gra = explode(",",$row["gramatura"]);
									foreach($prod_gra as $c) {
										array_push($arr_gra, $c);
									}	
										
									?>
									<option value=""></option>
									<?php
									while($gra = mysqli_fetch_array($gramatura)){
									?>	
									<option value="<?=$gra["id"];?>" <?php echo (in_array($gra["id"], $arr_gra) ? "selected" : "");?>><?=$gra["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos uma gramatura para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Unidade de Medida</label>
							<div class="col-md-8">
								
								<?php
								$unidade = mysqli_query($conn, "SELECT * FROM `prod-unidade` WHERE `status`='S' AND `deleted_at` IS NULL");
								if(mysqli_num_rows($unidade) > 0) {
								?>
								<div class="radio">
									<?php
									while($uni = mysqli_fetch_array($unidade)){
									?>
									<input id="unidade<?php echo $uni["id"];?>" class="magic-radio" type="radio" name="unidade" value="<?php echo $uni["id"];?>" <?php echo ($row["unidade"] == $uni["id"] ? "checked" : ""); ?>>
									<label for="unidade<?php echo $uni["id"];?>"><?php echo $uni["nome"];?> <small>(<?php echo $uni["abbr"];?>)</small></label>
									<?php	
									}
									?>
								</div>
								<?php
								} else {
								?>
								<input type="text" class="form-control" placeholder="Você deve cadastrar ao menos uma unidade de medida para prosseguir com o formulário" readonly>
								<?php
								}				
								?>
								
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="referencia">Referência</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a referência do produto" id="referencia" name="referencia" class="form-control" value="<?php echo $row["referencia"]; ?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="nome">Nome</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe o nome do produto" id="nome" name="nome" class="form-control" value="<?php echo $row["nome"]; ?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="tags">Tags</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe uma ou mais tags para identificação do produto" id="tags" name="tags" class="form-control" value="<?php echo $row["tags"]; ?>" required>
								<small>Digite as tags que melhor representam o produto, separadas por vírgula <code>Exemplo: camiseta, marculina, regata</code></small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="destaque">Destaque</label>
							<div class="col-md-8">
								<div class="radio">
									<input id="destaque1" class="magic-radio" type="radio" name="destaque" value="S" <?=($row["destaque"] == "S" ? "checked" : "");?>>
									<label for="destaque1">Sim</label>
									<input id="destaque2" class="magic-radio" type="radio" name="destaque" value="N" <?=($row["destaque"] == "N" ? "checked" : "");?>>
									<label for="destaque2">Não</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="lancamento">Lançamento</label>
							<div class="col-md-8">
								<div class="radio">
									<input id="lancamento1" class="magic-radio" type="radio" name="lancamento" value="S" <?=($row["lancamento"] == "S" ? "checked" : "");?>>
									<label for="lancamento1">Sim</label>
									<input id="lancamento2" class="magic-radio" type="radio" name="lancamento" value="N" <?=($row["lancamento"] == "N" ? "checked" : "");?>>
									<label for="lancamento2">Não</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="valor">Valor (R$)</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Informe o valor do produto" id="valor" name="valor" value="<?php echo $row["valor"]; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="oferta">Oferta</label>
							<div class="col-md-8">
								<div class="radio">
									<input id="oferta1" class="magic-radio" type="radio" name="oferta" value="S" <?=($row["oferta"] == "S" ? "checked" : "");?>>
									<label for="oferta1">Sim</label>
									<input id="oferta2" class="magic-radio" type="radio" name="oferta" value="N" <?=($row["oferta"] == "N" ? "checked" : "");?>>
									<label for="oferta2">Não</label>
								</div>
							</div>
						</div>
						<div id="inputdesconto" style="display: none;">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="desconto">Desconto (%)</label>
								<div class="col-sm-3">
									<input type="text" placeholder="Informe o valor do desconto em porcentagem (%)" id="desconto" name="desconto" value="<?php echo $row["desconto"]; ?>" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="altura">Altura (cm)</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Altura" id="altura" name="altura" value="<?php echo $row["altura"]; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="largura">Largura (cm)</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Largura" id="largura" name="largura" value="<?php echo $row["largura"]; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="profundidade">Profundidade (cm)</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Profundidade" id="profundidade" name="profundidade" value="<?php echo $row["profundidade"]; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="peso">Peso (g)</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Peso" id="peso" name="peso" value="<?php echo $row["peso"]; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="descricao">Descrição</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="descricao" id="descricao"><?php echo $row["descricao"]; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Status do Registro</label>
							<div class="col-md-8">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" <?=($row["status"] == "S" ? "checked" : "");?>>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N" <?=($row["status"] == "N" ? "checked" : "");?>>
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
							</div>
						</div>
					</div>
					
				</form>
				
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>