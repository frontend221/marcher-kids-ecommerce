<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");
				
				// configuração dos diretórios para upload
				
				if ($act == "store") {
					
					if($_POST["categoria"] == "") {
						echo('<script language = "javascript"> alert("Selecione a categoria."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$categoria = mysqli_real_escape_string($conn, stripslashes(trim($_POST["categoria"])));
					}
																	
					if($_POST["grupo"] == "") {
						echo('<script language = "javascript"> alert("Selecione um grupo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$grupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["grupo"])));
					}
						
					if($_POST["subgrupo"] != "") {
						$subgrupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["subgrupo"])));
					} else {
						$subgrupo = 0;
					}
					
					if($_POST["marca"] != "") {
						$marca = mysqli_real_escape_string($conn, stripslashes(trim($_POST["marca"])));
					} else {
						$marca = 0;
					}
					
					if($_POST["modelo"] != "") {
						$modelo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["modelo"])));
					} else {
						$modelo = 0;
					}
					
					if($_POST["prodcor"] != "") {
						$cor = implode(",", $_POST["prodcor"]);
					}
																	
					if($_POST["prodtam"] != "") {
						$tamanho = implode(",", $_POST["prodtam"]);
					}
																	
					if($_POST["gramatura"] != "") {
						$gramatura = implode(",", $_POST["gramatura"]);
					}
																	
					$unidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["unidade"])));
					
					if($_POST["referencia"] == "") {
						echo('<script language = "javascript"> alert("Informe a referência interna do produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$referencia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["referencia"])));
					}
																	
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe um nome para o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
																	
					if($_POST["tags"] == "") {
						echo('<script language = "javascript"> alert("Informe ao menos uma tag para o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tags = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tags"])));
					}
					
					$destaque = mysqli_real_escape_string($conn, stripslashes(trim($_POST["destaque"])));
					
					$lancamento = mysqli_real_escape_string($conn, stripslashes(trim($_POST["lancamento"])));
																																		
					if($_POST["valor"] == "") {
						echo('<script language = "javascript"> alert("Informe o valor do produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$valor = mysqli_real_escape_string($conn, stripslashes(trim($_POST["valor"])));
					}
					
					if($_POST["oferta"] == "S") {
						$oferta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oferta"])));
						if($_POST["desconto"] == "") {
							$desconto = 0.00;
						} else {
							$desconto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["desconto"])));
						}
					} else {
						$oferta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oferta"])));
						$desconto = 0.00;
					}

					if($_POST["descricao"] == "") {
						echo('<script language = "javascript"> alert("Digite as informações sobre o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$descricao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["descricao"])));
					}

					if($_POST["altura"] != "") {
						$altura = mysqli_real_escape_string($conn, stripslashes(trim($_POST["altura"])));
					} else {
						$altura = 0;
					}
					
					if($_POST["largura"] != "") {
						$largura = mysqli_real_escape_string($conn, stripslashes(trim($_POST["largura"])));
					} else {
						$largura = 0;
					}
					
					if($_POST["profundidade"] != "") {
						$profundidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["profundidade"])));
					} else {
						$profundidade = 0;
					}

					if($_POST["peso"] != "") {
						$peso = mysqli_real_escape_string($conn, stripslashes(trim($_POST["peso"])));
					} else {
						$peso = 0;
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
										
					$sql = mysqli_query($conn, "INSERT INTO `prod-item`(`id`, `categoria`, `grupo`, `subgrupo`, `marca`, `modelo`, `cor`, `tamanho`, `gramatura`, `unidade`, `referencia`, `nome`, `tags`, `destaque`, `lancamento`, `valor`, `oferta`, `desconto`, `descricao`, `altura`, `largura`, `profundidade`, `peso`, `status`, `created_at`) VALUES (0,'".$categoria."','".$grupo."','".$subgrupo."','".$marca."','".$modelo."','".$cor."','".$tamanho."','".$gramatura."','".$unidade."','".$referencia."','".$nome."','".$tags."','".$destaque."','".$lancamento."','".$valor."','".$oferta."','".$desconto."','".$descricao."','".$altura."','".$largura."','".$profundidade."','".$peso."','".$status."',NOW())");
										
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-item','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
																									
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if($_POST["categoria"] == "") {
						echo('<script language = "javascript"> alert("Selecione a categoria."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$categoria = mysqli_real_escape_string($conn, stripslashes(trim($_POST["categoria"])));
					}
																	
					if($_POST["grupo"] == "") {
						echo('<script language = "javascript"> alert("Selecione um grupo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$grupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["grupo"])));
					}
						
					if($_POST["subgrupo"] != "") {
						$subgrupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["subgrupo"])));
					} else {
						$subgrupo = 0;
					}
					
					if($_POST["marca"] != "") {
						$marca = mysqli_real_escape_string($conn, stripslashes(trim($_POST["marca"])));
					} else {
						$marca = 0;
					}
					
					if($_POST["modelo"] != "") {
						$modelo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["modelo"])));
					} else {
						$modelo = 0;
					}
					
					if($_POST["prodcor"] != "") {
						$cor = implode(",", $_POST["prodcor"]);
					}
																	
					if($_POST["prodtam"] != "") {
						$tamanho = implode(",", $_POST["prodtam"]);
					}
																	
					if($_POST["gramatura"] != "") {
						$gramatura = implode(",", $_POST["gramatura"]);
					}
																	
					$unidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["unidade"])));
																						
					if($_POST["referencia"] == "") {
						echo('<script language = "javascript"> alert("Informe a referência interna do produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$referencia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["referencia"])));
					}
																	
					if($_POST["nome"] == "") {
						echo('<script language = "javascript"> alert("Informe um nome para o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$nome = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome"])));
					}
																	
					if($_POST["tags"] == "") {
						echo('<script language = "javascript"> alert("Informe ao menos uma tag para o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tags = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tags"])));
					}
					
					$destaque = mysqli_real_escape_string($conn, stripslashes(trim($_POST["destaque"])));
					
					$lancamento = mysqli_real_escape_string($conn, stripslashes(trim($_POST["lancamento"])));
																																		
					if($_POST["valor"] == "") {
						echo('<script language = "javascript"> alert("Informe o valor do produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$valor = mysqli_real_escape_string($conn, stripslashes(trim($_POST["valor"])));
					}
					
					if($_POST["oferta"] == "S") {
						$oferta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oferta"])));
						if($_POST["desconto"] == "") {
							$desconto = 0.00;
						} else {
							$desconto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["desconto"])));
						}
					} else {
						$oferta = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oferta"])));
						$desconto = 0.00;
					}

					if($_POST["descricao"] == "") {
						echo('<script language = "javascript"> alert("Digite as informações sobre o produto."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$descricao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["descricao"])));
					}

					if($_POST["altura"] != "") {
						$altura = mysqli_real_escape_string($conn, stripslashes(trim($_POST["altura"])));
					} else {
						$altura = 0;
					}
					
					if($_POST["largura"] != "") {
						$largura = mysqli_real_escape_string($conn, stripslashes(trim($_POST["largura"])));
					} else {
						$largura = 0;
					}
					
					if($_POST["profundidade"] != "") {
						$profundidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["profundidade"])));
					} else {
						$profundidade = 0;
					}

					if($_POST["peso"] != "") {
						$peso = mysqli_real_escape_string($conn, stripslashes(trim($_POST["peso"])));
					} else {
						$peso = 0;
					}

					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}

					$sql = mysqli_query($conn, "UPDATE `prod-item` SET `categoria`='".$categoria."',`grupo`='".$grupo."',`subgrupo`='".$subgrupo."',`marca`='".$marca."',`modelo`='".$modelo."',`cor`='".$cor."',`tamanho`='".$tamanho."',`gramatura`='".$gramatura."',`unidade`='".$unidade."',`referencia`='".$referencia."',`nome`='".$nome."',`tags`='".$tags."',`destaque`='".$destaque."',`lancamento`='".$lancamento."',`valor`='".$valor."',`oferta`='".$oferta."',`desconto`='".$desconto."',`descricao`='".$descricao."',`altura`='".$altura."',`largura`='".$largura."',`profundidade`='".$profundidade."',`peso`='".$peso."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-item','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/2");

					}
										
				} else if ($act == "gallery") {
															
					$id_registro = $_POST["rid"];
					
					$targetDir = '../../../upload/produto/';

					if (!empty($_FILES)) {

						$targetName = $_FILES['file']['name'];
						$targetFile = $targetDir . $targetName;

						if(move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)){

							$sql = mysqli_query($conn, "INSERT INTO `prod-imagem`(`id`, `produto`, `imagem`, `status`, `created_at`) VALUES (0,'".$id_registro."','".$targetName."','S',NOW())");

						}
						
					}
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-imagem','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/galeria/".$pag."/".$id_registro."/1");

					} else {

						header("Location: " . URL . "/".$mod."/galeria/".$pag."/".$id_registro."/2");

					}
					
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `prod-item` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-item','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				} else if ($act == "delete_image") {
					
					$id_registro = $_POST["pid"];
					$id_img_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `prod-imagem` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_img_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','prod-imagem','".utf8_encode("Exclusão da imagem com registro ID: ".$id_img_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/galeria/".$pag."/".$id_registro."/1");

					} else {

						header("Location: " . URL . "/".$mod."/galeria/".$pag."/".$id_registro."/2");

					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>