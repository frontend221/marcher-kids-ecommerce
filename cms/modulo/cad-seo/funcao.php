<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];
				
				require_once ("../../config/cfg-database.php");
				
				if($_POST["title"] == "") {
					echo('<script language = "javascript"> alert("Informe o título do website."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$title = mysqli_real_escape_string($conn, stripslashes(trim($_POST["title"])));
				}
						
				if($_POST["description"] == "") {
					echo('<script language = "javascript"> alert("Informe uma descrição."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$description = mysqli_real_escape_string($conn, stripslashes(trim($_POST["description"])));
				}
						
				if($_POST["keywords"] == "") {
					echo('<script language = "javascript"> alert("Informe algumas palavras-chave."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$keywords = mysqli_real_escape_string($conn, stripslashes(trim($_POST["keywords"])));
				}
					
				if($_POST["robot"] == "") {
					echo('<script language = "javascript"> alert("Selecione o tipo de robô."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$robot = mysqli_real_escape_string($conn, stripslashes(trim($_POST["robot"])));
				}
					
				if($_POST["rating"] == "") {
					echo('<script language = "javascript"> alert("Selecione o classificação."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$rating = mysqli_real_escape_string($conn, stripslashes(trim($_POST["rating"])));
				}
										
				if($_POST["distribution"] == "") {
					echo('<script language = "javascript"> alert("Selecione a distribuição."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$distribution = mysqli_real_escape_string($conn, stripslashes(trim($_POST["distribution"])));
				}

				$language = mysqli_real_escape_string($conn, stripslashes(trim($_POST["language"])));
				
				$region = mysqli_real_escape_string($conn, stripslashes(trim($_POST["region"])));
				
				$placename = mysqli_real_escape_string($conn, stripslashes(trim($_POST["placename"])));
				
				$position = mysqli_real_escape_string($conn, stripslashes(trim($_POST["position"])));
				
				if($_POST["revisitafter"] == "") {
					echo('<script language = "javascript"> alert("Selecione o tempo de revisita dos robôs."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$revisitafter = mysqli_real_escape_string($conn, stripslashes(trim($_POST["revisitafter"])));
				}
				
				$ogtitle = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogtitle"])));
				
				$ogsitename = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogsitename"])));
				
				$ogurl = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogurl"])));
				
				$ogdescription = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogdescription"])));
				
				$oglocale = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oglocale"])));
				
				$oglocalealternate = mysqli_real_escape_string($conn, stripslashes(trim($_POST["oglocalealternate"])));
				
				if(isset($_FILES["ogimage"]) && $_FILES["ogimage"]["size"] > 0) {
					
					$diretorio = "../../../upload/seo/";

					$extensoes = array('jpeg','jpg','png','gif');

					$arquivo = $_FILES['ogimage']['name'];
					$tamanho = $_FILES['ogimage']['size'];
					$temp  = $_FILES['ogimage']['tmp_name'];
					$tipo = $_FILES['ogimage']['type'];
					$extensao = strtolower(end(explode('.',$arquivo)));

					$upload = $diretorio . basename($arquivo); 

					if (in_array($extensao, $extensoes)) {
									
						if ($tamanho < 2000000) {
										
							move_uploaded_file($temp, $upload);
										
						} else {
							echo('<script language = "javascript"> alert("Este arquivo tem mais de 2MB. O arquivo deve ser menor ou igual a 2MB."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
									
					} else {
						echo('<script language = "javascript"> alert("Esta extensão de arquivo não é permitida. Por favor, faça o upload de um arquivo JPEG ou PNG."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					}
												
				}
				
				$ogimagetype = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogimagetype"])));
				
				$ogimagewidth = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogimagewidth"])));
				
				$ogimageheight = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogimageheight"])));
				
				$ogtype = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ogtype"])));
				
				if ($act == "store") {
					
					$sql = mysqli_query($conn, "INSERT INTO `cad-seo`(`id`, `title`, `description`, `keywords`, `robot`, `rating`, `distribution`, `language`, `region`, `placename`, `position`, `revisitafter`, `ogtitle`, `ogsitename`, `ogurl`, `ogdescription`, `oglocale`, `oglocalealternate`, `ogimage`, `ogimagetype`, `ogimagewidth`, `ogimageheight`, `ogtype`, `created_at`) VALUES (0,'".$title."','".$description."','".$keywords."','".$robot."','".$rating."','".$distribution."','".$language."','".$region."','".$placename."','".$position."','".$revisitafter."','".$ogtitle."','".$ogsitename."','".$ogurl."','".$ogdescription."','".$oglocale."','".$oglocalealternate."','".$arquivo."','".$ogimagetype."','".$ogimagewidth."','".$ogimageheight."','".$ogtype."',NOW())");

					if ($sql) {

						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','cad-seo','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

					}
					
										
				} else if ($act == "update") {
					
					$id_registro = $_POST["rid"];
															
					if(isset($arquivo) && $arquivo != "") {

						$sql = mysqli_query($conn, "UPDATE `cad-seo` SET `title`='".$title."',`description`='".$description."',`keywords`='".$keywords."',`robot`='".$robot."',`rating`='".$rating."',`distribution`='".$distribution."',`language`='".$language."',`region`='".$region."',`placename`='".$placename."',`position`='".$position."',`revisitafter`='".$revisitafter."',`ogtitle`='".$ogtitle."',`ogsitename`='".$ogsitename."',`ogurl`='".$ogurl."',`ogdescription`='".$ogdescription."',`oglocale`='".$oglocale."',`oglocalealternate`='".$oglocalealternate."',`ogimage`='".$arquivo."',`ogimagetype`='".$ogimagetype."',`ogimagewidth`='".$ogimagewidth."',`ogimageheight`='".$ogimageheight."',`ogtype`='".$ogtype."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
											
					} else {
						
						$sql = mysqli_query($conn, "UPDATE `cad-seo` SET `title`='".$title."',`description`='".$description."',`keywords`='".$keywords."',`robot`='".$robot."',`rating`='".$rating."',`distribution`='".$distribution."',`language`='".$language."',`region`='".$region."',`placename`='".$placename."',`position`='".$position."',`revisitafter`='".$revisitafter."',`ogtitle`='".$ogtitle."',`ogsitename`='".$ogsitename."',`ogurl`='".$ogurl."',`ogdescription`='".$ogdescription."',`oglocale`='".$oglocale."',`oglocalealternate`='".$oglocalealternate."',`ogimagetype`='".$ogimagetype."',`ogimagewidth`='".$ogimagewidth."',`ogimageheight`='".$ogimageheight."',`ogtype`='".$ogtype."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");
											
					}

					if ($sql) {
							
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','cad-seo','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");

					} else {

						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					}
					
				}
					
			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>