	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[3]) && is_numeric($exp[3]) && $exp[3] != "") { include("include/inc-infos.php"); } ?>
                
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `cad-seo`");

					$row = mysqli_fetch_array($sql);

					$id_registro = $row["id"];

				?>
				
				<form id="formulario" class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post" enctype="multipart/form-data">
					
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="act" value="update" required>' : '<input type="hidden" name="act" value="store" required>');?>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<?=(isset($id_registro) && $id_registro != "" ? '<input type="hidden" name="reg" value="'.$permissao_c.'" required>' : '<input type="hidden" name="reg" value="'.$permissao_u.'" required>');?>
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="title">Título do Website</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Título do Website" id="title" name="title" class="form-control" value="<?=$row["title"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="description">Descrição</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Descrição" id="description" name="description" class="form-control" value="<?=$row["description"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="keywords">Palavras-chave</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Palavras-chave" id="keywords" name="keywords" class="form-control" value="<?=$row["keywords"];?>" required>
								<small>Separe as palavras-chave por <strong>vígula</strong> (,)</small>							
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="robot">Robôs</label>
							<div class="col-sm-8">
								<select name="robot" id="robot" data-placeholder="Selecione a varredura dos robôs" tabindex="0" data-width="100%">
                					<option value=""></option>
									<option value="index,follow" <?=($row["robot"] == 'index,follow' ? 'selected' : ''); ?>>index,follow - sintaxe mais comum de meta robots - orienta os buscadores a indexar o conteúdo da página.</option>
									<option value="noindex,nofollow" <?=($row["robot"] == 'noindex,nofollow' ? 'selected' : ''); ?>>noindex,nofollow - orienta os buscadores a não indexar o conteúdo da página.</option>
									<option value="index" <?=($row["robot"] == 'index' ? 'selected' : ''); ?>>index - indexe esta página - exiba-a em seus resultados de busca.</option>
									<option value="noindex" <?=($row["robot"] == 'noindex' ? 'selected' : ''); ?>>noindex - não indexe esta página - não a exiba nos resultados de busca.</option>
									<option value="follow" <?=($row["robot"] == 'follow' ? 'selected' : ''); ?>>follow: siga os links desta pagina para descobrir novas páginas.</option>
									<option value="nofollow" <?=($row["robot"] == 'nofollow' ? 'selected' : ''); ?>>nofollow: nenhum dos links desta página deve ser seguido.</option>
									<option value="nosnippet" <?=($row["robot"] == 'nosnippet' ? 'selected' : ''); ?>>nosnippet: orienta o site de busca a não exibir a descrição da página nos resultados de busca.</option>
									<option value="noodp" <?=($row["robot"] == 'noodp' ? 'selected' : ''); ?>>noodp: orienta o Google não utilizar a descrição do diretório DMOZ em seus resultados (snippet).</option>
									<option value="noarchive" <?=($row["robot"] == 'noarchive' ? 'selected' : ''); ?>>noarchive: instrui o Google a não exibir a versão em cache da página.</option>
									<option value="noimageindex" <?=($row["robot"] == 'noimageindex' ? 'selected' : ''); ?>>noimageindex: não indexe nehuma imagem da página.</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rating">Classificação</label>
							<div class="col-sm-8">
								<select name="rating" id="rating" data-placeholder="Selecione a classificação do website" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="general" <?=($row["rating"] == 'general' ? 'selected' : ''); ?>>general</option>
									<option value="mature" <?=($row["rating"] == 'mature' ? 'selected' : ''); ?>>mature</option>
									<option value="restricted" <?=($row["rating"] == 'restricted' ? 'selected' : ''); ?>>restricted</option>
									<option value="14 years" <?=($row["rating"] == '14 years' ? 'selected' : ''); ?>>14 years</option>
									<option value="safe for kids" <?=($row["rating"] == 'safe for kids' ? 'selected' : ''); ?>>safe for kids</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="distribution">Distribuição</label>
							<div class="col-sm-8">
								<select name="distribution" id="distribution" data-placeholder="Selecione o tipo de distribuição do website" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="global" <?=($row["distribution"] == 'global' ? 'selected' : ''); ?>>global</option>
									<option value="local" <?=($row["distribution"] == 'local' ? 'selected' : ''); ?>>local</option>
									<option value="iu" <?=($row["distribution"] == 'iu' ? 'selected' : ''); ?>>internal use</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="language">Idioma</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Idioma" id="language" name="language" class="form-control" value="<?=$row["language"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="region">Geo - Região</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Geo - Região" id="region" name="region" class="form-control" value="<?=$row["region"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="placename">Geo - Cidade</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Geo - Cidade" id="placename" name="placename" class="form-control" value="<?=$row["placename"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="position">Geo - Lat./Long.</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Geo - Latitude e Longitude" id="position" name="position" class="form-control" value="<?=$row["position"];?>">
								<small>Informe primeiro a <strong>latitude</strong>, em seguida a <strong>longitude</strong>, separados por <strong>ponto e vírgula</strong> (;)</small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="revisitafter">Revisitar após</label>
							<div class="col-sm-8">
								<select name="revisitafter" id="revisitafter" data-placeholder="Selecione o período de revisita dos robôs" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="1 day" <?=($row["revisitafter"] == '1 day' ? 'selected' : ''); ?>>1 dia</option>
									<option value="3 days" <?=($row["revisitafter"] == '3 days' ? 'selected' : ''); ?>>3 dias</option>
									<option value="7 days" <?=($row["revisitafter"] == '7 days' ? 'selected' : ''); ?>>7 dias</option>
									<option value="15 days" <?=($row["revisitafter"] == '15 days' ? 'selected' : ''); ?>>15 dias</option>
									<option value="1 month" <?=($row["revisitafter"] == '1 month' ? 'selected' : ''); ?>>1 mês</option>
									<option value="3 months" <?=($row["revisitafter"] == '3 months' ? 'selected' : ''); ?>>3 meses</option>
									<option value="6 months" <?=($row["revisitafter"] == '6 months' ? 'selected' : ''); ?>>6 meses</option>
									<option value="1 year" <?=($row["revisitafter"] == '1 year' ? 'selected' : ''); ?>>1 ano</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogtitle">Título (Facebook)</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Título (Facebook)" id="ogtitle" name="ogtitle" class="form-control" value="<?=$row["ogtitle"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogsitename">Nome (Facebook)</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome (Facebook)" id="ogsitename" name="ogsitename" class="form-control" value="<?=$row["ogsitename"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogurl">URL Canônica</label>
							<div class="col-sm-8">
								<input type="text" placeholder="URL Canônica" id="ogurl" name="ogurl" class="form-control" value="<?=$row["ogurl"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogdescription">Descrição (Facebook)</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Descrição (Facebook)" id="ogdescription" name="ogdescription" class="form-control" value="<?=$row["ogdescription"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="oglocale">Localização</label>
							<div class="col-sm-8">
								<select name="oglocale" id="oglocale" data-placeholder="Selecione a localização principal" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="en_US" <?=($row["oglocale"] == 'en_US' ? 'selected' : ''); ?>>en_US</option>
									<option value="pt_BR" <?=($row["oglocale"] == 'pt_BR' ? 'selected' : ''); ?>>pt_BR</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="oglocalealternate">Localização<br><small>(Alternativa)</small></label>
							<div class="col-sm-8">
								<select name="oglocalealternate" id="oglocalealternate" data-placeholder="Selecione a localização alternativa" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="en_US" <?=($row["oglocalealternate"] == 'en_US' ? 'selected' : ''); ?>>en_US</option>
									<option value="pt_BR" <?=($row["oglocalealternate"] == 'pt_BR' ? 'selected' : ''); ?>>pt_BR</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogimage">Imagem<br><small><?=(isset($row["ogimage"]) && $row["ogimage"] != "" ? '<a style="color: #C90D10;" href="'. IMG .'/seo/'.$row["ogimage"].'" data-fancybox>Ver imagem</a>' : '');?></small></label>
							<div class="col-sm-8">
								<input type="file" class="filestyle" data-text="Selecionar" data-btnClass="btn-primary" placeholder="Selecione uma imagem" id="ogimage" name="ogimage">
								<small>Permitido apenas imagens com as extensões <strong>jpg</strong>, <strong>gif</strong> e <strong>png</strong>, no formato <strong>máximo</strong> de <strong>1200 pixels de largura</strong> por <strong>628 pixel de altura</strong> e com <strong>1Mb</strong> de tamanho lógico.<br><code>Não utilizar acentos, espaços e caracteres especiais no nome das imagens.</code></small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogimagetype">Tipo da Imagem</label>
							<div class="col-sm-8">
								<select name="ogimagetype" id="ogimagetype" data-placeholder="Selecione o tipo da imagem" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="image/jpeg" <?=($row["ogimagetype"] == 'image/jpeg' ? 'selected' : ''); ?>>image/jpeg</option>
									<option value="image/gif" <?=($row["ogimagetype"] == 'image/gif' ? 'selected' : ''); ?>>image/gif</option>
									<option value="image/png" <?=($row["ogimagetype"] == 'image/png' ? 'selected' : ''); ?>>image/png</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogimagewidth">Largura da Imagem</label>
							<div class="col-sm-8">
								<input type="number" min="0" placeholder="Largura da Imagem - valor máximo 1200 pixels" id="ogimagewidth" name="ogimagewidth" maxlength="4" min="0" max="1200" class="form-control" value="<?=$row["ogimagewidth"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogimageheight">Altura da Imagem</label>
							<div class="col-sm-8">
								<input type="number" min="0" placeholder="Altura da Imagem - valor máximo 628 pixels" id="ogimageheight" name="ogimageheight" maxlength="3" min="0" max="628" class="form-control" value="<?=$row["ogimageheight"];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="ogtype">Tipo do Conteúdo</label>
							<div class="col-sm-8">
								<select name="ogtype" id="ogtype" data-placeholder="Selecione o tipo do conteúdo" tabindex="0" data-width="100%">
									<option value=""></option>
									<option value="article" <?=($row["ogtype"] == 'article' ? 'selected' : ''); ?>>article</option>
									<option value="book" <?=($row["ogtype"] == 'book' ? 'selected' : ''); ?>>book</option>
									<option value="books.author" <?=($row["ogtype"] == 'books.author' ? 'selected' : ''); ?>>books.author</option>
									<option value="books.book" <?=($row["ogtype"] == 'books.book' ? 'selected' : ''); ?>>books.book</option>
									<option value="books.genre" <?=($row["ogtype"] == 'books.genre' ? 'selected' : ''); ?>>books.genre</option>
									<option value="business.business" <?=($row["ogtype"] == 'business.business' ? 'selected' : ''); ?>>business.business</option>
									<option value="fitness.course" <?=($row["ogtype"] == 'fitness.course' ? 'selected' : ''); ?>>fitness.course</option>
									<option value="game.achievement" <?=($row["ogtype"] == 'game.achievement' ? 'selected' : ''); ?>>game.achievement</option>
									<option value="music.album" <?=($row["ogtype"] == 'music.album' ? 'selected' : ''); ?>>music.album</option>
									<option value="music.playlist" <?=($row["ogtype"] == 'music.playlist' ? 'selected' : ''); ?>>music.playlist</option>
									<option value="music.radio_station" <?=($row["ogtype"] == 'music.radio_station' ? 'selected' : ''); ?>>music.radio_station</option>
									<option value="music.song" <?=($row["ogtype"] == 'music.song' ? 'selected' : ''); ?>>music.song</option>
									<option value="place" <?=($row["ogtype"] == 'place' ? 'selected' : ''); ?>>place</option>
									<option value="product" <?=($row["ogtype"] == 'product' ? 'selected' : ''); ?>>product</option>
									<option value="product.group" <?=($row["ogtype"] == 'product.group' ? 'selected' : ''); ?>>product.group</option>
									<option value="product.item" <?=($row["ogtype"] == 'product.item' ? 'selected' : ''); ?>>product.item</option>
									<option value="profile" <?=($row["ogtype"] == 'profile' ? 'selected' : ''); ?>>profile</option>
									<option value="restaurant.menu" <?=($row["ogtype"] == 'restaurant.menu' ? 'selected' : ''); ?>>restaurant.menu</option>
									<option value="restaurant.item" <?=($row["ogtype"] == 'restaurant.item' ? 'selected' : ''); ?>>restaurant.item</option>
									<option value="restaurant.section" <?=($row["ogtype"] == 'restaurant.section' ? 'selected' : ''); ?>>restaurant.section</option>
									<option value="restaurant.restaurant" <?=($row["ogtype"] == 'restaurant.restaurant' ? 'selected' : ''); ?>>restaurant.restaurant</option>
									<option value="video.episode" <?=($row["ogtype"] == 'video.episode' ? 'selected' : ''); ?>>video.episode</option>
									<option value="video.movie" <?=($row["ogtype"] == 'video.movie' ? 'selected' : ''); ?>>video.movie</option>
									<option value="video.other" <?=($row["ogtype"] == 'video.other' ? 'selected' : ''); ?>>video.other</option>
									<option value="video.tv_show" <?=($row["ogtype"] == 'video.tv_show' ? 'selected' : ''); ?>>video.tv_show</option>
									<option value="website" <?=($row["ogtype"] == 'website' ? 'selected' : ''); ?>>website</option>
								</select>
								<small>Para saber mais, visite a <a href="https://developers.facebook.com/docs/opengraph/getting-started" target="_blank"><strong>documentação oficial</strong></a> do Facebook</small>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
							</div>
						</div>
					</div>
					
				</form>
								
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>