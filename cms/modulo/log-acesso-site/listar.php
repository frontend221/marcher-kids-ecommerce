	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				                
				<!-- BODY -->
				
				<div class="pad-btm form-inline">
					<div class="row">
					    <div class="col-sm-6 table-toolbar-left">

						</div>
					    <div class="col-sm-6 table-toolbar-right"></div>
					</div>
				</div>
				
                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="56%">IP</th>
					            <th class="text-center" width="12%" style="text-align: center !important;">Acessos</th>
					            <th class="text-center" width="12%" style="text-align: center !important;">Primeiro Acesso</th>
					            <th class="text-center" width="12%" style="text-align: center !important;">Último Acesso</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "*";
							
							$where = "WHERE `deleted_at` IS NULL";
														
							$final = "FROM `log-acesso-site` $where ORDER BY `updated_at` DESC";
							
							$sql = mysqli_query($conn, "SELECT $campos $final");
							
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["ip"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["contador"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=date("d/m/Y H:i:s", strtotime($ln["created_at"]));?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=date("d/m/Y H:i:s", strtotime($ln["updated_at"]));?></td>
					        </tr>
							
							<?php } ?>

						</tbody>
						
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>