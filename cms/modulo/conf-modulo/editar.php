	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
												   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_u); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `conf-modulo` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>
				
				<form class="form-horizontal" action="<?php echo URL ?>/modulo/<?=$exp[0];?>/funcao.php" method="post">
					
					<input type="hidden" name="rid" value="<?=$id_registro;?>" required>
					<input type="hidden" name="mod" value="<?=$exp[0];?>" required>
					<input type="hidden" name="act" value="update" required>
					<input type="hidden" name="tkn" value="<?=$_SESSION["token"];?>" required>
					<input type="hidden" name="pag" value="<?=$exp[2];?>" required>
					<input type="hidden" name="reg" value="<?=$permissao_u;?>" required>
					
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="grupo">Grupo do Módulo</label>
							<div class="col-sm-8">
								<select name="grupo" id="secao" data-placeholder="Selecione o grupo do módulo" tabindex="0" data-width="100%">
									<?php
									$menu = mysqli_query($conn, "SELECT * FROM `conf-grupo-modulo` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `nome` ASC");
									if(mysqli_num_rows($menu) > 0) {
									?>
									<option value="" selected>Selecione o grupo do módulo</option>
									<?php
									while($ln = mysqli_fetch_array($menu)){
									?>	
									<option value="<?=$ln["id"];?>" <?=($ln["id"] == $row["grupo"] ? "selected" : "");?>><?=$ln["nome"];?></option>
									<?php	
									}		
									} else {
									?>
									<option value="">Você deve cadastrar ao menos um grupo de módulo para prosseguir com o formulário</option>
									<?php
									}				
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="titulo">Descrição</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Descrição do Módulo" id="titulo" name="titulo" class="form-control" value="<?=$row["descricao"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="menu">Nome no Menu</label>
							<div class="col-sm-8">
								<input type="text" placeholder="Nome do Módulo no Menu" id="menu1" name="menu1" class="form-control" value="<?=$row["titulo"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="diretorio">Diretório da Aplicação</label>
							<div class="col-sm-8">
								<select name="diretorio" id="diretorio" data-placeholder="Selecione o diretório da aplicação" tabindex="0" data-width="100%">
									<option value="" selected>Selecione o diretório da aplicação</option>
									<?php
													
									$diretorio = "modulo/*";

									foreach(glob($diretorio, GLOB_ONLYDIR) as $dir) {
										echo '<option value="'.basename($dir).'" '.(basename($dir) == $row["diretorio"] ? "selected" : "").'>'.basename($dir).'</option>';
									}

									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="icone">Ícone do Módulo<br><small class="help-block"><a style="color: #C90D10;" href="https://fontawesome.com/icons?d=gallery&m=free" target="_blank">[ ver ícones ]</a></small></label>
							<div class="col-sm-8">
								<input type="text" placeholder="Informe a classe do ícone que será utilizado" id="icone" name="icone" class="form-control" value="<?=$row["icone"];?>" required>
								<small>Utilizar apenas a classe do ícone que se encontra entre as aspas, por exemplo: <code>far fa-address-book</code></small>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="posicao">Posição no Menu</label>
							<div class="col-sm-8">
								<input type="number" min="0" placeholder="Posição para exibição" id="posicao" name="posicao" class="form-control" value="<?=$row["posicao"];?>" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="status">Status do Registro</label>
							<div class="col-md-9">
								<div class="radio">
									<input id="status1" class="magic-radio" type="radio" name="status" value="S" <?=($row["status"] == "S" ? "checked" : "");?>>
									<label for="status1">Ativo</label>
									<input id="status2" class="magic-radio" type="radio" name="status" value="N" <?=($row["status"] == "N" ? "checked" : "");?>>
									<label for="status2">Inativo</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-footer">
						<div class="row">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Gravar registro" type="submit"><i class="far fa-save"></i> Gravar</button>
								<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>
							</div>
						</div>
					</div>
					
				</form>
				
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>