<?php

	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		require_once ("../../config/cfg-path.php");
		require_once ("../../config/cfg-device.php");

		$mod = $_POST["mod"];
		$tkn = $_POST["tkn"];
		$reg = $_POST["reg"];
		$pag = $_POST["pag"];

		$permissao = array($reg);
		
		if(in_array("S", $permissao)){
		
			if($tkn === $_SESSION["token"]){
		
				$act = $_POST["act"];

				require_once ("../../config/cfg-database.php");

				function SlugGenerator($str){
					$str = strtolower(utf8_decode($str)); $i=1;
					$str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
					$str = preg_replace("/([^a-z0-9])/", '-', utf8_encode($str));
					while($i > 0) $str = str_replace('--', '-', $str, $i);
					if (substr($str, -1) == '-') $str = substr($str, 0, -1);
					return $str;
				}
								
				if ($act == "store") {
					
					if($_POST["grupo"] == "") {
						echo('<script language = "javascript"> alert("Selecione o grupo do módulo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$grupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["grupo"])));
					}
						
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe uma descrição para o módulo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
						
					if($_POST["menu1"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome que aparecerá no menu."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$menu = mysqli_real_escape_string($conn, stripslashes(trim($_POST["menu1"])));
					}
						
					if($_POST["diretorio"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome do diretório da aplicação."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$diretorio = mysqli_real_escape_string($conn, stripslashes(trim($_POST["diretorio"])));
					}
						
					if($_POST["icone"] == "") {
						$icone = "fas fa-bars";
					} else {
						$icone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["icone"])));
					}
					
					if($_POST["posicao"] == "") {
						$posicao = 1;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
						
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
										
					$qry = mysqli_query($conn, "SELECT COUNT(menu) AS TOTAL FROM `conf-modulo` WHERE `menu`='".$menu."' AND `diretorio`='".$diretorio."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/4");

					} else {

						$sql = mysqli_query($conn, "INSERT INTO `conf-modulo`(`id`, `grupo`, `descricao`, `titulo`, `icone`, `posicao`, `diretorio`, `status`, `created_at`) VALUES (0,'".$grupo."','".$titulo."','".$menu."','".$icone."','".$posicao."','".$diretorio."','".$status."',NOW())");
						
						$ultimo_id = mysqli_insert_id($conn);

						if ($sql) {

							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-modulo','".utf8_encode("Inserido novo registro.")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
												
							$tipo = mysqli_query($conn, "SELECT DISTINCT `id` FROM `usu-tipo`");
							
							while($ln = mysqli_fetch_array($tipo)){
								
								$query = mysqli_query($conn, "INSERT INTO `usu-permissao-tipo`(`id`, `tipo`, `modulo`, `diretorio`, `habilitado`, `criar`, `ler`, `alterar`, `apagar`) VALUES (0,'".$ln["id"]."','".$ultimo_id."','".$diretorio."','N','N','N','N','N')");

							}
							
							if($query) {
								
								header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
							} else {
								
								header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
								
							}
							

						} else {

							header("Location: " . URL . "/".$mod."/listar/".$pag."/3");

						}

					}
										
				} else if ($act == "update") {
										
					$id_registro = $_POST["rid"];
					
					if($_POST["grupo"] == "") {
						echo('<script language = "javascript"> alert("Selecione o grupo do módulo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$grupo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["grupo"])));
					}
						
					if($_POST["titulo"] == "") {
						echo('<script language = "javascript"> alert("Informe uma descrição para o módulo."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$titulo = mysqli_real_escape_string($conn, stripslashes(trim($_POST["titulo"])));
					}
						
					if($_POST["menu1"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome que aparecerá no menu."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$menu = mysqli_real_escape_string($conn, stripslashes(trim($_POST["menu1"])));
					}
						
					if($_POST["diretorio"] == "") {
						echo('<script language = "javascript"> alert("Informe o nome do diretório da aplicação."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$diretorio = mysqli_real_escape_string($conn, stripslashes(trim($_POST["diretorio"])));
					}
						
					if($_POST["icone"] == "") {
						$icone = "fas fa-bars";
					} else {
						$icone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["icone"])));
					}
					
					if($_POST["posicao"] == "") {
						$posicao = 1;
					} else {
						$posicao = mysqli_real_escape_string($conn, stripslashes(trim($_POST["posicao"])));
					}
						
					if($_POST["status"] == "") {
						echo('<script language = "javascript"> alert("Selecione o status do registro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$status = mysqli_real_escape_string($conn, stripslashes(trim($_POST["status"])));
					}
					
					$qry = mysqli_query($conn, "SELECT COUNT(menu) AS TOTAL FROM `conf-modulo` WHERE `menu`='".$menu."' AND `diretorio` <> '".$diretorio."' AND `id` <> '".$id_registro."' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 1) {

						header("Location: " . URL . "/".$mod."/editar/".$pag."/".$id_registro."/3");

					} else {
																								
						$sql = mysqli_query($conn, "UPDATE `conf-modulo` SET `grupo`='".$grupo."',`descricao`='".$titulo."',`titulo`='".$menu."',`icone`='".$icone."',`posicao`='".$posicao."',`diretorio`='".$diretorio."',`status`='".$status."',`updated_at`=NOW() WHERE `id`='".$id_registro."'");

						if ($sql) {
							
							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-modulo','".utf8_encode("Alteração do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
								
							header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
								
						} else {

							header("Location: " . URL . "/".$mod."/listar/".$pag."/2");

						}
												
					}
					
				} else if ($act == "delete") {
					
					$id_registro = $_POST["rid"];
					
					$sql = mysqli_query($conn, "UPDATE `conf-modulo` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$id_registro."'");
					
					if ($sql) {
						
						$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','conf-modulo','".utf8_encode("Exclusão do registro ID: ".$id_registro.".")."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/1");
						
					} else {
						
						header("Location: " . URL . "/".$mod."/listar/".$pag."/2");
						
					}
					
				}

			} else {
				echo('<script language = "javascript">alert("Acesso negado: token inválido.")</script>');
				echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
				exit;
			}
		
		} else {
			echo('<script language = "javascript">alert("Acesso negado: permissão negada.")</script>');
			echo('<script language = "javascript">window.location=("' . URL . '/dashboard/listar/1")</script>');
			exit;
		}

	} else {
		echo('<script language = "javascript">alert("Acesso negado!")</script>');
		echo('<script language = "javascript">window.location=("' . URL . '/logout.php")</script>');
		exit;
	}

	ob_end_flush();

?>