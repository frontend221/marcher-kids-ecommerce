	<?php

		if (preg_match("/listar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">

			<div class="panel-body">
				
				<?php $permissao = array($permissao_r); ?>

				<?php if(in_array("S", $permissao)){ ?>
				                
				<!-- BODY -->
				
				<div class="pad-btm form-inline">
					<div class="row">
					    <div class="col-sm-6 table-toolbar-left">

						</div>
					    <div class="col-sm-6 table-toolbar-right"></div>
					</div>
				</div>
				
                <div class="table-responsive">
					<table id="item" class="table table-striped" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center" width="6%" style="text-align: center !important;">#</th>
					            <th width="19%">Nome</th>
					            <th width="16%">E-mail</th>
					            <th width="11%">Telefone</th>
					            <th width="26%">Assunto</th>
					            <th width="11%" style="text-align: center !important;">Data</th>
					            <th width="12%" style="text-align: center !important;">Ações</th>
					        </tr>
					    </thead>
					    <tbody id="myTable">
							
							<?php
																				
							$campos = "*";
							
							$where = "WHERE `deleted_at` IS NULL";
							
							$final = "FROM `msg-mail-recebido` $where ORDER BY `created_at` DESC";
							
							$sql = mysqli_query($conn, "SELECT $campos $final");
							
							?>
							
							<?php while ($ln = mysqli_fetch_array($sql)) { ?>
														
					        <tr>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=$ln["id"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["nome"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["email"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["fone"];?></td>
					            <td style="vertical-align: middle !important;"><?=$ln["assunto"];?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;"><?=date("d/m/Y H:i:s", strtotime($ln["created_at"]));?></td>
					            <td style="text-align: center !important; vertical-align: middle !important;">
									<div class="btn-group">
										<a href="<?php echo URL ?>/<?=$exp[0];?>/detalhar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-info add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Informações do registro">
											<i class="fas fa-info-circle"></i>
										</a>
										<a href="<?php echo URL ?>/<?=$exp[0];?>/apagar/<?=$exp[2];?>/<?=$ln["id"];?>" class="btn btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="left" data-original-title="Apagar registro">
											<i class="far fa-trash-alt"></i>
										</a>
									</div>
								</td>
					        </tr>
							
							<?php } ?>

						</tbody>
						
					</table>
					
				</div>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>