	<?php

		if (preg_match("/editar.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
												   
            <div class="panel-body">
				
				<?php $permissao = array($permissao_u); ?>

				<?php if(in_array("S", $permissao)){ ?>
				
				<?php if(isset($exp[4]) && is_numeric($exp[4]) && $exp[4] != "") { include("include/inc-infos.php"); } ?>
				
				<!-- BODY -->
				
				<?php

					$sql = mysqli_query($conn, "SELECT * FROM `msg-mail-recebido` WHERE `id`='".$exp[3]."' AND `deleted_at` IS NULL");
													
					if(mysqli_num_rows($sql) == 1) {
						
						$row = mysqli_fetch_array($sql);
						
						$id_registro = $row["id"];
                
				?>
				
				<div class="container">
													
					<div class="table-responsive">

						<table id="item" class="table table-striped" style="width:100%">
							<tbody id="myTable">
								<tr>
									<td width="17%"><strong>Nome do Remetente</strong></td>
									<td><?=$row["nome"];?></td>
								</tr>
								<tr>
									<td><strong>E-mail</strong></td>
									<td><?=$row["email"];?></td>
								</tr>
								<tr>
									<td><strong>Telefone</strong></td>
									<td><?=$row["fone"];?></td>
								</tr>
								<tr>
									<td><strong>Assunto</strong></td>
									<td><?=$row["assunto"];?></td>
								</tr>
								<tr>
									<td><strong>Mensagem</strong></td>
									<td><?=$row["mensagem"];?></td>
								</tr>
								<tr>
									<td><strong>Recebido em</strong></td>
									<td><?=date("d/m/Y H:i:s", strtotime($row["created_at"]));?></td>
								</tr>
							</tbody>
						</table>
						<tfoot>
							<tr>
								<td colspan="2"><a href="<?php echo URL ?>/<?=$exp[0];?>/listar/1" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a></td>
							</tr>
						</tfoot>					

					</div>
					
				</div>
				
				<?php } else { ?>
				
				<div class="alert alert-warning" role="alert"> 
					Não existe registro com este identificador.
				</div>
				
				<a href="<?php echo URL ?>/<?=$exp[0];?>/listar/<?=$exp[2];?>" class="btn btn-warning add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cancelar" type="reset"><i class="fas fa-undo-alt"></i> Cancelar</a>

				<?php } ?>
				
				<!-- END BODY -->
				
				<?php } else { ?>
							
				    <div class="alert alert-danger" role="alert">
					    Você não tem <strong>permissão</strong> para acesso a este módulo. Verifique com seu <strong>administrador do sistema</strong>.
				    </div>						
																																		
			    <?php } ?>
				
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php");
		}

	?>