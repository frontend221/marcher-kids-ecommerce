	<?php

		if (preg_match("/404.php/", $_SERVER["SCRIPT_NAME"])) { 
			header("Location: " . URL . "/logout.php"); 
		}

		if(isset($_SESSION["token"]) && $_SESSION["token"] != "") {

	?>

		<div class="panel">
			
            <div class="panel-body">
				
				<div class="text-center" style="margin: 250px 0px;">
					<h1 style="font-size: 112px !important">404</h1>
					<h4 style="text-transform: uppercase;">Módulo inexistente</h4>
				</div>
								
			</div>
			
		</div>

	<?php

		} else {
			header("Location: " . URL . "/logout.php"); 
		}

	?>