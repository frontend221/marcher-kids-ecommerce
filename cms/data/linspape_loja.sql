-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 07-Fev-2020 às 22:09
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `linspape_loja`
--
CREATE DATABASE IF NOT EXISTS `linspape_loja` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `linspape_loja`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `arq-carrossel`
--

DROP TABLE IF EXISTS `arq-carrossel`;
CREATE TABLE IF NOT EXISTS `arq-carrossel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `somenteimagem` char(1) COLLATE utf8mb4_bin NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `subtitulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `arq-catalogo`
--

DROP TABLE IF EXISTS `arq-catalogo`;
CREATE TABLE IF NOT EXISTS `arq-catalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `capa` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `arquivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog-categoria`
--

DROP TABLE IF EXISTS `blog-categoria`;
CREATE TABLE IF NOT EXISTS `blog-categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog-post`
--

DROP TABLE IF EXISTS `blog-post`;
CREATE TABLE IF NOT EXISTS `blog-post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `usuario` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad-info-contato`
--

DROP TABLE IF EXISTS `cad-info-contato`;
CREATE TABLE IF NOT EXISTS `cad-info-contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local` int(11) NOT NULL,
  `cnpj` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `inscricao` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `razaosocial` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `fantasia` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `responsavel` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `cep` varchar(9) COLLATE utf8mb4_bin NOT NULL,
  `logradouro` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `numero` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `complemento` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `email` text COLLATE utf8mb4_bin NOT NULL,
  `telefone` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `celular` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `whatsapp` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `areamapa` longtext COLLATE utf8mb4_bin NOT NULL,
  `urlmapa` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `local` (`local`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cad-seo`
--

DROP TABLE IF EXISTS `cad-seo`;
CREATE TABLE IF NOT EXISTS `cad-seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin NOT NULL,
  `keywords` text COLLATE utf8mb4_bin NOT NULL,
  `robot` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `distribution` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `placename` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `revisitafter` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogtitle` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogsitename` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogurl` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogdescription` text COLLATE utf8mb4_bin NOT NULL,
  `oglocale` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `oglocalealternate` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogimage` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogimagetype` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ogimagewidth` int(8) NOT NULL,
  `ogimageheight` int(8) NOT NULL,
  `ogtype` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cli-cadastro`
--

DROP TABLE IF EXISTS `cli-cadastro`;
CREATE TABLE IF NOT EXISTS `cli-cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipopessoa` char(2) COLLATE utf8mb4_bin NOT NULL COMMENT 'PF = Pessoa Física / PJ = Pessoa Jurídica',
  `razaosocial` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `nomefantasia` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `cpfcnpj` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `inscricaoestadual` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `celular` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `pessoacontato` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `senha` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `token_acesso` text COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpfcnpj` (`cpfcnpj`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cli-endereco`
--

DROP TABLE IF EXISTS `cli-endereco`;
CREATE TABLE IF NOT EXISTS `cli-endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` int(11) NOT NULL,
  `cep` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `logradouro` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `numero` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `complemento` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `cidade` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `estado` char(2) COLLATE utf8mb4_bin NOT NULL,
  `principal` char(1) COLLATE utf8mb4_bin NOT NULL COMMENT 'S = Principal / N = Outro endereço',
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-gateway`
--

DROP TABLE IF EXISTS `conf-gateway`;
CREATE TABLE IF NOT EXISTS `conf-gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chaveapi` text COLLATE utf8mb4_bin NOT NULL,
  `chavecriptografia` text COLLATE utf8mb4_bin NOT NULL,
  `banco` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `agencia` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `agenciadv` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `conta` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `contadv` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `tipoconta` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cpfcnpj` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `nome` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `idconta` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `idrecebedor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-grupo-modulo`
--

DROP TABLE IF EXISTS `conf-grupo-modulo`;
CREATE TABLE IF NOT EXISTS `conf-grupo-modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu` (`menu`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `conf-grupo-modulo`
--

INSERT INTO `conf-grupo-modulo` (`id`, `menu`, `nome`, `posicao`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Dashboard', 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(2, 1, 'Configurações', 2, 'S', '2020-01-23 15:00:00', NULL, NULL),
(3, 1, 'Usuários e Permissões', 3, 'S', '2020-01-23 15:00:00', NULL, NULL),
(4, 1, 'Localização', 4, 'S', '2020-01-23 15:00:00', NULL, NULL),
(5, 1, 'Cadastro Geral', 5, 'S', '2020-01-23 15:00:00', NULL, NULL),
(6, 1, 'Social', 6, 'S', '2020-01-28 18:00:00', NULL, NULL),
(7, 1, 'Imagens', 7, 'S', '2020-01-23 15:00:00', NULL, NULL),
(8, 1, 'Textos', 8, 'S', '2020-01-23 15:00:00', NULL, NULL),
(9, 1, 'Blog', 9, 'S', '2020-01-23 15:00:00', NULL, NULL),
(10, 1, 'Classificação do Produto', 10, 'S', '2020-01-23 15:00:00', NULL, NULL),
(11, 1, 'Produto', 11, 'S', '2020-01-23 15:00:00', NULL, NULL),
(12, 1, 'Catálogo', 12, 'S', '2020-01-23 15:00:00', NULL, NULL),
(13, 1, 'Clientes', 13, 'S', '2020-01-23 15:00:00', NULL, NULL),
(14, 1, 'Pedidos', 14, 'S', '2020-01-23 15:00:00', NULL, NULL),
(15, 1, 'Contato', 15, 'S', '2020-01-23 15:00:00', NULL, NULL),
(16, 1, 'Log do Sistema ', 99, 'S', '2020-01-23 15:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-manutencao`
--

DROP TABLE IF EXISTS `conf-manutencao`;
CREATE TABLE IF NOT EXISTS `conf-manutencao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `conf-manutencao`
--

INSERT INTO `conf-manutencao` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'N', '2019-11-06 03:00:00', '2019-11-07 17:46:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-menu`
--

DROP TABLE IF EXISTS `conf-menu`;
CREATE TABLE IF NOT EXISTS `conf-menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `conf-menu`
--

INSERT INTO `conf-menu` (`id`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CMS', 'S', '2020-01-23 15:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-modulo`
--

DROP TABLE IF EXISTS `conf-modulo`;
CREATE TABLE IF NOT EXISTS `conf-modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` int(11) NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `icone` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(11) NOT NULL,
  `diretorio` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grupo` (`grupo`) USING BTREE,
  KEY `diretorio` (`diretorio`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `conf-modulo`
--

INSERT INTO `conf-modulo` (`id`, `grupo`, `descricao`, `titulo`, `icone`, `posicao`, `diretorio`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Dashboard', 'Dashboard', 'fas fa-tachometer-alt', 1, 'dashboard', 'S', '2020-01-23 15:00:00', NULL, NULL),
(2, 2, 'Menu', 'Menu', 'fas fa-bars', 1, 'conf-menu', 'S', '2020-01-23 15:00:00', NULL, NULL),
(3, 2, 'Grupo de Módulo', 'Grupo de Módulo', 'fas fa-cubes', 2, 'conf-grupo-modulo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(4, 2, 'Módulo', 'Módulo', 'fas fa-cube', 3, 'conf-modulo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(5, 2, 'Página do Website', 'Página do Website', 'far fa-file-code', 4, 'conf-pagina-site', 'S', '2020-01-23 15:00:00', NULL, NULL),
(6, 2, 'Seo e Metatag', 'Seo e Metatag', 'fas fa-code', 5, 'cad-seo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(7, 2, 'Servidor SMTP', 'Servidor SMTP', 'fas fa-at', 6, 'conf-smtp', 'S', '2020-01-23 15:00:00', NULL, NULL),
(8, 2, 'Configuração do Gateway', 'Configuração do Gateway', 'far fa-credit-card', 7, 'conf-gateway', 'S', '2020-01-23 15:00:00', NULL, NULL),
(9, 3, 'Tipo de Usuário', 'Tipo de Usuário', 'fas fa-user-tag', 1, 'usu-tipo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(10, 3, 'Grupo de Módulo por Tipo de Usuário', 'Grupo de Módulo por Tipo de Usuário', 'fas fa-user-shield', 2, 'usu-grupo-tipo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(11, 3, 'Permissão por Tipo de Usuário', 'Permissão por Tipo de Usuário', 'fas fa-user-cog', 3, 'usu-permissao-tipo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(12, 3, 'Usuário do CMS', 'Usuário do CMS', 'fas fa-users', 4, 'usu-sistema', 'S', '2020-01-23 15:00:00', NULL, NULL),
(13, 4, 'País', 'País', 'fas fa-globe-americas', 1, 'local-pais', 'S', '2020-01-23 15:00:00', NULL, NULL),
(14, 4, 'Estado', 'Estado', 'fas fa-map-marked-alt', 2, 'local-estado', 'S', '2020-01-23 15:00:00', NULL, NULL),
(15, 4, 'Cidade', 'Cidade', 'fas fa-map-marker-alt', 3, 'local-cidade', 'S', '2020-01-23 15:00:00', NULL, NULL),
(16, 5, 'Informação de Contato', 'Informação de Contato', 'fas fa-info', 1, 'cad-info-contato', 'S', '2020-01-23 15:00:00', NULL, NULL),
(17, 5, 'Modo de Manutenção', 'Modo de Manutenção', 'fas fa-exclamation-triangle', 2, 'conf-manutencao', 'S', '2020-01-23 15:00:00', NULL, NULL),
(18, 6, 'Rede Social', 'Rede Social', 'fab fa-facebook-square', 3, 'social-rede', 'S', '2020-01-23 15:00:00', NULL, NULL),
(19, 6, 'URL da Rede Social', 'URL da Rede Social', 'fas fa-link', 4, 'social-url', 'S', '2020-01-23 15:00:00', NULL, NULL),
(22, 7, 'Carrossel', 'Carrossel', 'far fa-images', 2, 'arq-carrossel', 'S', '2020-01-23 15:00:00', NULL, NULL),
(23, 8, 'Sobre a Empresa', 'Sobre a Empresa', 'fas fa-align-left', 1, 'txt-sobre-empresa', 'S', '2020-01-23 15:00:00', NULL, NULL),
(24, 8, 'Privacidade e Segurança', 'Privacidade e Segurança', 'fas fa-align-left', 2, 'txt-privacidade-seguranca', 'S', '2020-01-23 15:00:00', NULL, NULL),
(25, 8, 'Como Comprar', 'Como Comprar', 'fas fa-align-left', 3, 'txt-como-comprar', 'S', '2020-01-23 15:00:00', NULL, NULL),
(26, 8, 'Pagamento e Frete', 'Pagamento e Frete', 'fas fa-align-left', 4, 'txt-pagamento-frete', 'S', '2020-01-23 15:00:00', NULL, NULL),
(27, 8, 'Política de Entrega', 'Política de Entrega', 'fas fa-align-left', 5, 'txt-politica-entrega', 'S', '2020-01-23 15:00:00', NULL, NULL),
(28, 8, 'Política de Troca', 'Política de Troca', 'fas fa-align-left', 6, 'txt-politica-troca', 'S', '2020-01-23 15:00:00', NULL, NULL),
(29, 9, 'Categoria do Blog', 'Categoria do Blog', 'fas fa-list', 1, 'blog-categoria', 'S', '2020-01-23 15:00:00', NULL, NULL),
(30, 9, 'Blog', 'Blog', 'fas fa-blog', 2, 'blog-post', 'S', '2020-01-23 15:00:00', NULL, NULL),
(31, 10, 'Categoria', 'Categoria', 'fas fa-layer-group', 1, 'prod-categoria', 'S', '2020-01-23 15:00:00', NULL, NULL),
(32, 10, 'Grupo', 'Grupo', 'fas fa-object-group', 2, 'prod-grupo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(33, 10, 'Subgrupo', 'Subgrupo', 'fas fa-object-ungroup', 3, 'prod-subgrupo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(34, 10, 'Marca', 'Marca', 'far fa-registered', 4, 'prod-marca', 'S', '2020-01-23 15:00:00', NULL, NULL),
(35, 10, 'Modelo', 'Modelo', 'far fa-list-alt', 5, 'prod-modelo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(36, 10, 'Cor', 'Cor', 'fas fa-palette', 6, 'prod-cor', 'S', '2020-01-23 15:00:00', NULL, NULL),
(37, 10, 'Tamanho', 'Tamanho', 'fas fa-expand-alt', 7, 'prod-tamanho', 'S', '2020-01-23 15:00:00', NULL, NULL),
(38, 10, 'Gramatura', 'Gramatura', 'fas fa-balance-scale-right', 8, 'prod-gramatura', 'S', '2020-01-23 15:00:00', NULL, NULL),
(39, 11, 'Produto', 'Produto', 'fas fa-tag', 1, 'prod-item', 'S', '2020-01-23 15:00:00', NULL, NULL),
(40, 11, 'Produto Estoque', 'Produto Estoque', 'fas fa-sort-amount-down-alt', 2, 'prod-estoque', 'S', '2020-01-23 15:00:00', NULL, NULL),
(41, 11, 'Produto Mais Vendido', 'Produto Mais Vendido', 'fas fa-dollar-sign', 3, 'prod-mais-vendido', 'S', '2020-01-23 15:00:00', NULL, NULL),
(42, 11, 'Produto Visitado', 'Produto Visitado', 'fas fa-eye', 4, 'prod-visitado', 'S', '2020-01-23 15:00:00', NULL, NULL),
(43, 12, 'Catálogo', 'Catálogo', 'fas fa-file-upload', 1, 'arq-catalogo', 'S', '2020-01-23 15:00:00', NULL, NULL),
(44, 13, 'Cadastro de Cliente', 'Cadastro de Cliente', 'far fa-user-circle', 1, 'cli-cadastro', 'S', '2020-01-23 15:00:00', NULL, NULL),
(45, 14, 'Pedido', 'Pedido', 'fas fa-dolly', 1, 'ped-cadastro', 'S', '2020-01-23 15:00:00', NULL, NULL),
(46, 14, 'Pedido Retorno', 'Pedido Retorno', 'fas fa-exchange-alt', 2, 'ped-retorno', 'S', '2020-01-23 15:00:00', NULL, NULL),
(47, 15, 'E-mail Recebido', 'E-mail Recebido', 'fas fa-inbox', 1, 'msg-mail-recebido', 'S', '2020-01-23 15:00:00', NULL, NULL),
(48, 15, 'Newsletter', 'Newsletter', 'fas fa-rss', 2, 'msg-newsletter', 'S', '2020-01-23 15:00:00', NULL, NULL),
(49, 16, 'Log de Acesso ao CMS', 'Log de Acesso ao CMS', 'fas fa-list-ol', 1, 'log-acesso-cms', 'S', '2020-01-23 15:00:00', NULL, NULL),
(50, 16, 'Log de Acesso ao Website', 'Log de Acesso ao Website', 'fas fa-list-ol', 2, 'log-acesso-site', 'S', '2020-01-23 15:00:00', NULL, NULL),
(51, 16, 'Log de Envio de E-mail', 'Log de Envio de E-mail', 'fas fa-list-ol', 3, 'log-envio-mail', 'S', '2020-01-23 15:00:00', NULL, NULL),
(52, 16, 'Log de Execução', 'Log de Execução', 'fas fa-list-ol', 4, 'log-execucao', 'S', '2020-01-23 15:00:00', NULL, NULL),
(53, 14, 'Carrinho Abadonado', 'Carrinho Abandonado', 'fas fa-shopping-cart', 3, 'ped-carrinho-abandonado', 'S', '2020-02-07 14:20:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-pagina-site`
--

DROP TABLE IF EXISTS `conf-pagina-site`;
CREATE TABLE IF NOT EXISTS `conf-pagina-site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `arquivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `extensao` varchar(5) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `conf-pagina-site`
--

INSERT INTO `conf-pagina-site` (`id`, `nome`, `arquivo`, `extensao`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home', 'home', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(2, 'Papéis', 'papeis', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(3, 'Papelaria', 'papelaria', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(4, 'Presentes', 'presentes', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(5, 'Produto', 'produto', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(6, 'Catálogo', 'catalogo', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(7, 'Blog', 'blog', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(8, 'Post', 'post', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(9, 'Sobre a Lins', 'sobre', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(10, 'Contato', 'contato', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(11, 'Meu Carrinho', 'carrinho', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(12, 'Cadastro', 'cadastro', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(13, 'Acesso à Conta', 'login', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(14, 'Minha Conta', 'minha-conta', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(15, 'Recuperar Senha', 'recuperar-senha', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(16, 'Privacidade e Segurança', 'privacidade-e-seguranca', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(17, 'Política de Entrega', 'politica-de-entrega', '.php', 'S', '2020-01-23 15:00:00', NULL, NULL),
(18, 'Política de Troca e Devolução', 'politica-de-troca-e-devolucao', '.php', 'S', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conf-smtp`
--

DROP TABLE IF EXISTS `conf-smtp`;
CREATE TABLE IF NOT EXISTS `conf-smtp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `host` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `porta` int(5) NOT NULL,
  `usuario` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `senha` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `seguranca` char(3) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `local-cidade`
--

DROP TABLE IF EXISTS `local-cidade`;
CREATE TABLE IF NOT EXISTS `local-cidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `local-estado`
--

DROP TABLE IF EXISTS `local-estado`;
CREATE TABLE IF NOT EXISTS `local-estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pais` (`pais`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `local-pais`
--

DROP TABLE IF EXISTS `local-pais`;
CREATE TABLE IF NOT EXISTS `local-pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `log-acesso-cms`
--

DROP TABLE IF EXISTS `log-acesso-cms`;
CREATE TABLE IF NOT EXISTS `log-acesso-cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) NOT NULL,
  `acao` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `log-acesso-cms`
--

INSERT INTO `log-acesso-cms` (`id`, `usuario`, `acao`, `ip`, `dispositivo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(36, 1, 'Acesso ao painel de controle', '::1', 'Chrome', '2020-01-28 18:03:50', NULL, NULL),
(37, 1, 'Acesso ao painel de controle', '::1', 'Chrome', '2020-01-28 18:22:06', NULL, NULL),
(38, 1, 'Acesso ao painel de controle', '::1', 'Chrome', '2020-02-06 19:48:24', NULL, NULL),
(39, 1, 'Expirado por tempo de inatividade', '::1', 'Chrome', '2020-02-07 12:37:27', NULL, NULL),
(42, 1, 'Acesso ao painel de controle', '::1', 'Chrome', '2020-02-07 12:37:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `log-acesso-site`
--

DROP TABLE IF EXISTS `log-acesso-site`;
CREATE TABLE IF NOT EXISTS `log-acesso-site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `contador` int(11) NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `log-envio-mail`
--

DROP TABLE IF EXISTS `log-envio-mail`;
CREATE TABLE IF NOT EXISTS `log-envio-mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `acao` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `mensagem` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `log-execucao`
--

DROP TABLE IF EXISTS `log-execucao`;
CREATE TABLE IF NOT EXISTS `log-execucao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) NOT NULL,
  `tabela` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `acao` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `log-execucao`
--

INSERT INTO `log-execucao` (`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'conf-modulo', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 14:20:18', NULL, NULL),
(2, 1, 'prod-categoria', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:18:39', NULL, NULL),
(3, 1, 'prod-grupo', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:20:23', NULL, NULL),
(4, 1, 'prod-subgrupo', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:21:44', NULL, NULL),
(5, 1, 'prod-marca', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:25:29', NULL, NULL),
(6, 1, 'prod-modelo', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:30:18', NULL, NULL),
(7, 1, 'prod-cor', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:34:07', NULL, NULL),
(8, 1, 'prod-tamanho', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:35:58', NULL, NULL),
(9, 1, 'prod-gramatura', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:40:34', NULL, NULL),
(10, 1, 'prod-item', 'Inserido novo registro.', '::1', 'Chrome', '2020-02-07 20:41:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `msg-mail-recebido`
--

DROP TABLE IF EXISTS `msg-mail-recebido`;
CREATE TABLE IF NOT EXISTS `msg-mail-recebido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `fone` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `assunto` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `mensagem` longtext COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `msg-newsletter`
--

DROP TABLE IF EXISTS `msg-newsletter`;
CREATE TABLE IF NOT EXISTS `msg-newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `origem` char(1) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ped-cadastro`
--

DROP TABLE IF EXISTS `ped-cadastro`;
CREATE TABLE IF NOT EXISTS `ped-cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` int(11) NOT NULL,
  `endereco` int(11) NOT NULL,
  `valortotal` decimal(10,2) NOT NULL,
  `opcaofrete` varchar(5) COLLATE utf8mb4_bin NOT NULL COMMENT 'Sedex, PAC',
  `valorfrete` decimal(10,2) NOT NULL,
  `formapagamento` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `quantidadeparcelas` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `urlboleto` text COLLATE utf8mb4_bin DEFAULT NULL,
  `codebarboleto` text COLLATE utf8mb4_bin DEFAULT NULL,
  `codrastreamentofrete` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `notafiscal` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `visualizado` int(1) DEFAULT 0 COMMENT '0 = Não / 1 = Sim',
  `status` char(3) COLLATE utf8mb4_bin NOT NULL COMMENT 'ANA = Em Análise / CON = Pagamento Confirmado / FAT = Pedido Faturado / EXP = Em Expediçao / ENV =Enviado / CAN = Cancelado / OPR = Recusado Pela Operadora',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente` (`cliente`),
  KEY `endereco` (`endereco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ped-item`
--

DROP TABLE IF EXISTS `ped-item`;
CREATE TABLE IF NOT EXISTS `ped-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ped-retorno`
--

DROP TABLE IF EXISTS `ped-retorno`;
CREATE TABLE IF NOT EXISTS `ped-retorno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-categoria`
--

DROP TABLE IF EXISTS `prod-categoria`;
CREATE TABLE IF NOT EXISTS `prod-categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-categoria`
--

INSERT INTO `prod-categoria` (`id`, `nome`, `imagem`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Categoria', '', 'S', '2020-02-07 20:18:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-cor`
--

DROP TABLE IF EXISTS `prod-cor`;
CREATE TABLE IF NOT EXISTS `prod-cor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `hexa` varchar(7) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-cor`
--

INSERT INTO `prod-cor` (`id`, `nome`, `hexa`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cor', '#181dbe', 'S', '2020-02-07 20:34:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-gramatura`
--

DROP TABLE IF EXISTS `prod-gramatura`;
CREATE TABLE IF NOT EXISTS `prod-gramatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-gramatura`
--

INSERT INTO `prod-gramatura` (`id`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '150g', 'S', '2020-02-07 20:40:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-grupo`
--

DROP TABLE IF EXISTS `prod-grupo`;
CREATE TABLE IF NOT EXISTS `prod-grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-grupo`
--

INSERT INTO `prod-grupo` (`id`, `categoria`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 'Grupo', 'S', '2020-02-07 20:20:22', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-imagem`
--

DROP TABLE IF EXISTS `prod-imagem`;
CREATE TABLE IF NOT EXISTS `prod-imagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto` int(11) NOT NULL,
  `imagem` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto` (`produto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-item`
--

DROP TABLE IF EXISTS `prod-item`;
CREATE TABLE IF NOT EXISTS `prod-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` int(11) NOT NULL,
  `grupo` int(11) NOT NULL,
  `subgrupo` int(11) DEFAULT NULL,
  `marca` int(11) DEFAULT NULL,
  `modelo` int(11) DEFAULT NULL,
  `cor` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `tamanho` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `gramatura` varchar(191) COLLATE utf8mb4_bin DEFAULT NULL,
  `referencia` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `tags` text COLLATE utf8mb4_bin NOT NULL,
  `destaque` char(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `lancamento` char(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `oferta` char(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `desconto` decimal(10,2) DEFAULT NULL,
  `descricao` longtext COLLATE utf8mb4_bin NOT NULL,
  `altura` decimal(10,2) DEFAULT NULL,
  `largura` decimal(10,2) DEFAULT NULL,
  `profundidade` decimal(10,2) DEFAULT NULL,
  `peso` decimal(10,3) DEFAULT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria` (`categoria`),
  KEY `grupo` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-item`
--

INSERT INTO `prod-item` (`id`, `categoria`, `grupo`, `subgrupo`, `marca`, `modelo`, `cor`, `tamanho`, `gramatura`, `referencia`, `nome`, `tags`, `destaque`, `lancamento`, `valor`, `oferta`, `desconto`, `descricao`, `altura`, `largura`, `profundidade`, `peso`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 2, 2, 2, 2, 2, '1', '2', '2', '123456', 'Teste de Produto', 'teste, produto', 'N', 'N', '150.00', 'N', '0.00', 'Teste', '0.00', '0.00', '0.00', '0.000', 'S', '2020-02-07 20:41:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-mais-vendido`
--

DROP TABLE IF EXISTS `prod-mais-vendido`;
CREATE TABLE IF NOT EXISTS `prod-mais-vendido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto` int(11) NOT NULL,
  `data` timestamp NULL DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produto` (`produto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-marca`
--

DROP TABLE IF EXISTS `prod-marca`;
CREATE TABLE IF NOT EXISTS `prod-marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-marca`
--

INSERT INTO `prod-marca` (`id`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Marca', 'S', '2020-02-07 20:25:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-modelo`
--

DROP TABLE IF EXISTS `prod-modelo`;
CREATE TABLE IF NOT EXISTS `prod-modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marca` (`marca`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-modelo`
--

INSERT INTO `prod-modelo` (`id`, `marca`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 'Modelo', 'S', '2020-02-07 20:30:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-subgrupo`
--

DROP TABLE IF EXISTS `prod-subgrupo`;
CREATE TABLE IF NOT EXISTS `prod-subgrupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grupo` (`grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-subgrupo`
--

INSERT INTO `prod-subgrupo` (`id`, `grupo`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 'Subgrupo', 'S', '2020-02-07 20:21:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-tamanho`
--

DROP TABLE IF EXISTS `prod-tamanho`;
CREATE TABLE IF NOT EXISTS `prod-tamanho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `prod-tamanho`
--

INSERT INTO `prod-tamanho` (`id`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Tamanho', 'S', '2020-02-07 20:35:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prod-visitado`
--

DROP TABLE IF EXISTS `prod-visitado`;
CREATE TABLE IF NOT EXISTS `prod-visitado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto` int(11) NOT NULL,
  `data` timestamp NULL DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `dispositivo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produto` (`produto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `social-rede`
--

DROP TABLE IF EXISTS `social-rede`;
CREATE TABLE IF NOT EXISTS `social-rede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `icone` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `social-rede`
--

INSERT INTO `social-rede` (`id`, `nome`, `icone`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Facebook', 'fab fa-facebook', 'S', '2020-01-23 15:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `social-url`
--

DROP TABLE IF EXISTS `social-url`;
CREATE TABLE IF NOT EXISTS `social-url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rede` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rede` (`rede`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `social-url`
--

INSERT INTO `social-url` (`id`, `rede`, `nome`, `url`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Lins Papéis', 'https://www.facebook.com/pages/category/Advertising-Agency/Lins-Pap%C3%A9is-e-Produtos-Gr%C3%A1ficos-Ltda-627213040632302/', 'S', '2020-01-23 15:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-como-comprar`
--

DROP TABLE IF EXISTS `txt-como-comprar`;
CREATE TABLE IF NOT EXISTS `txt-como-comprar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-pagamento-frete`
--

DROP TABLE IF EXISTS `txt-pagamento-frete`;
CREATE TABLE IF NOT EXISTS `txt-pagamento-frete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-politica-entrega`
--

DROP TABLE IF EXISTS `txt-politica-entrega`;
CREATE TABLE IF NOT EXISTS `txt-politica-entrega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-politica-troca`
--

DROP TABLE IF EXISTS `txt-politica-troca`;
CREATE TABLE IF NOT EXISTS `txt-politica-troca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-privacidade-seguranca`
--

DROP TABLE IF EXISTS `txt-privacidade-seguranca`;
CREATE TABLE IF NOT EXISTS `txt-privacidade-seguranca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `txt-sobre-empresa`
--

DROP TABLE IF EXISTS `txt-sobre-empresa`;
CREATE TABLE IF NOT EXISTS `txt-sobre-empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `texto` longtext COLLATE utf8mb4_bin NOT NULL,
  `posicao` int(3) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usu-grupo-tipo`
--

DROP TABLE IF EXISTS `usu-grupo-tipo`;
CREATE TABLE IF NOT EXISTS `usu-grupo-tipo` (
  `grupo` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  KEY `tipo` (`tipo`),
  KEY `grupo` (`grupo`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `usu-grupo-tipo`
--

INSERT INTO `usu-grupo-tipo` (`grupo`, `tipo`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(2, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(3, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(5, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(7, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(8, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(10, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(11, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(14, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(15, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(16, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(9, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(13, 1, 'S', '2020-01-23 15:00:00', NULL, NULL),
(4, 1, 'S', '2020-01-23 15:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usu-permissao-tipo`
--

DROP TABLE IF EXISTS `usu-permissao-tipo`;
CREATE TABLE IF NOT EXISTS `usu-permissao-tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `modulo` int(11) NOT NULL,
  `diretorio` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `habilitado` char(1) COLLATE utf8mb4_bin NOT NULL,
  `criar` char(1) COLLATE utf8mb4_bin NOT NULL,
  `ler` char(1) COLLATE utf8mb4_bin NOT NULL,
  `alterar` char(1) COLLATE utf8mb4_bin NOT NULL,
  `apagar` char(1) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo` (`tipo`),
  KEY `modulo` (`modulo`),
  KEY `diretorio` (`diretorio`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `usu-permissao-tipo`
--

INSERT INTO `usu-permissao-tipo` (`id`, `tipo`, `modulo`, `diretorio`, `habilitado`, `criar`, `ler`, `alterar`, `apagar`) VALUES
(1, 1, 1, 'dashboard', 'S', 'S', 'S', 'S', 'S'),
(2, 1, 2, 'conf-menu', 'S', 'S', 'S', 'S', 'S'),
(3, 1, 3, 'conf-grupo-modulo', 'S', 'S', 'S', 'S', 'S'),
(4, 1, 4, 'conf-modulo', 'S', 'S', 'S', 'S', 'S'),
(5, 1, 5, 'conf-pagina-site', 'S', 'S', 'S', 'S', 'S'),
(6, 1, 6, 'cad-seo', 'S', 'S', 'S', 'S', 'S'),
(7, 1, 7, 'conf-smtp', 'S', 'S', 'S', 'S', 'S'),
(8, 1, 8, 'conf-gateway', 'S', 'S', 'S', 'S', 'S'),
(9, 1, 9, 'usu-tipo', 'S', 'S', 'S', 'S', 'S'),
(10, 1, 10, 'usu-grupo-tipo', 'S', 'S', 'S', 'S', 'S'),
(11, 1, 11, 'usu-permissao-tipo', 'S', 'S', 'S', 'S', 'S'),
(12, 1, 12, 'usu-sistema', 'S', 'S', 'S', 'S', 'S'),
(13, 1, 13, 'local-pais', 'S', 'S', 'S', 'S', 'S'),
(14, 1, 14, 'local-estado', 'S', 'S', 'S', 'S', 'S'),
(15, 1, 15, 'local-cidade', 'S', 'S', 'S', 'S', 'S'),
(16, 1, 16, 'cad-info-contato', 'S', 'S', 'S', 'S', 'S'),
(17, 1, 17, 'conf-manutencao', 'S', 'S', 'S', 'S', 'S'),
(18, 1, 18, 'social-rede', 'S', 'S', 'S', 'S', 'S'),
(19, 1, 19, 'social-url', 'S', 'S', 'S', 'S', 'S'),
(20, 1, 20, 'forma-de-pagamento', 'S', 'S', 'S', 'S', 'S'),
(21, 1, 21, 'cabecalho-da-pagina', 'S', 'S', 'S', 'S', 'S'),
(22, 1, 22, 'arq-carrossel', 'S', 'S', 'S', 'S', 'S'),
(23, 1, 23, 'txt-sobre-empresa', 'S', 'S', 'S', 'S', 'S'),
(24, 1, 24, 'txt-privacidade-seguranca', 'S', 'S', 'S', 'S', 'S'),
(25, 1, 25, 'txt-como-comprar', 'S', 'S', 'S', 'S', 'S'),
(26, 1, 26, 'txt-pagamento-frete', 'S', 'S', 'S', 'S', 'S'),
(27, 1, 27, 'txt-politica-entrega', 'S', 'S', 'S', 'S', 'S'),
(28, 1, 28, 'txt-politica-troca', 'S', 'S', 'S', 'S', 'S'),
(29, 1, 29, 'blog-categoria', 'S', 'S', 'S', 'S', 'S'),
(30, 1, 30, 'blog-post', 'S', 'S', 'S', 'S', 'S'),
(31, 1, 31, 'prod-categoria', 'S', 'S', 'S', 'S', 'S'),
(32, 1, 32, 'prod-grupo', 'S', 'S', 'S', 'S', 'S'),
(33, 1, 33, 'prod-subgrupo', 'S', 'S', 'S', 'S', 'S'),
(34, 1, 34, 'prod-marca', 'S', 'S', 'S', 'S', 'S'),
(35, 1, 35, 'prod-modelo', 'S', 'S', 'S', 'S', 'S'),
(36, 1, 36, 'prod-cor', 'S', 'S', 'S', 'S', 'S'),
(37, 1, 37, 'prod-tamanho', 'S', 'S', 'S', 'S', 'S'),
(38, 1, 38, 'prod-gramatura', 'S', 'S', 'S', 'S', 'S'),
(39, 1, 39, 'prod-item', 'S', 'S', 'S', 'S', 'S'),
(40, 1, 40, 'prod-estoque', 'S', 'S', 'S', 'S', 'S'),
(41, 1, 41, 'prod-mais-vendido', 'S', 'S', 'S', 'S', 'S'),
(42, 1, 42, 'prod-visitado', 'S', 'S', 'S', 'S', 'S'),
(43, 1, 43, 'arq-catalogo', 'S', 'S', 'S', 'S', 'S'),
(44, 1, 44, 'cli-cadastro', 'S', 'S', 'S', 'S', 'S'),
(45, 1, 45, 'ped-cadastro', 'S', 'S', 'S', 'S', 'S'),
(46, 1, 46, 'ped-retorno', 'S', 'S', 'S', 'S', 'S'),
(47, 1, 47, 'msg-mail-recebido', 'S', 'S', 'S', 'S', 'S'),
(48, 1, 48, 'msg-newsletter', 'S', 'S', 'S', 'S', 'S'),
(49, 1, 49, 'log-acesso-cms', 'S', 'S', 'S', 'S', 'S'),
(50, 1, 50, 'log-acesso-site', 'S', 'S', 'S', 'S', 'S'),
(51, 1, 51, 'log-envio-mail', 'S', 'S', 'S', 'S', 'S'),
(52, 1, 52, 'log-execucao', 'S', 'S', 'S', 'S', 'S'),
(53, 1, 53, 'ped-carrinho-abandonado', 'N', 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usu-sistema`
--

DROP TABLE IF EXISTS `usu-sistema`;
CREATE TABLE IF NOT EXISTS `usu-sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `login` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `senha` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo` (`tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `usu-sistema`
--

INSERT INTO `usu-sistema` (`id`, `tipo`, `nome`, `email`, `login`, `senha`, `token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Administrador', 'web@oxigeniocomunicacao.com.br', 'masteradmin', '31f91e7cfc58250432cb4b17f20a14eab657777d', '$2y$18$Jx3DSe2EqXytPUAZr7hmw.dXoYWWQ36l/uImBPEEUmE/h8np52Ovm', 'S', '2020-01-23 15:00:00', '2020-02-07 12:37:46', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usu-tipo`
--

DROP TABLE IF EXISTS `usu-tipo`;
CREATE TABLE IF NOT EXISTS `usu-tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `status` char(1) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Extraindo dados da tabela `usu-tipo`
--

INSERT INTO `usu-tipo` (`id`, `nome`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrador', 'S', '2020-01-23 15:00:00', NULL, NULL);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cad-info-contato`
--
ALTER TABLE `cad-info-contato`
  ADD CONSTRAINT `FK1_local` FOREIGN KEY (`local`) REFERENCES `local-cidade` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `cli-endereco`
--
ALTER TABLE `cli-endereco`
  ADD CONSTRAINT `FK1_cliente` FOREIGN KEY (`cliente`) REFERENCES `cli-cadastro` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `conf-grupo-modulo`
--
ALTER TABLE `conf-grupo-modulo`
  ADD CONSTRAINT `FK1_menu` FOREIGN KEY (`menu`) REFERENCES `conf-menu` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `conf-modulo`
--
ALTER TABLE `conf-modulo`
  ADD CONSTRAINT `FK2_grupo` FOREIGN KEY (`grupo`) REFERENCES `conf-grupo-modulo` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `local-cidade`
--
ALTER TABLE `local-cidade`
  ADD CONSTRAINT `FK1_estado` FOREIGN KEY (`estado`) REFERENCES `local-estado` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `local-estado`
--
ALTER TABLE `local-estado`
  ADD CONSTRAINT `FK1_pais` FOREIGN KEY (`pais`) REFERENCES `local-pais` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `log-acesso-cms`
--
ALTER TABLE `log-acesso-cms`
  ADD CONSTRAINT `FK1_usuario` FOREIGN KEY (`usuario`) REFERENCES `usu-sistema` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `log-execucao`
--
ALTER TABLE `log-execucao`
  ADD CONSTRAINT `FK2_usuario` FOREIGN KEY (`usuario`) REFERENCES `usu-sistema` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `ped-cadastro`
--
ALTER TABLE `ped-cadastro`
  ADD CONSTRAINT `FK1_endereco` FOREIGN KEY (`endereco`) REFERENCES `cli-endereco` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK2_cliente` FOREIGN KEY (`cliente`) REFERENCES `cli-cadastro` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-imagem`
--
ALTER TABLE `prod-imagem`
  ADD CONSTRAINT `FK1_produto` FOREIGN KEY (`produto`) REFERENCES `prod-item` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-item`
--
ALTER TABLE `prod-item`
  ADD CONSTRAINT `FK2_categoria` FOREIGN KEY (`categoria`) REFERENCES `prod-categoria` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK3_grupo` FOREIGN KEY (`grupo`) REFERENCES `prod-grupo` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-mais-vendido`
--
ALTER TABLE `prod-mais-vendido`
  ADD CONSTRAINT `FK6_produto` FOREIGN KEY (`produto`) REFERENCES `prod-item` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-modelo`
--
ALTER TABLE `prod-modelo`
  ADD CONSTRAINT `FK1_marca` FOREIGN KEY (`marca`) REFERENCES `prod-marca` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-subgrupo`
--
ALTER TABLE `prod-subgrupo`
  ADD CONSTRAINT `FK4_grupo` FOREIGN KEY (`grupo`) REFERENCES `prod-grupo` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prod-visitado`
--
ALTER TABLE `prod-visitado`
  ADD CONSTRAINT `FK5_produto` FOREIGN KEY (`produto`) REFERENCES `prod-item` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `social-url`
--
ALTER TABLE `social-url`
  ADD CONSTRAINT `FK1_rede` FOREIGN KEY (`rede`) REFERENCES `social-rede` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `usu-grupo-tipo`
--
ALTER TABLE `usu-grupo-tipo`
  ADD CONSTRAINT `FK1_grupo` FOREIGN KEY (`grupo`) REFERENCES `conf-grupo-modulo` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK1_tipo` FOREIGN KEY (`tipo`) REFERENCES `usu-tipo` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `usu-permissao-tipo`
--
ALTER TABLE `usu-permissao-tipo`
  ADD CONSTRAINT `FK2_tipo` FOREIGN KEY (`tipo`) REFERENCES `usu-tipo` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `usu-sistema`
--
ALTER TABLE `usu-sistema`
  ADD CONSTRAINT `FK3_tipo` FOREIGN KEY (`tipo`) REFERENCES `usu-tipo` (`id`) ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
