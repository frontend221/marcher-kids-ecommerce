SELECT p.`id`, 
    p.`valortotal`, 
    c.`razaosocial`, 
    COUNT(p.`id`) as quantidade, 
    (CASE WHEN p.`formapagamento` = 'boleto' THEN 'Boleto' ELSE 'Cartão Crédito' END) as formapagamento, 
    (CASE WHEN p.`status` = 'ANA' THEN 'EM ANÁLISE' ELSE 'APROVADO' END) as status, 
    p.`created_at` 
FROM `ped-cadastro` as p 
INNER JOIN `cli-cadastro` as c on c.`id` = p.`cliente` 
INNER JOIN `ped-item` as pi on pi.`pedido` = p.`id` 
GROUP BY p.`id`

