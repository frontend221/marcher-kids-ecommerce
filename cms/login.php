<?php

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	require_once ("config/cfg-path.php");
	require_once ("config/cfg-device.php");

?>

<!DOCTYPE html>
<html lang="en">
	
<head>

<title>Login</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/fonts/fontawesome-free-5.5.0-web/css/all.css">
	
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/css/login.css">
	
<link rel="shortcut icon" type="image/png" href="<?php echo URL ?>/assets/img/favicon.png">
<link rel="apple-touch-icon" type="image/png" href="<?php echo URL ?>/assets/img/favicon.png">

</head>

<body>
	
	<div class="form-body" class="container-fluid">
		
		<div class="website-logo">
			<a href="login.php">
				<div class="logo">
					<img class="logo-size" src="<?php echo URL ?>/assets/img/logo.png">
				</div>
			</a>
		</div>
		
		<div class="row">
			
			<div class="img-holder">
				<div class="bg"></div>
				<div class="info-holder"></div>
			</div>
			
			<div class="form-holder">
				<div class="form-content">
					<div class="form-items">
						
						<h3>Gestão de Conteúdo</h3>
						
						<div class="page-links">
							<a href="login.php" class="active">Login</a> <a href="recupera-senha.php">Recuperar Senha</a>
						</div>
						
						<form method="post" action="<?php echo URL ?>/funcao.php" enctype="multipart/form-data">
							<input type="hidden" name="action" value="login" required>
                            <input class="form-control" type="text" name="usuario" placeholder="Usuário" required>
                            <input class="form-control" type="password" name="senha" placeholder="Senha" required>
                            <div class="form-button">
                                <input id="submit" name="enviar" type="submit" class="ibtn" value="Entrar">
                            </div>
						</form>
											
					</div>
				</div>
			</div>
			
		</div>
		
    </div>
	
	<script src="<?php echo URL ?>/assets/js/jquery.min.js"></script>
	<script src="<?php echo URL ?>/assets/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="<?php echo URL ?>/assets/js/popper.min.js"></script>
	
</body>

</html>