<?php
	
	ob_start();
	session_name("LINSPAPEISECOMMERCEOXYGEN");
	session_start();
	
	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	require_once ("config/cfg-path.php");
	require_once ("config/cfg-database.php");
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		function validaEmail($email) {
			if(preg_match("/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/", $email)) {
				return true;
			} else {
				return false;
			}
		}
			
		if (isset($_POST["enviar"])) {
				
		$action = mysqli_real_escape_string($conn, stripslashes(trim($_POST["action"])));
		
		switch ($action) {
								
			// Login do usuário cliente	
				
			case "login":

				if(file_exists("config.php")) {
					require_once("config.php");
				} else {
					die("Erro: arquivo de segurança não encontrado.");
				}

				if(isset($_POST["usuario"]) and isset($_POST["senha"])) {

					$usuario = isset($_POST["usuario"]) ? mysqli_real_escape_string($conn, trim($_POST["usuario"])) : "";
					$senha = isset($_POST["senha"]) ? mysqli_real_escape_string($conn, trim($_POST["senha"])) : "";

					$query = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `usu-sistema` WHERE `login`='".$usuario."' AND `status`='S' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 0) {

						header("Location: " . URL . "/login/usuario");

					} else {

						$query = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `usu-sistema` WHERE `login`='".$usuario."' AND `senha`='".sha1($senha)."' AND `status`='S' AND `deleted_at` IS NULL");
						$row = mysqli_fetch_array($query);
						$total = $row["NUM_ROWS"];

						if($total == 0) {

							header("Location: " . URL . "/login/senha");

						} else {

							$query = mysqli_query($conn, "SELECT * FROM `usu-sistema` WHERE `login`='".$usuario."' AND `senha`='".sha1($senha)."' AND `status`='S' AND `deleted_at` IS NULL");

							while($ln = mysqli_fetch_array($query)) {

								$options = [
									'cost' => 18,
								];

								$_SESSION["token"] = password_hash("LINSPAPEISECOMMERCEOXYGEN", PASSWORD_BCRYPT, $options);

								$update = mysqli_query($conn, "UPDATE `usu-sistema` SET `token`='".$_SESSION["token"]."', `updated_at`=NOW() WHERE `id`='".$ln["id"]."'");

								if($update) {

									$log = mysqli_query($conn, "INSERT INTO `log-acesso-cms`(`id`, `usuario`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$ln["id"]."','".utf8_encode('Acesso ao painel de controle')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

									$query = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `usu-tipo` WHERE `id`='".$ln["tipo"]."' AND `status`='S' AND `deleted_at` IS NULL");
									$row = mysqli_fetch_array($query);
									$total = $row["NUM_ROWS"];

									if($total == 0) {

										header("Location: " . URL . "/login/config");

									} else {

										$_SESSION["id"] = $ln["id"];
										$_SESSION["tipo"] = $ln["tipo"];
										$_SESSION["nome"] = $ln["nome"];
										$_SESSION["email"] = $ln["email"];
										$_SESSION["login"] = $ln["login"];
										$_SESSION["status"] = $ln["status"];
										$_SESSION["time"] = time();

										header("Location: " . URL . "/dashboard/listar/1");

									}

								} else {

									header("Location: " . URL . "/login/erro");

								}

							}

						}

					}

				} else {

					header("Location: " . URL . "/login/campos");

				}
				
				break;
				
			// Recuperação de senha do usuário	
				
			case "recuperar":
				
				if(isset($_POST["usuario"])) {
					
					$usuario = isset($_POST["usuario"]) ?  mysqli_real_escape_string($conn, trim($_POST["usuario"])) : "";
					
					$query = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `cpfcnpj`='".$usuario."' AND `status`='S' AND `deleted_at` IS NULL");
					$linha = mysqli_fetch_array($query);

					if(mysqli_num_rows($query) == 0) {

						header("Location: " . URL . "/recuperar-senha/invalido");

					} else {
						
						$caracteres_aceitos = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456789!@#%*";
						$maximo_caracteres = strlen($caracteres_aceitos)-1;
						$senha_gerada = NULL;

						for($i = 0; $i < 19; $i++) {
							$senha_gerada .= $caracteres_aceitos{mt_rand(0,$maximo_caracteres)};
						}

						$update = mysqli_query($conn, "UPDATE `cli-cadastro` SET `senha`='".sha1($senha_gerada)."', `updated_at`=NOW() WHERE `id`='".$linha["id"]."'");

						if($update) {
							
							require_once ("config/cfg-device.php");

							$log = mysqli_query($conn, "INSERT INTO `log-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["id"]."','usu-sistema','".utf8_encode('Recuperação da senha do usuário')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");						

							$html  = "<!DOCTYPE html>";

							$html .= "<html lang='en'>";
							$html .= "<head>";
							$html .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
							$html .= "<title>Recuperação de Senha</title>";
							$html .= "</head>";

							$html .= "<body>";

								$html .= "<h4>Olá, <strong>".$linha["razaosocial"]."</strong></h4>";
								$html .= "<p>Uma nova senha foi gerada para seu usuário.</p>";
								$html .= "<p>Utilize a senha abaixo para acessar seu painel de compras da loja virtual.</p>";
								$html .= "<p>Para sua segunrança, faça a alteração da senha do seu usuário para uma senha segura e de sua autoria.</p>";
								$html .= "<p><strong>Nova senha:</strong> ".$senha_gerada."</p>";

							$html .= "</body>";

							$html .= "</html>";

							require_once("lib/class.phpmailer.php");

							$mail = new PHPMailer();

							$mail->IsSMTP();

							$mail->SMTPAuth = true;
							$mail->CharSet = "iso-8859-1";
							$mail->Host = "mail.agenciab33.com.br";
							$mail->SMTPSecure = "ssl"; 
							$mail->Port = "465";
							$mail->Username = "rafael@agenciab33.com.br";
							$mail->Password = "P@ss@r3d0!5090";
							$mail->From = "rafael@agenciab33.com.br";
							$mail->FromName = "Oxygen e-Commerce";
							$mail->IsHTML(true);
							$mail->Subject = "Teste";
							$mail->Body = $html;

							$mail->AddAddress($linha["email"], $linha["razaosocial"]);

							if(!$mail->Send()){

								//$log_smtp = mysqli_query($conn, "INSERT INTO `log-envio-mail`(`id`, `email`, `acao`, `mensagem`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["email"]."','".utf8_encode('Recuperação da senha do usuário')."','".$mail->ErrorInfo."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: Location: " . URL . "/recuperar-senha/nao-enviado");
								
							} else {
								
								//$log_smtp = mysqli_query($conn, "INSERT INTO `log-envio-mail`(`id`, `email`, `acao`, `mensagem`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["email"]."','".utf8_encode('Recuperação da senha do usuário')."','".utf8_encode('E-mail enviado com sucesso')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: " . URL . "/recuperar-senha/enviado");
								
							} 
							
						} else {

							header("Location: " . URL . "/recuperar-senha/erro");

						}
						
					}	
					
				} else {

					header("Location: " . URL . "/recuperar-senha/campos");

				}				

				break;
								
		}
		
	} else {
		header("Location: " . URL . "/login/metodo");						
	}
	
} else {
	header("Location: " . URL . "/login/metodo");						
}

?>