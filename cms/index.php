<?php

	ob_start();

	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	require_once ("seguranca.php");
	require_once ("config/cfg-path.php");

	$p = (isset($_GET["p"])) ? $_GET["p"] : "";
	$exp = explode('/', $p);
	
?>

<!DOCTYPE html>
<html>
    
<head>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Sistema de Gestão de Conteúdo</title>

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Open+Sans:300,400,600,700|Raleway:300,400,500,600,700|Roboto:300,400,500,700" rel="stylesheet">
	
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/fonts/fontawesome-free-5.11.2-web/css/all.css">

<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/css/estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/css/demo/icons.min.css" rel="stylesheet">
	
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/plugins/pace/pace.min.css">
<script src="<?php echo URL ?>/assets/plugins/pace/pace.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/css/demo/demo.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/css/dashboard.css">

<link rel="stylesheet" href="<?php echo URL ?>/assets/plugins/chosen/chosen.min.css">
<link rel="stylesheet" href="<?php echo URL ?>/assets/plugins/switchery/switchery.min.css">
	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.15.0/ui/trumbowyg.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/plugins/bootstrap-select/bootstrap-select.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
	
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/plugins/datatables/datatables.min.css"/>	
	
<link rel="stylesheet" href="<?php echo URL ?>/assets/plugins/dropzone-master/dist/dropzone.css">
	
<link rel="shortcut icon" type="image/png" href="<?php echo URL ?>/assets/img/favicon.png">
<link rel="apple-touch-icon" type="image/png" href="<?php echo URL ?>/assets/img/favicon.png">
	
</head>

<body>

	<?php require_once ("config/cfg-access.php"); ?>
	
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		
		<?php require_once ("include/inc-header.php"); ?>
		
        <div class="boxed">

            <!-- ========= -->
            <!-- CONTAINER -->
            <!-- ========= -->
			
            <div id="content-container">
				
				<?php
								
				$query = "SELECT m.*, g.`nome` AS grupo FROM `conf-modulo` AS m INNER JOIN `conf-grupo-modulo` AS g ON m.`grupo` = g.`id` WHERE g.`status`='S' AND g.`deleted_at` IS NULL AND m.`status`='S' AND m.`deleted_at` IS NULL";
				
				$exec = mysqli_query($conn, $query);
								
				if($exp[0] == "dashboard" or !isset($exp[0]) or $exp[0] == "") {
					
				$arg["diretorio"] = "dashboard";
									
				?>
				
                <div id="page-head">
                    
					<div class="pad-all text-center">
						<h3>Seja bem-vindo ao painel de gestão de conteúdo.</h3>
						<p>Através do menu lateral você poderá ter acesso aos módulos habilitados para seu usuário para a gestão dos conteúdos do seu aplicativo web.</p>
					</div>
									
				</div>
				
				<?php
						
				} else {
					
					while($breadcrumb = mysqli_fetch_array($exec)) {
						
						$arr = array();
					
						while($arg = mysqli_fetch_array($exec)) {
							
							array_push($arr, $arg["diretorio"]);
							
							if($arg["diretorio"] == $exp[0]) {
								
							?>
				
							<div id="page-head">

								<div id="page-title">
									<h1 class="page-header text-overflow"><?=$arg["descricao"];?></h1>
								</div>

								<ol class="breadcrumb">
									<li class="active">
										<a href="<?php echo URL ?>/dashboard/listar/1">
											<i class="fas fa-tachometer-alt"></i>
										</a>
									</li>
									<li class="active"><?=$arg["grupo"];?></li>
									<li class="active"><?=$arg["titulo"];?></li>
								</ol>

							</div>	
				
							<?php								
								
							}
							
							
						}
					
						if($breadcrumb["diretorio"] == $exp[0]) {							

				?>
				
				<?php
							
						}
						
					}
										
				}
				
				?>
				
                <!-- ======== -->
                <!-- CONTEÚDO -->
                <!-- ======== -->
				
                <div id="page-content">
					
					<?php
										
						// $exp[0] = modulo
					 	// $exp[1] = ação
						// $exp[2] = página
					
						/*
						 *
						 * Módulo
						 *
						 */
					
						if(isset($exp[0]) && $exp[0] == ""){
							
							// se nada é apontado como parâmetro, carrega a view do dahsboard
							require_once ("modulo/dashboard/listar.php");
							
						} else if ($exp[0] != "") {
																						
							/*
							 *
							 * Ação
							 *
							 */

							if(isset($exp[1]) && $exp[1] == ""){

								// se nada é apontado como parâmetro, carrega a view do dahsboard
								require_once ("modulo/dashboard/listar.php");
									
							} else {
									
								if(file_exists("modulo/".$exp[0]."/".$exp[1].".php")) {
										
									$exp[2] = ((isset($exp[2]) or ($exp[2] != "")) ? $exp[2] : 1);
										
									// Carrega ação corresponte ao solicitado, para o módulo específicado no parametro $exp[0]
									require_once ("modulo/".$exp[0]."/".$exp[1].".php");
											
								} else {
											
									require_once ("404.php");
											
								}
									
							}
							
						}

					?>

				</div>
				
                <!-- =========== -->
                <!-- // CONTEÚDO -->
                <!-- =========== -->

            </div>

            <!-- ============ -->
            <!-- // CONTAINER -->
            <!-- ============ -->
			
			<?php require_once ("include/inc-menu.php"); ?>
			
		</div>
    
		<?php require_once ("include/inc-footer.php"); ?>

	</div>
	
	<script src="<?php echo URL ?>/assets/js/jquery.min.js"></script>
	<script src="<?php echo URL ?>/assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo URL ?>/assets/js/nifty.min.js"></script>
    <script src="<?php echo URL ?>/assets/js/nifty-demo.min.js"></script>
	<script src="<?php echo URL ?>/assets/plugins/chosen/chosen.jquery.min.js"></script>
	<script src="<?php echo URL ?>/assets/plugins/switchery/switchery.min.js"></script>
    <script src="<?php echo URL ?>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.15.0/trumbowyg.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.15.0/langs/pt_br.min.js"></script>
	<script src="<?php echo URL ?>/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="<?php echo URL ?>/assets/js/jquery.maskedinput.js"></script>
	<script src="<?php echo URL ?>/assets/js/jquery.maskMoney.js"></script>
	<script src="<?php echo URL ?>/assets/js/bootstrap-filestyle.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
	<script src="<?php echo URL ?>/assets/js/form-component.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
	
	<script src="<?php echo URL ?>/assets/plugins/dropzone-master/dist/dropzone.js"></script>
	<script type="text/javascript" src="<?php echo URL ?>/assets/plugins/datatables/datatables.min.js"></script>
	
	<script src="<?php echo URL ?>/modulo/informacao-de-contato/jquery.receita-ws.js"></script>
	
	<script type="text/javascript">
		
		/* ============================================ */
		/* ---------- ESCONDE A DIV MENSAGEM ---------- */
		/* ============================================ */

		setTimeout(function() {
			$("#info").fadeOut("slow");
		}, 5000);

		/* ============================================ */
		/* ---------- VALIDANDO O FORMULÁRIO ---------- */
		/* ============================================ */

		$("#pesquisa").validate();
		$("#formulario").validate();
		
		/* ============================================ */
		/* ----------- MÁSCARAS NOS CAMPOS ------------ */
		/* ============================================ */
		
		$("#cep").mask("99999-999");
		$("#cnpj").mask("99.999.999/9999-99");
		$("#peso").mask("99.999");
		
		/* ============================================ */
		/* ------------- MÁSCARA DE MOEDA ------------- */
		/* ============================================ */
		
		$("#valor").maskMoney({
			allowNegative: false, 
			thousands: "", 
			decimal: ".",
			precision: 2,
			allowZero: false,
			defaultZero: false,
			affixesStay: false
		});

		$("#desconto").maskMoney({
			allowNegative: false, 
			thousands: "", 
			decimal: ".",
			precision: 2,
			allowZero: false,
			defaultZero: false,
			affixesStay: false
		});
		
		/* ============================================ */
		/* ---- MÁSCARA DE TELEFONE / NONO DÍGITO ----- */
		/* ============================================ */
	
		function mascara(o,f){
			v_obj=o
			v_fun=f
			setTimeout("execmascara()",1)
		}
			
		function execmascara(){
			v_obj.value=v_fun(v_obj.value)
		}
			
		function mtel(v){
			v=v.replace(/\D/g,"");
			v=v.replace(/^(\d{2})(\d)/g,"($1) $2");
			v=v.replace(/(\d)(\d{4})$/,"$1-$2");
			return v;
		}
			
		function id( el ){
			return document.getElementById(el);
		}
			
		window.onload = function(){
			id("telefone").onkeyup = function(){
				mascara( this, mtel );
			}
			id("celular").onkeyup = function(){
				mascara( this, mtel );
			}
			id("whatsapp").onkeyup = function(){
				mascara( this, mtel );
			}
		}
	
		/* ============================================ */
		/* ----------------- WYSIWYG ------------------ */
		/* ============================================ */
						
		$("#texto").trumbowyg({
			lang: 'pt_br'
		});
		$("#texto").closest(".trumbowyg-box").css("min-height", "600px");
		$("#texto").prev(".trumbowyg-editor").css("min-height", "600px");
												
		$("#destaque").trumbowyg({
			lang: 'pt_br'
		});
		$("#destaque").closest(".trumbowyg-box").css("min-height", "100px");
		$("#destaque").closest(".trumbowyg-box").css("max-height", "150px");
		$("#destaque").prev(".trumbowyg-editor").css("min-height", "100px");
		$("#destaque").prev(".trumbowyg-editor").css("max-height", "150px");

		$("#detalhe").trumbowyg({
			lang: 'pt_br'
		});
		$("#detalhe").closest(".trumbowyg-box").css("min-height", "100px");
		$("#detalhe").closest(".trumbowyg-box").css("max-height", "150px");
		$("#detalhe").prev(".trumbowyg-editor").css("min-height", "100px");
		$("#detalhe").prev(".trumbowyg-editor").css("max-height", "150px");

		$("#descricao").trumbowyg({
			lang: 'pt_br'
		});
		$("#descricao").closest(".trumbowyg-box").css("min-height", "450px");
		$("#descricao").prev(".trumbowyg-editor").css("min-height", "450px");
												
		$("#pergunta").trumbowyg({
			lang: 'pt_br'
		});
		$("#pergunta").closest(".trumbowyg-box").css("min-height", "100px");
		$("#pergunta").closest(".trumbowyg-box").css("max-height", "150px");
		$("#pergunta").prev(".trumbowyg-editor").css("min-height", "100px");
		$("#pergunta").prev(".trumbowyg-editor").css("max-height", "150px");
												
		$("#resposta").trumbowyg({
			lang: 'pt_br'
		});
		$("#resposta").closest(".trumbowyg-box").css("min-height", "300px");
		$("#resposta").closest(".trumbowyg-box").css("max-height", "450px");
		$("#resposta").prev(".trumbowyg-editor").css("min-height", "300px");
		$("#resposta").prev(".trumbowyg-editor").css("max-height", "450px");
												
		/* ============================================ */
		/* ----------- MOSTRA SENHA DIGITADA ---------- */
		/* ============================================ */
		
		function verSenhaSMTP() {
		var x = document.getElementById("senha");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		function verSenhaAtual() {
		var x = document.getElementById("atual");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		function verSenhaUm() {
		var x = document.getElementById("senha1");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		function verSenhaDois() {
		var x = document.getElementById("senha2");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		/* ============================================ */
		/* --- PREENCHE ENDEREÇO PELO CEP DIGITADO ---- */
		/* ============================================ */
	
		$(document).ready(function() {
		
			function limpa_formulário_cep() {
				$("#logradouro").val("");
				$("#bairro").val("");
				$("#nomecidade").val("");
				$("#nomeestado").val("");
			}
				
			$("#cep").blur(function() {
				
				var cep = $(this).val().replace(/\D/g, "");
				if (cep != "") {
						
					var validacep = /^[0-9]{8}$/;
					if(validacep.test(cep)) {
		
						$("#logradouro").val("carregando...");
						$("#bairro").val("carregando...");
						$("#nomecidade").val("carregando...");
						$("#nomeestado").val("carregando...");
		
						$.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
								
							if (!("erro" in dados)) {
								$("#logradouro").val(dados.logradouro);
								$("#bairro").val(dados.bairro);
								$("#nomecidade").val(dados.localidade);
								$("#nomeestado").val(dados.uf);
								$("#numero").focus();
							} else {
								limpa_formulário_cep();
								alert("CEP não encontrado.");
							}
							
						});
							
					} else {
						limpa_formulário_cep();
						alert("Formato de CEP inválido.");
					}
					
				} else {
					limpa_formulário_cep();
				}
				
			});
		
		});
														
		/* ============================================ */
		/* ---------- AJAX FILTERS - TABLES ----------- */
		/* ============================================ */
	
		$(document).ready(function(){
			$("#demo-input-search2").on("keyup", function() {
				var value = $(this).val().toLowerCase();
				$("#myTable tr").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				});
			});
		});
														
		/* ============================================ */
		/* ------------ HIDE INPUT FIELDS ------------- */
		/* ============================================ */
	
		$(function() {
			$("input[type='radio']").click(function() {
				if ($("#somenteimagem1").is(":checked")) {
					$("#inputtexto").hide();
				} else {
					$("#inputtexto").show();
				}
			});
		});
		
		$(function() {
			$("input[type='radio']").click(function() {
				if ($("#oferta2").is(":checked")) {
					$("#inputdesconto").hide();
				} else {
					$("#inputdesconto").show();
				}
			});
		});
		
	</script>
	
	<script>

		/* ============================================ */
		/* --------------- DATA TABLES ---------------- */
		/* ============================================ */

		$(document).ready(function() {
			$('#item').DataTable( {
				"order": [[ 0, "desc" ]],
				"language": {
					"decimal": ",",
					"thousands": ".",
					"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
				}
			});
		});
		
		/* ============================================ */
		/* ------ BUSCA DADOS A PARTIR DO CNPJ -------- */
		/* ============================================ */
	
		$(document).ready(function () {
			function fecharMensagem() {
				setTimeout(function () {
					$('#statuscnpj').html('');
					$('#cnpj').prop('disabled', false);
				}, 2000);
			}

			$('#cnpj').receitaws({
				afterRequest: function () {
					var cnpj = $('#cnpj').val();
					$('#statuscnpj').html('<div class="form-group"><label class="col-sm-2 control-label" for="cnpj"></label><div class="col-sm-8"><div class="alert alert-info">Buscando o CNPJ...</div></div>');
					$('form').find("input[type=text]").val("");
					$('#cnpj').val(cnpj);
					$('#cnpj').prop('disabled', true);
				},
				success: function (data) {
					$('#statuscnpj').html('<div class="form-group"><label class="col-sm-2 control-label" for="cnpj"></label><div class="col-sm-8"><div class="alert alert-success">CNPJ encontrado! Aguarde...</div></div>');
					fecharMensagem();
				},
				fail: function (message) {
					$('#statuscnpj').html('<div class="form-group"><label class="col-sm-2 control-label" for="cnpj"></label><div class="col-sm-8"><div class="alert alert-danger">' + message + '</div></div>');
					fecharMensagem();
				},
				notfound: function (message) {
					$('#statuscnpj').html('<div class="form-group"><label class="col-sm-2 control-label" for="cnpj"></label><div class="col-sm-8"><div class="alert alert-warning">CNPJ inexistente! Tente novamente.</div></div>');
					fecharMensagem();
				},

				fields: {
					nome: '#razaosocial',
					fantasia: '#fantasia',
					email: '#email',
					cep: '#cep',
					logradouro: '#logradouro',
					numero: '#numero',
					bairro: '#bairro',
					municipio: '#nomecidade',
					uf: '#nomeestado',

					telefone: function (data) {
						var separa = data.split('/');
						if (typeof separa[0] != 'undefined') {
							$('#telefone').val(separa[0]);
						}
					},
					
					qsa: function (data) {
						var responsaveis = [];
						$.each(data, function(i, val) {
							if (typeof val != 'undefined') {
								responsaveis[i] = val.nome
							}
						});
						$('#responsavel').val(responsaveis.join(', '));
					}
				}
			});

		});
		
		$(document).ready(function(){
			Dropzone.options.dropzoneFrom = {
				autoProcessQueue: true,
				acceptedFiles:".png, .jpg, .gif, .bmp, .jpeg",
				init: function(){
					var submitButton = document.querySelector('#submit-all');
					myDropzone = this;
					submitButton.addEventListener("click", function(){
						myDropzone.processQueue();
					});
					this.on("complete", function(){
						if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
							var _this = this;
							_this.removeAllFiles();
						}
					});
				},
			};
		});
		
	</script>
	
</body>

</html>

<?php
	
	ob_end_flush();
			
?>