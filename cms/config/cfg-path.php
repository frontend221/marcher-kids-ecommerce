<?php

	/* ------------------------------------------- */
	/* ------- DEFINE URL GERAL DO WEB SITE ------ */
	/* ------------------------------------------- */

	if ($_SERVER["HTTP_HOST"] == "localhost") {

		define("URL", 	 "http://localhost/marcherteste/cms");
		define("IMG", 	 "http://localhost/marcherteste/upload");
		define("UPLOAD", "../../upload");
		
	} else if($_SERVER["HTTP_HOST"] == "www.marcherkids.com.br"){

		define("URL", 	 "https://www.marcherkids.com.br/loja/cms");
		define("IMG", 	 "https://www.marcherkids.com.br/loja/upload");
		define("UPLOAD", "../../../upload");
		
	} else if($_SERVER["HTTP_HOST"] == "marcherkids.com.br"){

		define("URL", 	 "https://marcherkids.com.br/loja/cms");
		define("IMG", 	 "https://marcherkids.com.br/loja/upload");
		define("UPLOAD", "../../../upload");

	}

?>