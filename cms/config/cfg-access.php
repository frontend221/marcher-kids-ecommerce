<?php

	// VERIFICA DE USUÁRIO ESTÁ CONFIGURADO

	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] != "") {
		
		if(isset($_SESSION["login"]) && $_SESSION["login"] == "masteradmin") {
			
			$permissao_m = $exp[0]; // CARREGA SLUG DO MODULO
			$permissao_h = "S"; // HABILITADO?
			$permissao_c = "S"; // PERMISSÃO DE CRIAÇÃO
			$permissao_r = "S"; // PERMISSÃO DE LEITURA 
			$permissao_u = "S"; // PERMISSÃO DE EDIÇÃO
			$permissao_d = "S"; // PERMISSÃO DE EXCLUSÃO
			
		} else {

			$sql = mysqli_query($conn, "SELECT p.*, m.`diretorio` AS modulo FROM `usu-permissao-tipo` AS p INNER JOIN `modulo` AS m ON p.`diretorio` = m.`diretorio` WHERE p.`tipo`='".$_SESSION["tipo"]."' AND p.`diretorio`='".$exp[0]."' AND p.`habilitado`='S' AND m.`status`='S' AND m.`deleted_at` IS NULL");

			while($ln = mysqli_fetch_array($sql)){

				$permissao_m = $ln["modulo"]; // CARREGA SLUG DO MODULO
				$permissao_c = $ln["criar"]; // PERMISSÃO DE CRIAÇÃO
				$permissao_r = $ln["ler"]; // PERMISSÃO DE LEITURA 
				$permissao_u = $ln["alterar"]; // PERMISSÃO DE EDIÇÃO
				$permissao_d = $ln["apagar"]; // PERMISSÃO DE EXCLUSÃO

			}
			
		}

	}

?>