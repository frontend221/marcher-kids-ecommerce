<?php

ob_start();
if(session_status() == PHP_SESSION_NONE){
	session_start([
		"cookie_lifetime" => 3600,
		"read_and_close"  => true,
	]);
	session_name("MARCHERECOMMERCE");
}

ini_set("display_errors", 0);
ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ini_alter("date.timezone", "America/Sao_Paulo");

if($_SERVER["REQUEST_METHOD"] == "POST") {
	
	function validaEmail($email) {
		if(preg_match("/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/", $email)) {
			return true;
		} else {
			return false;
		}
	}
	
	function cpfcnpj($valor){
		$valor = trim($valor);
		$valor = str_replace(".", "", $valor);
		$valor = str_replace(",", "", $valor);
		$valor = str_replace("-", "", $valor);
		$valor = str_replace("/", "", $valor);
		return $valor;
	}
		
	if (isset($_POST["enviar"])) {
		
		require_once ("config/database.php");
		require_once ("config/path.php");
		
		$action = mysqli_real_escape_string($conn, stripslashes(trim($_POST["action"])));
	
		switch ($_POST["action"]) {
			
			case "ircheckout": 
				header("Location: " . PATH . "/checkout");
				break;
			
			case "finalizarcheckout":
				header("Location: " . PATH . "/finalizado");
				break;
			
			case "detalhespedido":
				$id_pedido = mysqli_real_escape_string($conn, stripslashes(trim($_POST["id_pedido"])));
				header("Location: " . PATH . "/detalhes-pedido/$id_pedido");
				break;
			
			case "buscar":

				if($_POST["pesquisar"] == "") {
					echo('<script language = "javascript"> alert("Informe seu endereço de e-mail."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$pesquisar = mysqli_real_escape_string($conn, stripslashes(trim($_POST["pesquisar"])));
					header("Location: " . PATH . "/buscar/$pesquisar");
				}

				break;

				
			// Cadastro de novo registro de newsletter	
			case "newsletter":
				
				if($_POST["email"] == "") {
					echo('<script language = "javascript"> alert("Informe seu endereço de e-mail."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				} else {
					$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
				}
				
				if(validaEmail($email)) {
					
					// Verifica se e-mail já se encontra na base e ATIVO
					$query1 = mysqli_query($conn, "SELECT `id`, `email` FROM `msg-newsletter` WHERE `email`='".$email."' AND `status`='S' AND `deleted_at` IS NULL");
					if(mysqli_num_rows($query1) == 1) {
						echo('<script language = "javascript"> alert("Este e-mail já encontra-se cadastrado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						
						// Verifica se e-mail se encontra na base, mas INATIVO
						$query2 = mysqli_query($conn, "SELECT `id`, `email` FROM `msg-newsletter` WHERE `email`='".$email."' AND `status`='N' AND `deleted_at` IS NOT NULL");
						if(mysqli_num_rows($query2) == 1) {
							$sql = mysqli_query($conn, "UPDATE `msg-newsletter` SET `origem`='F',`status`='S',`updated_at`=NOW(),`deleted_at`=NULL WHERE `email`='".$email."'");
						} else {
							$sql = mysqli_query($conn, "INSERT INTO `msg-newsletter`(`id`,`nome`,`email`,`origem`,`status`,`created_at`) VALUES (0,'".$email."','".$email."','F','S',NOW())");
						}
						
						if($sql) {
							echo('<script language = "javascript"> alert("Seu e-mail foi cadastrado com sucesso!"); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							echo('<script language = "javascript"> alert("Ocorreu um erro ao cadastrar seu e-mail."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
						
					}
					
				} else {
					echo('<script language = "javascript"> alert("Informe um endereço de e-mail válido."); </script>');
					echo('<script language = "javascript"> window.history.back(); </script>');
					exit;
				}

				break;
				
			// Cadastro de novo registro de cliente	
			case "cadastro":
				
				$idcliente = $_SESSION['usuario_id'];
                
				if ($_POST["_METHOD"]) {
					if($_POST["tipopessoa"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo de pessoa a ser cadastrado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tipopessoa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tipopessoa"])));
					}
					
					if($tipopessoa == "PF") {
						
						if($_POST["razaosocial1"] == "") {
							echo('<script language = "javascript"> alert("Informe seu nome."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial1"])));
						}

						if($_POST["nomefantasia1"] == "") {
							echo('<script language = "javascript"> alert("Informe seu sobrenome."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$nomefantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomefantasia1"])));
						}

						if($_POST["cpf"] == "") {
							echo('<script language = "javascript"> alert("Informe seu CPF."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$cpfcnpj = cpfcnpj(mysqli_real_escape_string($conn, stripslashes(trim($_POST["cpf"]))));
						}
						
					} else if($tipopessoa == "PJ") {
						
						if($_POST["razaosocial"] == "") {
							echo('<script language = "javascript"> alert("Informe sua razão social."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial"])));
						}

						if($_POST["nomefantasia"] == "") {
							echo('<script language = "javascript"> alert("Informe seu nome fantasia."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$nomefantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomefantasia"])));
						}

						if($_POST["cnpj"] == "") {
							echo('<script language = "javascript"> alert("Informe seu CNPJ."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$cpfcnpj = cpfcnpj(mysqli_real_escape_string($conn, stripslashes(trim($_POST["cnpj"]))));
						}
						
					}

					$inscricaoestadual = (mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricaoestadual"]))) == "" ? "ISENTO" : mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricaoestadual"]))));
					
					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Informe seu endereço de e-mail."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						if(preg_match("/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/", $_POST["email"])) {
							$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
						} else {
							echo('<script language = "javascript"> alert("O endereço de e-mail informado nao é válido."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
					}
									
					$telefone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["telefone"])));
					$celular = mysqli_real_escape_string($conn, stripslashes(trim($_POST["celular"])));
					$pessoacontato = mysqli_real_escape_string($conn, stripslashes(trim($_POST["pessoacontato"])));
					$senha1 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha1"])));
					$senha2 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha2"])));
					
					// Confirma senhas iguais					
					if($senha1 != null && $senha1 != '') {
						if($senha1 === $senha2) {
							
							// Atualiza cadastro								
							$sql = mysqli_query($conn, "UPDATE `cli-cadastro` SET 
								`tipopessoa`='".$tipopessoa."',
								`razaosocial`='".$razaosocial."', 
								`nomefantasia`='".$nomefantasia."', 
								`cpfcnpj`='".$cpfcnpj."', 
								`inscricaoestadual`='".$inscricaoestadual."',
								`email`='".$email."', 
								`telefone`='".$telefone."', 
								`celular`='".$celular."',
								`pessoacontato`='".$pessoacontato."',
								`senha`='".sha1($senha1)."',
								`updated_at`=NOW()
								WHERE `id`='".$idcliente."'
								");

							// Redireciona a saída							
							if($sql) {
								header("Location: " . PATH . "/minha-conta");
							} else {
								header("Location: " . PATH . "/cadastro/erro");
							}
						}		
						else {
							echo('<script language = "javascript"> alert("As senhas digitadas não são iguais."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
					} else {
						// Atualiza cadastro								
						$sql = mysqli_query($conn, "UPDATE `cli-cadastro` SET 
							`tipopessoa`='".$tipopessoa."',
							`razaosocial`='".$razaosocial."', 
							`nomefantasia`='".$nomefantasia."', 
							`cpfcnpj`='".$cpfcnpj."', 
							`inscricaoestadual`='".$inscricaoestadual."',
							`email`='".$email."', 
							`telefone`='".$telefone."', 
							`celular`='".$celular."',
							`pessoacontato`='".$pessoacontato."',
							`updated_at`=NOW()
							WHERE `id`='".$idcliente."'
							");

						if($sql) {
							header("Location: " . PATH . "/minha-conta/$idcliente");
						} else {
							header("Location: " . PATH . "/cadastro/erro");
						}
					}

				} else {

					if($_POST["tipopessoa"] == "") {
						echo('<script language = "javascript"> alert("Selecione o tipo de pessoa a ser cadastrado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$tipopessoa = mysqli_real_escape_string($conn, stripslashes(trim($_POST["tipopessoa"])));
					}
					
					if($tipopessoa == "PF") {
						
						if($_POST["razaosocial1"] == "") {
							echo('<script language = "javascript"> alert("Informe seu nome."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial1"])));
						}

						if($_POST["nomefantasia1"] == "") {
							echo('<script language = "javascript"> alert("Informe seu sobrenome."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$nomefantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomefantasia1"])));
						}

						if($_POST["cpf"] == "") {
							echo('<script language = "javascript"> alert("Informe seu CPF."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$cpfcnpj = cpfcnpj(mysqli_real_escape_string($conn, stripslashes(trim($_POST["cpf"]))));
						}
						
					} else if($tipopessoa == "PJ") {
						
						if($_POST["razaosocial"] == "") {
							echo('<script language = "javascript"> alert("Informe sua razão social."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$razaosocial = mysqli_real_escape_string($conn, stripslashes(trim($_POST["razaosocial"])));
						}

						if($_POST["nomefantasia"] == "") {
							echo('<script language = "javascript"> alert("Informe seu nome fantasia."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$nomefantasia = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nomefantasia"])));
						}

						if($_POST["cnpj"] == "") {
							echo('<script language = "javascript"> alert("Informe seu CNPJ."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						} else {
							$cpfcnpj = cpfcnpj(mysqli_real_escape_string($conn, stripslashes(trim($_POST["cnpj"]))));
						}
						
					}

					$inscricaoestadual = (mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricaoestadual"]))) == "" ? "ISENTO" : mysqli_real_escape_string($conn, stripslashes(trim($_POST["inscricaoestadual"]))));
					
					if($_POST["email"] == "") {
						echo('<script language = "javascript"> alert("Informe seu endereço de e-mail."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						if(preg_match("/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/", $_POST["email"])) {
							$email = mysqli_real_escape_string($conn, stripslashes(trim($_POST["email"])));
						} else {
							echo('<script language = "javascript"> alert("O endereço de e-mail informado nao é válido."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}
					}
									
					$telefone = mysqli_real_escape_string($conn, stripslashes(trim($_POST["telefone"])));
					$celular = mysqli_real_escape_string($conn, stripslashes(trim($_POST["celular"])));
					$pessoacontato = mysqli_real_escape_string($conn, stripslashes(trim($_POST["pessoacontato"])));
					
					if($_POST["senha1"] == "") {
						echo('<script language = "javascript"> alert("Informe uma senha."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha1 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha1"])));
					}
					
					if($_POST["senha2"] == "") {
						echo('<script language = "javascript"> alert("Por favor, confirme a senha digitada."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$senha2 = mysqli_real_escape_string($conn, stripslashes(trim($_POST["senha2"])));
					}
					
					// Confirma senhas iguais					
					if($senha1 === $senha2) {
							
						// Verifica se CPF ou CNPJ está na base e ATIVO
						$query1 = mysqli_query($conn, "SELECT `cpfcnpj` FROM `cli-cadastro` WHERE `cpfcnpj`='".$cpfcnpj."' AND `status`='S' AND `deleted_at` IS NULL");
						if(mysqli_num_rows($query1) == 0) {

							// Verifica se CPF ou CNPJ está na base e INATIVO
							$query2 = mysqli_query($conn, "SELECT `cpfcnpj` FROM `cli-cadastro` WHERE `cpfcnpj`='".$cpfcnpj."' AND `status`='N' AND `deleted_at` IS NOT NULL");
							if(mysqli_num_rows($query2) == 0) {
								// Verifica se e-mail está na base e ATIVO
								$query3 = mysqli_query($conn, "SELECT `email` FROM `cli-cadastro` WHERE `email`='' AND `status`='S' AND `deleted_at` IS NULL");
								if(mysqli_num_rows($query3) == 0) {
									
									// Insere novo cadastro								
									$sql = mysqli_query($conn, "INSERT INTO `cli-cadastro`(`id`, `tipopessoa`, `razaosocial`, `nomefantasia`, `cpfcnpj`, `inscricaoestadual`, `email`, `telefone`, `celular`, `pessoacontato`, `senha`, `status`, `created_at`) VALUES (0, '".$tipopessoa."', '".$razaosocial."', '".$nomefantasia."', '".$cpfcnpj."', '".$inscricaoestadual."', '".$email."', '".$telefone."', '".$celular."', '".$pessoacontato."', '".sha1($senha1)."','S',NOW())");
									
									// Redireciona a saída							
									if($sql) {
										header("Location: " . PATH . "/login/sucesso");
									} else {
										header("Location: " . PATH . "/cadastro/erro");
									}
									
									//echo "INSERT INTO `cli-cadastro`(`id`, `tipopessoa`, `razaosocial`, `nomefantasia`, `cpfcnpj`, `inscricaoestadual`, `email`, `telefone`, `celular`, `pessoacontato`, `senha`, `status`, `created_at`) VALUES (0, '".$tipopessoa."', '".$razaosocial."', '".$nomefantasia."', '".$cpfcnpj."', '".$inscricaoestadual."', '".$email."', '".$telefone."', '".$celular."', '".$pessoacontato."', '".sha1($senha1)."','S',NOW())";
									
								} else {
									echo('<script language = "javascript"> alert("Este e-mail já encontra-se cadastrado."); </script>');
									echo('<script language = "javascript"> window.history.back(); </script>');
									exit;
								}

							} else {
								echo('<script language = "javascript"> alert("Este CPF/CNPJ já encontra-se cadastrado."); </script>');
								echo('<script language = "javascript"> window.history.back(); </script>');
								exit;
							}
							
						} else {
							echo('<script language = "javascript"> alert("Este CPF/CNPJ já encontra-se cadastrado."); </script>');
							echo('<script language = "javascript"> window.history.back(); </script>');
							exit;
						}

					} else {
						echo('<script language = "javascript"> alert("As senhas digitadas não são iguais."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					}
				}
				
				break;
				
			// Recuperação de senha do usuário cliente	
				
			case "recuperar":
				
				if(isset($_POST["usuario"])) {
					
					$usuario = isset($_POST["usuario"]) ?  mysqli_real_escape_string($conn, trim($_POST["usuario"])) : "";
					
					$query = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `cpfcnpj`='".$usuario."' AND `status`='S' AND `deleted_at` IS NULL");
					$linha = mysqli_fetch_array($query);

					if(mysqli_num_rows($query) == 0) {

						header("Location: " . PATH . "/recuperar-senha/invalido");

					} else {
						
						$caracteres_aceitos = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456789!@#%*";
						$maximo_caracteres = strlen($caracteres_aceitos)-1;
						$senha_gerada = NULL;

						for($i = 0; $i < 19; $i++) {
							$senha_gerada .= $caracteres_aceitos{mt_rand(0,$maximo_caracteres)};
						}

						$update = mysqli_query($conn, "UPDATE `cli-cadastro` SET `senha`='".sha1($senha_gerada)."', `updated_at`=NOW() WHERE `id`='".$linha["id"]."'");

						if($update) {
							
							//require_once ("config/cfg-device.php");

							//$log = mysqli_query($conn, "INSERT INTO `log-de-execucao`(`id`, `usuario`, `tabela`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["id"]."','usuario-do-cms','".utf8_encode('Recuperação da senha do usuário')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");						

							$html  = "<!DOCTYPE html>";

							$html .= "<html lang='en'>";
							$html .= "<head>";
							$html .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
							$html .= "<title>Recuperação de Senha</title>";
							$html .= "</head>";

							$html .= "<body>";

								$html .= "<h4>Olá, <strong>".$linha["razaosocial"]."</strong></h4>";
								$html .= "<p>Uma nova senha foi gerada para seu usuário.</p>";
								$html .= "<p>Utilize a senha abaixo para acessar seu painel de compras da loja virtual.</p>";
								$html .= "<p>Para sua segunrança, faça a alteração da senha do seu usuário para uma senha segura e de sua autoria.</p>";
								$html .= "<p><strong>Nova senha:</strong> ".$senha_gerada."</p>";

							$html .= "</body>";

							$html .= "</html>";

							require_once("lib/class.phpmailer.php");

							$mail = new PHPMailer();

							$mail->IsSMTP();

							$mail->SMTPAuth = true;
							$mail->CharSet = "iso-8859-1";
							$mail->Host = "mail.agenciab33.com.br";
							$mail->SMTPSecure = "ssl"; 
							$mail->Port = "465";
							$mail->Username = "rafael@agenciab33.com.br";
							$mail->Password = "P@ss@r3d0!5090";
							$mail->From = "cms@linspapeis.com.br";
							$mail->FromName = "Lins Papéis";
							$mail->IsHTML(true);
							$mail->Subject = "Teste";
							$mail->Body = $html;

							$mail->AddAddress($linha["email"], $linha["razaosocial"]);

							if(!$mail->Send()){

								//$log_smtp = mysqli_query($conn, "INSERT INTO `log-envio-mail`(`id`, `email`, `acao`, `mensagem`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["email"]."','".utf8_encode('Recuperação da senha do usuário')."','".$mail->ErrorInfo."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: Location: " . PATH . "/recuperar-senha/nao-enviado");
								
							} else {
								
								//$log_smtp = mysqli_query($conn, "INSERT INTO `log-envio-mail`(`id`, `email`, `acao`, `mensagem`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$linha["email"]."','".utf8_encode('Recuperação da senha do usuário')."','".utf8_encode('E-mail enviado com sucesso')."','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");

								header("Location: " . PATH . "/recuperar-senha/enviado");
								
							} 
							
						} else {

							header("Location: " . PATH . "/recuperar-senha/erro");

						}
						
					}	
					
				} else {

					header("Location: " . PATH . "/recuperar-senha/campos");

				}				

				break;
				
			// Login do usuário cliente	
				
			case "login":
				
				if(isset($_POST["usuario"]) and isset($_POST["senha"])) {

					$usuario = isset($_POST["usuario"]) ? mysqli_real_escape_string($conn, trim($_POST["usuario"])) : "";
					$senha = isset($_POST["senha"]) ? mysqli_real_escape_string($conn, trim($_POST["senha"])) : "";

					$query = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `cli-cadastro` WHERE `cpfcnpj`='".$usuario."' AND `status`='S' AND `deleted_at` IS NULL");
					$row = mysqli_fetch_array($query);
					$total = $row["NUM_ROWS"];

					if($total == 0) {

						header("Location: " . PATH . "/login/usuario");

					} else {

						$query = mysqli_query($conn, "SELECT COUNT(*) AS 'NUM_ROWS' FROM `cli-cadastro` WHERE `cpfcnpj`='".$usuario."' AND `senha`='".sha1($senha)."' AND `status`='S' AND `deleted_at` IS NULL");
						$row = mysqli_fetch_array($query);
						$total = $row["NUM_ROWS"];

						if($total == 0) {

							header("Location: " . PATH . "/login/senha");

						} else {

							$query = mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `cpfcnpj`='".$usuario."' AND `senha`='".sha1($senha)."' AND `status`='S' AND `deleted_at` IS NULL");

							while($ln = mysqli_fetch_array($query)) {

								$options = [
									'cost' => 18,
								];

								$_SESSION["usuario_token"] = password_hash("MARCHERKIDS", PASSWORD_BCRYPT, $options);

								$update = mysqli_query($conn, "UPDATE `cli-cadastro` SET `token_acesso`='".$_SESSION["usuario_token"]."', `updated_at`=NOW() WHERE `id`='".$ln["id"]."'");

								if($update) {


									session_start();
									$_SESSION["usuario_id"] = $ln["id"];
									$_SESSION["usuario_razao"] = $ln["razaosocial"];
									$_SESSION["usuario_cpfcnpj"] = $ln["cpfcnpj"];
									$_SESSION["usuario_email"] = $ln["email"];
									$_SESSION["usuario_status"] = $ln["status"];
									$_SESSION["usuario_token"] = password_hash("MARCHERKIDS", PASSWORD_BCRYPT, $options);
									$_SESSION["usuario_time"] = time();

									header("Location: " . PATH . "/minha-conta");
									
									
									//echo $_SESSION["usuario_id"] . " // " . $_SESSION["usuario_razao"] . " // " . $_SESSION["usuario_cpfcnpj"] . " // " . $_SESSION["usuario_email"] . " // " . $_SESSION["usuario_token"] . " // " . $_SESSION["usuario_status"] . " // " . $_SESSION["usuario_time"];

								} else {

									header("Location: " . PATH . "/login/erro");

								}

							}

						}

					}
			
				} else {

					header("Location: " . PATH . "/login/campos");

				}				
				
				break;
			
			// Adicionar endereco usuário cliente	
				
			case "adicionar-endereco":
				$idcliente = $_SESSION['usuario_id'];

				if ($_POST["_METHOD"]) {

					$idendereco = $_POST['_METHOD'];

					if($_POST["cep"] == "") {
						echo('<script language = "javascript"> alert("Informe o CEP."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cep = $_POST["cep"];
					}
					
					if($_POST["logradouro"] == "") {
						echo('<script language = "javascript"> alert("Informe seu logradouro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$logradouro = $_POST["logradouro"];
					}

					if($_POST["numero"] == "") {
						echo('<script language = "javascript"> alert("Informe o número do logradouro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$numero = $_POST["numero"];
					}
									
					$complemento = stripslashes(trim($_POST["complemento"]));
					
					if($_POST["bairro"] == "") {
						echo('<script language = "javascript"> alert("Informe seu bairro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$bairro = stripslashes(trim($_POST["bairro"]));
					}
					
					if($_POST["cidade"] == "") {
						echo('<script language = "javascript"> alert("Informe a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cidade = $_POST["cidade"];
					}

					if($_POST["estado"] == "") {
						echo('<script language = "javascript"> alert("Informe o estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$estado = stripslashes(trim($_POST["estado"]));
					}

					$endereco_principal = $_POST["enderecoprincipal"];
				
					if($endereco_principal == "S") {
						
						$sql = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente` = '".$idcliente."' AND `principal` = 'S'");
						$total_endereco = mysqli_num_rows($sql);
						
						if($total_endereco > 0) {
							$idenderecoupdate = mysqli_fetch_array($sql);

							$sql = mysqli_query($conn, "UPDATE `cli-endereco` SET `principal`='N',`updated_at`=NOW(),`deleted_at`=NULL WHERE `id`='".$idenderecoupdate['id']."'");
						}
					}
					
					$sql = mysqli_query($conn, "UPDATE `cli-endereco` SET
						`cep`='".$cep."',
						`logradouro`='".$logradouro."',
						`numero`='".$numero."',
						`complemento`='".$complemento."',
						`bairro`='".$bairro."',
						`cidade`='".$cidade."',
						`estado`='".$estado."',
						`principal`='".$endereco_principal."',
						`updated_at`=NOW(),
						`deleted_at`=NULL 
						WHERE `id`='".$idendereco."'"
					);

				} else {

					if($_POST["cep"] == "") {
						echo('<script language = "javascript"> alert("Informe o CEP."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cep = $_POST["cep"];
					}
					
					if($_POST["logradouro"] == "") {
						echo('<script language = "javascript"> alert("Informe seu logradouro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$logradouro = $_POST["logradouro"];
					}

					if($_POST["numero"] == "") {
						echo('<script language = "javascript"> alert("Informe o número do logradouro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$numero = $_POST["numero"];
					}
									
					$complemento = stripslashes(trim($_POST["complemento"]));
					
					if($_POST["bairro"] == "") {
						echo('<script language = "javascript"> alert("Informe seu bairro."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$bairro = stripslashes(trim($_POST["bairro"]));
					}
					
					if($_POST["cidade"] == "") {
						echo('<script language = "javascript"> alert("Informe a cidade."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$cidade = $_POST["cidade"];
					}

					if($_POST["estado"] == "") {
						echo('<script language = "javascript"> alert("Informe o estado."); </script>');
						echo('<script language = "javascript"> window.history.back(); </script>');
						exit;
					} else {
						$estado = $_POST["estado"];
					}

					$endereco_principal = $_POST["enderecoprincipal"];
					
					if($endereco_principal == "S") {
						$sql = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente` = '".$idcliente."' AND `principal` = 'S'");
						
						$total_endereco = mysqli_num_rows($sql);

						if($total_endereco > 0) {
							$endereco = mysqli_fetch_array($sql);

							$idendereco = $endereco["id"];

							$sql = mysqli_query($conn, "UPDATE `cli-endereco` SET `principal`='N',`updated_at`=NOW(),`deleted_at`=NULL WHERE `id`='".$idendereco."'");
						}
					}
				
					$sql = mysqli_query($conn, "INSERT INTO `cli-endereco`(`cliente`, `cep`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `principal`, `status`, `created_at`) VALUES ('".$idcliente."', '".$cep."', '".$logradouro."', '".$numero."', '".$complemento."', '".$bairro."', '".$cidade."', '".$estado."', '".$endereco_principal."', 'S', NOW())");
					
				}
				
				header("Location: " . PATH . "/minha-conta/enderecos/$idcliente");
				
				break;
			
			case "deletar-endereco":
				
				$idendereco = $_POST['idendereco'];				
				$sql = mysqli_query($conn, "UPDATE `cli-endereco` SET `status`='N', `deleted_at`=NOW() WHERE `id`='".$idendereco."'");

				header("Location: " . PATH . "/minha-conta/enderecos/$idcliente");				
				break;
			
		}
		
	} else {
				
	}
	
} else {
	
}

?>