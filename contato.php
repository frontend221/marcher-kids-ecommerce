<section id="contato">
	<div class="container">
		<div class="row titulo">
			<h1>Entre em contato conosco</h1>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 formulario">
				<form action="<?php echo PATH ?>/funcao.php" method="post">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
								<label for="nome">Nome</label>
								<input type="text" class="form-control" name="nome" id="nome" required>
							</div>
						</div>				
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group">
								<label for="telefone">Telefone</label>
								<input type="tel" class="form-control" name="telefone" id="telefone" maxlength="15" required>
							</div>
						</div>				
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="email" class="form-control" id="email" name="email" required>
					</div>
					<div class="form-group">
						<label for="assunto">Assunto</label>
						<input type="text" class="form-control" id="assunto" name="assunto" required>
					</div>
					<div class="form-group">
						<label for="mensagem">Mensagem</label>
						<textarea class="form-control" id="mensagem" name="mensagem" required></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Enviar mensagem</button>
				</form>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 endereco">
				<h4><?php echo $info["razaosocial"]; ?></h4>
				<?php 
				$info_contato = mysqli_query($conn, "SELECT * FROM `cad-info-contato` WHERE `status`='S' AND `deleted_at` IS NULL");
				if(mysqli_num_rows($info_contato) > 0) {
				while($contato = mysqli_fetch_array($info_contato)) { 
				?>
				<address>
					<h4><?php echo $contato["cidade"]; ?></h4>
					<p>
						<?php echo ($contato["logradouro"] != "" ? $contato["logradouro"] : ""); ?> <?php echo ($contato["numero"] != "" ? "Nº. " . $contato["numero"] : ""); ?>
						<?php echo ($contato["complemento"] != "" ? "<br>" . $contato["complemento"] : ""); ?>
						<?php echo ($contato["bairro"] != "" ? "<br>" . $contato["bairro"] : ""); ?> <?php echo ($contato["cidade"] != "" ? ", " . $contato["cidade"] : ""); ?> <?php echo ($contato["estado"] != "" ? ", " . $contato["estado"] : ""); ?>
						<?php echo ($contato["cep"] != "" ? "<br>" . $contato["cep"] : ""); ?>
						<?php echo ($contato["telefone"] != "" ? "<br>" . $contato["telefone"] : ""); ?>
						<?php echo ($contato["celular"] != "" ? "<br>" . $contato["celular"] : ""); ?>
						<?php echo ($contato["whatsapp"] != "" ? "<br>" . $contato["whatsapp"] : ""); ?>
						<?php echo ($contato["urlmapa"] != "" ? "<br><a data-fancybox='' data-options='{'iframe' : {'css' : {'width' : '80%', 'height' : '80%'}}}' href='".$contato["urlmapa"]."' class='btn btn-map'>Abrir Mapa</a>" : ""); ?>
					</p>
				</address>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
</section>