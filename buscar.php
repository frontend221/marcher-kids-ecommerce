<?php

$teste = "SELECT pi.*, pimg.`imagem` FROM `prod-item` AS `pi`
INNER JOIN `prod-imagem` AS pimg ON pimg.`produto` = pi.`id`
WHERE pi.`nome` = '%".$exp[1]."%' AND pi.`deleted_at` IS NULL";

$produtos = mysqli_query($conn, "SELECT pi.*, pimg.`imagem` FROM `prod-item` AS `pi`
    INNER JOIN `prod-imagem` AS pimg ON pimg.`produto` = pi.`id`
    WHERE (lower(pi.`nome`) LIKE lower('%".$exp[1]."%') OR lower(pi.`tags`) LIKE lower('%".$exp[1]."%')) AND pi.`deleted_at` IS NULL");

    echo "<script>console.log(".json_encode($teste).");</script>";
    echo "<script>console.log(".json_encode($produtos).");</script>";


if(mysqli_num_rows($produtos) > 0) {    
?>
<section class="container">
    <div class="row">
        <?php while($prod = mysqli_fetch_array($produtos)) { ?>
            <div class="col-lg-4 col-md-4 col-sm-12 py-3">
                <a href="<?php echo PATH ?>/produto/<?php echo $prod["id"]; ?>/<?php echo slugit($prod["nome"]); ?>">
                    <img src="<?php echo IMAGE ?>/produto/<?php echo ($prod["imagem"] == "" ? "produto.jpg" : $prod["imagem"]); ?>" alt="<?php echo $prod["nome"]; ?>" class="img-fluid">
                    <div class="container">    
                        <div class="row justify-content-center">
                            <div class="col-lg-12 col-md-12 col-12">
                                <h3><?php echo $prod["nome"]; ?></h3>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12">
                                <span><h4>R$ <?php echo number_format($prod["valor"], 2, ",", "."); ?></h4></span>
                            </div>
                        </div>
                    </div>
                </a>
                
                <div class="row justify-content-center" id="botao">
                    <div class="col-lg-12 col-md-12 col-12 botao">
                        <a href="<?php echo PATH ?>/produto/<?php echo $prod["id"]; ?>/<?php echo slugit($prod["nome"]); ?>/<?php echo base64_encode($prod["referencia"]); ?>" class="btn btn-comprar">Comprar</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<?php } else { ?>

<section style="height: 50vh">
    <div class="container">
        <div class="row justify-content-center">
            <h1>Não há produtos nessa categoria</h1>
        </div>
    </div>
</section>

<?php } ?>