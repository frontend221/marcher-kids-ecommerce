<section id="blog">
	<div class="container">
		<div class="row titulo">
			<h1>Blog</h1>
		</div>
		<div class="row">

			<?php

			if(isset($exp[1]) && $exp[1] != "") {

			$blog = mysqli_query($conn, "SELECT b.*, u.`nome` AS nomeusuario FROM `blog-post` AS b INNER JOIN `usu-sistema` AS u ON b.`usuario`=u.`id` WHERE b.`id`='".$exp[1]."' AND b.`status`='S' AND b.`deleted_at` IS NULL ORDER BY b.`created_at` DESC");

			?>

			<div class="col-lg-8 col-md-8 col-sm-12 posts">
				<div class="row">
					<?php if(mysqli_num_rows($blog) == 1) { ?>
					<div class="post">
						
						<?php $post = mysqli_fetch_array($blog); ?>
						
						<img src="<?php echo IMAGE ?>/blog/<?php echo ($post["imagem"] == "" ? "default.png" : $post["imagem"]); ?>" alt="<?php echo $post["titulo"]; ?>" class="img-fluid">
						<h1><?php echo $post["titulo"]; ?></h1>
						<p><?php echo $post["texto"]; ?></p>
						<h5>
							<i class="far fa-calendar-alt fa-fw"></i> <?php echo date("d/m/Y", strtotime($post["created_at"])); ?> - Escrito por <span><?php echo $post["nomeusuario"]; ?></span> - Publicado em <?php $publicado = explode(",", $post["categoria"]); foreach($publicado as $p){ $nome_categoria = mysqli_query($conn, "SELECT * FROM `blog-categoria` WHERE `id`='".$p."' AND `status`='S' AND `deleted_at` IS NULL"); while($ln_categoria = mysqli_fetch_array($nome_categoria)) { echo "<span>" . $ln_categoria["nome"] . "</span> "; } } ?>
						</h5>
						
					</div>
					<?php } else { ?>
					
						<div class="alert alert-secondary" style="width: 100%; border-radius: 0; border: 1px solid #fff; height: 64px;">
							<p style="text-align: center !important; vertical-align: middle !important; color: #333 !important; text-transform: uppercase; font-size: 15px; line-height: 38px;">Não há publicação cadastrada com este identificador.</p>
						</div>
					
					<?php } ?>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4  col-sm-12 sidebar">
				<form method="get" action="">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Pesquisar..." aria-describedby="button-addon2">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="button-addon2">Buscar</button>
						</div>
					</div>
				</form>
				
				<?php
				
				$categorias = mysqli_query($conn, "SELECT * FROM `blog-categoria` WHERE `status`='S' AND `deleted_at` IS NULL");
				if(mysqli_num_rows($categorias) > 0) {
				
				?>
				
				<ul class="categorias">
					<?php while($cat = mysqli_fetch_array($categorias)) { ?>
					<li><a href="#"><?php echo $cat["nome"];?></a></li>
					<?php } ?>
				</ul>
				
				<?php }  ?>
				
				<?php 
												  
				$recentes = mysqli_query($conn, "SELECT b.*, u.`nome` AS nomeusuario FROM `blog-post` AS b INNER JOIN `usu-sistema` AS u ON b.`usuario`=u.`id` WHERE b.`status`='S' AND b.`deleted_at` IS NULL ORDER BY b.`created_at` DESC LIMIT 5");
				if(mysqli_num_rows($recentes) > 0) {								  
				
				?>
				
				<h4>Publicações Recentes</h4>
				
				<?php while($rec = mysqli_fetch_array($recentes)) { ?>
				<div class="media">
					<img src="<?php echo IMAGE ?>/blog/<?php echo ($rec["imagem"] == "" ? "default.png" : $rec["imagem"]); ?>" alt="<?php echo $rec["titulo"]; ?>" class="align-self-start mr-3" width="25%" height="auto">
					<div class="media-body">
						<h5 class="mt-0"><?php echo $rec["titulo"]; ?></h5>
						<p><i class="far fa-calendar-alt fa-fw"></i> <?php echo date("d/m/Y", strtotime($rec["created_at"])); ?></p>
					</div>
				</div>
				<?php } ?>
				
				<?php } ?>
				
			</div>
			
			<?php } else { ?>
			
			<div class="alert alert-secondary" style="width: 100%; border-radius: 0; border: 1px solid #fff; height: 64px;">
				<p style="text-align: center !important; vertical-align: middle !important; color: #333 !important; text-transform: uppercase; font-size: 15px; line-height: 38px;">Não há publicações cadastradas no blog do website.</p>
			</div>
			
			<?php } ?>
						
		</div>
	</div>
</section>