<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12">
				<ul>
					<li>
						<a href="<?php echo PATH ?>/privacidade-e-seguranca">Privacidade e segurança</a>
					</li>
					<li>
						<a href="<?php echo PATH ?>/politica-de-entrega">Política de entrega</a>
					</li>
					<li>
						<a href="<?php echo PATH ?>/politica-de-troca-e-devolucao">Política de troca e devolução</a>
					</li>
					<li>
						<a href="<?php echo PATH ?>/minha-conta">Minha conta</a>
					</li>
					<li>
						<a href="<?php echo PATH ?>/contato">Contato</a>
					</li>
				</ul>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 social">
				<ul>
					<li>
						<a href="#">
							<i class="fab fa-facebook-square"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fab fa-twitter-square"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<p>Marcher Kids - CNPJ 01.001.001/0001-01 - Maringá - Paraná - <a href="mailto:falecom@marcherkids.com.br">falecom@marcherkids.com.br</a></p>
		</div>
	</div>
</footer>