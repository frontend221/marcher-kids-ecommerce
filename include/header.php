<section id="logo">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-3 col-lg-3 col-sm-12">
				<a href="<?php echo PATH ?>">
					<img src="<?php echo ASSETS ?>/img/marcher-kids.png" alt="" class="img-fluid">
				</a>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
				<form action="<?php echo PATH ?>/funcao.php" method="post">
					<input type="hidden" name="action" value="buscar" required>
					<div class="input-group mb-3">
						<input type="text" class="form-control" name="pesquisar" required>
						<div class="input-group-append">
							<button name="enviar" type="submit" class="btn btn-outline-secondary">Pesquise</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-3 col-lg-3 col-sm-12">
				<ul class="customer">
					<li><a href="<?php echo PATH ?>/minha-conta"><span><i class="far fa-user fa-fw"></i></span></a></li>
					<li><a href="<?php echo PATH ?>/carrinho"><span><i class="fas fa-shopping-cart fa-fw"></i></span></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<header>
	<nav class="navbar navbar-expand-lg navbar-dark">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
			<ul class="navbar-nav">
				<?php 
					$categoria = mysqli_query($conn, "SELECT * FROM `prod-categoria` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `nome` ASC");
					
					if(mysqli_num_rows($categoria) > 0) {
				?>

				<?php while($cat = mysqli_fetch_array($categoria)) { ?>
					
					<li class="nav-item">
						<a class="nav-link border1"a href="<?php echo PATH ?>/categoria/<?php echo $cat["id"]; ?>/<?php echo slugit($cat["nome"]); ?>/1">
							<img src="<?php echo ASSETS ?>/img/<?=($cat["imagem"] == "" ? "nav-calcados.png" : "nav-".$cat["imagem"]."");?>" alt="<?=$cat["nome"];?>" class="img-fluid">
							<span><?php echo $cat["nome"] ?></span>
						</a>
					</li>
				<?php } ?>

				<?php } ?>
			</ul>
		</div>
	</nav>
</header>