<?php

	session_start();
    $id_sessao = session_id();

	if(session_status() == PHP_SESSION_NONE){
		session_start([
			"cookie_lifetime" => 3600,
			"read_and_close"  => true,
		]);
		session_name("MARCHERECOMMERCE");
	}
	
	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$_SESSION["carrinho"]["produtos"] = array();		
		
		$id_produto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["id_produto"])));
		$nome_produto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["nome_produto"])));
		$ref_produto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["ref_produto"])));
		$val_produto = mysqli_real_escape_string($conn, stripslashes(trim($_POST["val_produto"])));
		$quantidade = mysqli_real_escape_string($conn, stripslashes(trim($_POST["quantidade"])));
		$cor = ($_POST["cor"] != "" ? mysqli_real_escape_string($conn, stripslashes(trim($_POST["cor"]))) : 0);

		if(in_array($id_produto, $_SESSION["carrinho"]["produtos"])) {
			
			unset($id_produto);
			
		} else {
		
			//array_push($_SESSION["carrinho"]["produtos"], $id_produto);	
			
			if(isset($_SESSION["usuario_id"]) && $_SESSION["usuario_id"] != "") {
				
				$id_usuario = $_SESSION["usuario_id"];
				
				$sql = mysqli_query($conn, "INSERT INTO `ped-carrinho`(`id`, `id_sessao`, `id_cliente`, `id_produto`, `nome_produto`, `ref_produto`, `valor_produto`, `id_cor`,`quantidade`, `status`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$id_sessao."','".$id_usuario."','".$id_produto."','".$nome_produto."','".$ref_produto."','".$val_produto."','".$cor."','".$quantidade."','S','".$_SERVER["REMOTE_ADDR"]."','".$_SERVER["HTTP_USER_AGENT"]."',NOW())");
				
				//echo "INSERT INTO `ped-carrinho`(`id`, `id_sessao`, `id_cliente`, `id_produto`, `nome_produto`, `ref_produto`, `valor_produto`, `id_cor`, `id_gramatura`, `quantidade`, `status`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$id_sessao."','".$id_usuario."','".$id_produto."','".$nome_produto."','".$ref_produto."','".$val_produto."','".$cor."','".$gramatura."','".$quantidade."','S','".$_SERVER["REMOTE_ADDR"]."','".$_SERVER["HTTP_USER_AGENT"]."',NOW())";
				
			} else {
				
				$sql = mysqli_query($conn, "INSERT INTO `ped-carrinho`(`id`, `id_sessao`, `id_cliente`, `id_produto`, `nome_produto`, `ref_produto`, `valor_produto`, `id_cor`,`quantidade`, `status`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$id_sessao."',NULL,'".$id_produto."','".$nome_produto."','".$ref_produto."','".$val_produto."','".$cor."','".$quantidade."','S','".$_SERVER["REMOTE_ADDR"]."','".$_SERVER["HTTP_USER_AGENT"]."',NOW())");
				
				//echo "INSERT INTO `ped-carrinho`(`id`, `id_sessao`, `id_cliente`, `id_produto`, `nome_produto`, `ref_produto`, `valor_produto`, `id_cor`, `id_gramatura`, `quantidade`, `status`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$id_sessao."',NULL,'".$id_produto."','".$nome_produto."','".$ref_produto."','".$val_produto."','".$cor."','".$gramatura."','".$quantidade."','S','".$_SERVER["REMOTE_ADDR"]."','".$_SERVER["HTTP_USER_AGENT"]."',NOW())";
				
			}

		}
		
		if($sql) {
			echo "<script>window.location.href='".PATH."/carrinho';</script>";
			exit;
		}
			
	}

	//print_r($_SESSION["carrinho"]["produtos"]);

	?>

<section id="carrinho" class="my-4">
	<div class="container">
		
		<?php
		$carrinho =  mysqli_query($conn, "SELECT * FROM `ped-carrinho` WHERE `id_sessao`='".$id_sessao."' AND `status`='S' AND `deleted_at` IS NULL");
		
		$total_produtos = mysqli_num_rows($carrinho);
		?>
		
		<div class="row">
			<span class="count-carrinho">Você tem <strong><?php echo $total_produtos; ?> produto(s)</strong> em seu carrinho</span>
		</div>
		
		<div class="row">
			
			<div class="table-responsive info-carrinho">
					
				<table class="table table-condensed">
					<thead>
						<tr class="table-header">
							<td class="imagem" width="10%"></td>
							<td class="descricao" width="40%">Produto</td>
							<td class="valor" width="15%">Valor Unitário</td>
							<td class="quantidade" width="15%">Quantidade</td>
							<td class="valor" width="15%">Total</td>
							<td width="5%"></td>
						</tr>
					</thead>
					<tbody>
					
					<?php $valor_total = 0.00; ?>	
						
					<?php if($total_produtos > 0) { ?>
							
						<?php while($row = mysqli_fetch_array($carrinho)) { ?>
						
							<?php array_push($_SESSION["carrinho"]["produtos"], $row["id_produto"]); ?>	
							
							<?php $valor_total += $row["val_produto"]; ?>
							
							<tr>
								<td class="carrinho-produto" style="text-align: center;">
								<?php
								$imagem = mysqli_query($conn, "SELECT * FROM `prod-imagem` WHERE `produto`='".$row["id_produto"]."' AND `status`='S' AND `deleted_at` IS NULL ORDER BY `id` ASC");
								if(mysqli_num_rows($imagem) > 0) {
								$img = mysqli_fetch_array($imagem);	
								?>
									<img src="<?php echo IMAGE ?>/produto/<?php echo $img["imagem"]; ?>" alt="<?php echo $r["nome_produto"]; ?>" class="img-fluid">
								<?php } else { ?>
									<img src="<?php echo IMAGE ?>/produto/produto.jpg" alt="" class="img-fluid">
								<?php } ?> 
								</td>
								<td class="carrinho-descricao">
									<a href="<?php echo PATH ?>/produto/<?php echo $row["id_produto"]; ?>/<?php echo slugit($row["nome_produto"]); ?>/<?php echo base64_encode($row["ref_produto"]); ?>">
										<h4><?php echo $row["nome_produto"]; ?></h4>
										<p><strong>Ref.:</strong> <span><?php echo $row["ref_produto"]; ?></span></p>
									</a>
								</td>
								<td class="carrinho-preco">
									<p>R$ <?php echo number_format($row["valor_produto"], 2, ",", "."); ?></p>
								</td>
								<td class="carrinho-qtde">
									<input type="number" id="quantidade<?php echo $row["id_produto"]; ?>" name="quantidade" class="form-control" value="<?php echo $row["quantidade"]; ?>" onchange="recalculo(<?php echo $row["id"] ?>, <?php echo $row["valor_produto"] ?>, $(this).val())" />
								</td>
								<td class="carrinho-preco">
									<p id="preco-<?php echo $row["id"]; ?>">R$ <?php echo number_format(($row["valor_produto"] * $row["quantidade"]), 2, ",", "."); ?></p>
								</td>
								<td class="carrinho-apagar" style="text-align: center;">
									<a href="<?php echo PATH ?>/apagar.php?r=<?php echo $row["id"]; ?>&s=<?php echo $id_sessao; ?>" class="carrinho-qtde-apagar"><i class="fa fa-times"></i></a>
								</td>
							</tr>
									
							<?php } ?>
						
						<?php //print_r($_SESSION["carrinho"]["produtos"]); ?>
							
					<?php } else { ?>
					
						<tr>
							<td colspan="6" align="center" valign="middle">
								<p style="margin-top: 12px;">Seu carrinho está vazio! Selecione algum produto para continuar com a operação de compra.</p>
							</td>
						</tr>
						
					<?php } ?>
						
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">

							</td>
						</tr>
					</tfoot>				
				</table>
				
			</div>
			
			<div class="col-lg-12">
						
			</div>
						
			<div class="col-md-6 col-lg-6 col-12 botao-voltar">
				<a href="<?php echo PATH ?>" class="btn btn-secondary">Continuar comprando</a>	
			</div>
			<div class="col-md-6 col-lg-6 col-12 botao-finalizar">
				<?php if(isset($_SESSION["usuario_id"]) && $_SESSION["usuario_id"] != "") { ?>
					<a href="<?php echo PATH ?>/checkout" class="btn btn-primary">Finalizar compra</a>
				<?php } else { ?>
				<a href="<?php echo PATH ?>/login" class="btn btn-primary">Faça seu Login</a>
				<?php } ?>
			</div>
			
		</div>
	</div>
</section>


<script type="text/javascript">
	 
	$(document).ready(function () {
		$('#cep').mask('99999-999');
	});

	function recalculo(produto, preco, quantidade) {
		$.post('<?php echo PATH ?>/recalculo.php', {produto: produto, preco: preco, quantidade: quantidade}, function (retorno) {
			$('#valorsubtotal').html(retorno);
		});

		const valorProduto = (preco * quantidade).toFixed(2);

		$(`#preco-${produto}`).text(`R$ ${valorProduto.replace('.', ',')}`);
	}
	  
 </script>
