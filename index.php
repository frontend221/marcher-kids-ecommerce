<?php

ob_start();

ini_set("display_errors", 0);
ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

ini_alter("date.timezone", "America/Sao_Paulo");

include ("config/database.php");
include ("config/function.php");
include ("config/path.php");

$url = (isset($_GET["p"]) ? $_GET["p"] : "");
$exp = explode('/', $url);

if(isset($exp[0]) && $exp[0] == ""){
	$exp[0] = "home";
}

/*
$manutencao = mysqli_query($conn, "SELECT * FROM `modo-de-manutencao` WHERE `status`='S' AND `id`='1' LIMIT 1");
if(mysqli_num_rows($manutencao) == 1) {
	header("Location: ". OUT);
}

$sql_seo = mysqli_query($conn, "SELECT * FROM `seo-e-metatag` WHERE `deleted_at` IS NULL AND `id`='1' LIMIT 1");
$seo = mysqli_fetch_array($sql_seo);

$sql_info = mysqli_query($conn, "SELECT * FROM `informacao-de-contato` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `created_at` DESC LIMIT 1");
$info = mysqli_fetch_array($sql_info);
*/

?>

<!DOCTYPE html>
<html lang="en">

<head>
	
<title>MARCHER KIDS</title>
	
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!--<?//=($seo["description"] != '' ? '<meta name="description" content="'.$seo["description"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["keywords"] != '' ? '<meta name="keywords" content="'.$seo["keywords"].'">'.PHP_EOL : '');?>--> 
<meta name="author" content="Rafael Henrique B. Pereira - Oxigênio Comunicação">
<!--<?//=($seo["robot"] != '' ? '<meta name="robot" content="'.$seo["robot"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["rating"] != '' ? '<meta name="rating" content="'.$seo["rating"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["distribution"] != '' ? '<meta name="distribution" content="'.$seo["distribution"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["language"] != '' ? '<meta name="language" content="'.$seo["language"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["region"] != '' ? '<meta name="geo.region" content="'.$seo["region"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["placename"] != '' ? '<meta name="geo.placename" content="'.$seo["placename"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["position"] != '' ? '<meta name="geo.position" content="'.$seo["position"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["revisitafter"] != '' ? '<meta name="revisit-after" content="'.$seo["revisitafter"].'">'.PHP_EOL : '');?>-->

<!--<?//=($seo["ogtitle"] != '' ? '<meta property="og:title" content="'.$seo["ogtitle"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogsitename"] != '' ? '<meta property="og:site_name" content="'.$seo["ogsitename"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogurl"] != '' ? '<meta property="og:url" content="'.$seo["ogurl"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogdescription"] != '' ? '<meta property="og:description" content="'.$seo["ogdescription"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["oglocale"] != '' ? '<meta property="og:locale" content="'.$seo["oglocale"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["oglocalealternate"] != '' ? '<meta property="og:locale:alternate" content="'.$seo["oglocalealternate"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogimage"] != '' ? '<meta property="og:image" content="'.$seo["ogurl"].'/upload/seo/'.$seo["ogimage"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogimagetype"] != '' ? '<meta property="og:image:type" content="'.$seo["ogimagetype"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogimagewidth"] != '' ? '<meta property="og:image:width" content="'.$seo["ogimagewidth"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogimageheight"] != '' ? '<meta property="og:image:height" content="'.$seo["ogimageheight"].'">'.PHP_EOL : '');?>-->
<!--<?//=($seo["ogtype"] != '' ? '<meta property="og:type" content="'.$seo["ogtype"].'">'.PHP_EOL : '');?>-->
	
<link rel="stylesheet" href="<?php echo ASSETS ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo ASSETS ?>/fonts/fontawesome/css/all.css">
<link rel="stylesheet" href="<?php echo ASSETS ?>/css/estilo.css">
<link rel="stylesheet" href="<?php echo ASSETS ?>/css/responsivo.css">
	
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
	
<link rel="stylesheet" href="<?php echo ASSETS ?>/plugin/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo ASSETS ?>/plugin/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="icon" href="<?php echo ASSETS ?>/img/favicon.png">

</head>
<body>
	
	<?php require_once("include/header.php"); ?>
		
	<?php

		/*
		$arquivo = mysqli_query($conn, "SELECT `nome`,`arquivo`,`extensao` FROM `pagina-do-website` WHERE `status`='S' AND `deleted_at` IS NULL");
		if(mysqli_num_rows($arquivo) > 0) {
			$paginas = array();
			while($ln = mysqli_fetch_array($arquivo)) {
				array_push($paginas, $ln["arquivo"]);
			}
		} else {
			$paginas = array("home");
		}
		*/
	
		$paginas = array("adicionar-endereco", "buscar", "cadastro", "calcular-frete", "carrinho", "categoria", "checkout", "contato", "detalhes-pedido", "home", "login", "minha-conta", "politica-de-entrega", "politica-de-troca-e-devolucao", "privacidade-e-seguranca", "produto", "produtos-grupo", "recalculo", "recuperar-senha", "selecionar-endereco");
	  
	  	if(isset($exp[0]) && $exp[0] == ""){
			include ("home.php");
		} else if ($exp[0] != "" && is_string($exp[0])) {
			if(isset($exp[0]) && in_array($exp[0], $paginas)){
				include ($exp[0].".php");
			} else {
				include ("home.php");
			}
		}
		
	?>
		
	<?php require_once("include/footer.php"); ?>

	<?php
	
	if(isset($exp[0]) && $exp[0] == "metodo"){
			
		if(isset($exp[1]) && $exp[1] == "enviar") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer o envio do seu formul�rio. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "request") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer o envio do seu formul�rio. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		}
			
	} else if(isset($exp[0]) && $exp[0] == "newsletter"){
			
		if(isset($exp[1]) && $exp[1] == "sucesso") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Obrigado!","Seu cadastro em nossa newsletter foi feita com sucesso.","success");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "erro") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer o seu cadastro em nossa newsletter. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "email") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","O endere�o de e-mail informado para o cadastro n�o � um endere�o de e-mail v�lido.","warning");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "campos") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Preencha o campo com seu e-mail para poder se cadastrar em nossa newsletter.","warning");';
			echo '}, 100);</script>';
		}
			
	} else if(isset($exp[0]) && $exp[0] == "contato"){
			
		if(isset($exp[1]) && $exp[1] == "sucesso") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Obrigado!","Sua mensagem foi enviada com sucesso! Aguarde nosso retorno.","success");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "erro") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer o seu cadastro em nossa newsletter. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "email") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","O endere�o de e-mail informado para o cadastro n�o � um endere�o de e-mail v�lido.","warning");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "campos") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Para poder enviar sua mensagem, voc� deve preencher os campos marcados como obrigat�rios.","warning");';
			echo '}, 100);</script>';
		}
			
	} else if(isset($exp[0]) && $exp[0] == "cadastro"){
			
		if(isset($exp[1]) && $exp[1] == "sucesso") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Obrigado!","Seu cadastro foi efetuado com sucesso. Fa�a seu login para ter acesso ao seu painel do cliente.","success");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "erro") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer o seu cadastro. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "email") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","O endere�o de e-mail informado para o cadastro n�o � um endere�o de e-mail v�lido.","warning");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "campos") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Para poder fazer o seu cadastro, voc� deve preencher os campos marcados como obrigat�rios.","warning");';
			echo '}, 100);</script>';
		}
			
	} else if(isset($exp[0]) && $exp[0] == "login"){
		
		if(isset($exp[1]) && $exp[1] == "sucesso") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Obrigado!","Seu cadastro foi efetuado com sucesso. Fa�a seu login para ter acesso ao seu painel do cliente.","success");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "usuario") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Usu�rio incorreto.","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "senha") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!", "Senha incorreta.","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "autenticar") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Voc� precisa efetuar seu login para ter acesso ao painel do cliente.","warning");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "campos") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Preencha seu usu�rio e senha para poder ter acesso ao painel do cliente.","warning");';
			echo '}, 100);</script>';
		}
			
	} else if(isset($exp[0]) && $exp[0] == "recuperar-senha"){
			
		if(isset($exp[1]) && $exp[1] == "sucesso") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Parab�ns!","Uma mensagem contendo seu novo c�digo de acesso acaba de ser enviada para seu caixa de e-mail. Verifique e fa�a seu login.","success");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "erro") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Ops!","Ocorreu um erro ao fazer a recupera��o do seu novo c�digo de acesso. Por favor, tente novamente!","error");';
			echo '}, 100);</script>';
		} else if(isset($exp[1]) && $exp[1] == "campos") {
			echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Aten��o!","Voc� deve informar seu e-mail, CPF ou CNPJ para poder receber o novo c�digo de acesso ao painel do cliente.","warning");';
			echo '}, 100);</script>';
		}
			
	}
	
	?>

	<script src="<?php echo ASSETS ?>/js/jquery-3.4.1.min.js"></script>
	<script src="<?php echo ASSETS ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo ASSETS ?>/js/popper.min.js"></script>
	<script src="<?php echo ASSETS ?>/js/tooltip.min.js"></script>
	<script src="<?php echo ASSETS ?>/js/jquery.mask.min.js"></script>
	
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	
	<script src="<?php echo ASSETS ?>/plugin/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
	<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

	
	<script type="text/javascript">
		
		var behavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		options = {
			onKeyPress: function (val, e, field, options) {
				field.mask(behavior.apply({}, arguments), options);
			}
		};
		$('#telefone').mask(behavior, options);
		$('#celular').mask(behavior, options);
		
	</script>

	<script>
		
		$('.owl-carousel').owlCarousel({
			loop: true,
			margin: 15,
			nav: true,
			navText:  ["<div class='nav-btn prev-slide'><i class='fas fa-angle-double-left'></i></div>", "<div class='nav-btn next-slide'><i class='fas fa-angle-double-right'></i></div>"],
			responsive:{
				0:{
					items: 2
				},
				600:{
					items: 3
				},
				1000:{
					items: 4
				}
			}
		});
		
		$(".ddd").on("click", function () {
			var $button = $(this);
			var oldValue = $button.closest('.sp-quantity').find("input.quantity-input").val();
			if ($button.text() == "+") {
				var newVal = parseFloat(oldValue) + 1;
			} else {
				if (oldValue > 1) {
					var newVal = parseFloat(oldValue) - 1;
				} else {
					newVal = 1;
				}
			}
			$button.closest('.sp-quantity').find("input.quantity-input").val(newVal);
		});
			
		$(document).ready(function(){
			$('#qty_input').prop('disabled', true);
			$('#plus-btn').click(function(){
				$('#qty_input').val(parseInt($('#qty_input').val()) + 1 );
			});
			$('#minus-btn').click(function(){
				$('#qty_input').val(parseInt($('#qty_input').val()) - 1 );
				if ($('#qty_input').val() == 0) {
					$('#qty_input').val(1);
				}
			});
		 });
		
		$(function() {
			$("input[type='radio']").click(function() {
				if ($("#pessoafisica").is(":checked")) {
					$("#inputpessoafisica").show();
					$("#inputpessoajuridica").hide();
				} else {
					$("#inputpessoafisica").hide();
					$("#inputpessoajuridica").show();
				}
			});
		});
		
		$("#cpf").mask("999.999.999-99");
		$("#cnpj").mask("99.999.999/9999-99");
		$("#cep").mask('99999-999');
		
	</script>

</body>
</html>