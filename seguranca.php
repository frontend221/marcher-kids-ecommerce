<?php

	ob_start();
	session_name("MARCHERECOMMERCE");
	session_start();
	
	ini_set("display_errors", 0);
	ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_alter("date.timezone", "America/Sao_Paulo");

	require_once ("config/path.php");

	if(file_exists("config.php")) {
		require_once ("config.php");
	} else {
		die("Erro: arquivo de setup não encontrado.");
	}
	
	if(strpos(strtolower($_SERVER["REQUEST_URI"]), "PHPSESSID") !== false) {

		session_destroy();	
    	session_regenerate_id();
		ob_start();
    	session_name("MARCHERECOMMERCE");
    	session_start();
	
	}
	
	if(!(isset($_SESSION["id"]) and isset($_SESSION["razaosocial"]) and isset($_SESSION["cpfcnpj"]) and isset($_SESSION["email"]) and isset($_SESSION["token_acesso"]) and isset($_SESSION["status"]) and isset($_SESSION["time"]))) {
		
		session_destroy();
		header("Location: " . PATH . "/login/autenticar"); 
		  	
	} else {
	
		$time_expire = 7200;
    	$time = $_SESSION["time"];
    	$time_new = time();
    	$time_off = $time_new - $time;
			
		if($time_off > $time_expire) {
		
			ob_start();
			session_name("MARCHERECOMMERCE");
			session_start();
						
			//$query = mysqli_query($conn, "INSERT INTO `log-acesso-cms`(`id`, `usuario`, `acao`, `ip`, `dispositivo`, `created_at`) VALUES (0,'".$_SESSION["id"]."','Expirado por tempo de inatividade','".$_SERVER["REMOTE_ADDR"]."','".Dispositivo($_SERVER["HTTP_USER_AGENT"])."',NOW())");
			
			session_unset();
			session_destroy();

			header("Location: " . PATH . "/login/autenticar");
			
		} else {
		
			$id = $_SESSION["id"];
	  		$razaosocial = $_SESSION["razaosocial"];
	  		$cpfcnpj = $_SESSION["cpfcnpj"];
	  		$email = $_SESSION["email"];
	  		$token_acesso = $_SESSION["token_acesso"];
			$status = $_SESSION["status"];
	  		$_SESSION["time"] = time();
	
		}
	
	}

	ob_end_flush();
  
?>