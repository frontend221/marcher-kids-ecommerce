<section id="catalogo">
	<div class="container">
		<div class="row titulo">
			<h1>Nosso Catálogos</h1>
		</div>
		<div class="row">

			<?php 
			
			$catalogo = mysqli_query($conn, "SELECT `id`, `titulo`, `capa`, `arquivo`, `texto` FROM `arq-catalogo` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `posicao` ASC"); 
			if(mysqli_num_rows($catalogo) > 0) {
			
			?>
			
			<?php while($row = mysqli_fetch_array($catalogo)) { ?>
			
			<div class="col-lg-3 col-md-3 col-12 item">
				<a href="<?php echo IMAGE ?>/catalogo/arquivo/<?php echo $row["arquivo"]; ?>" data-fancybox data-caption="<?=$row["titulo"];?>">
					<img src="<?php echo IMAGE ?>/catalogo/capa/<?php echo ($row["capa"] == "" ? "default.png" : $row["capa"]); ?>" alt="<?php echo $row["titulo"]; ?>" class="img-fluid">
					<h2><?php echo $row["titulo"]; ?></h2>
				</a>
			</div>
			
			<?php } ?>
			
			<?php } else { ?>
			
			<div class="alert alert-secondary" style="width: 100%; border-radius: 0; border: 1px solid #fff; height: 64px;">
				<p style="text-align: center !important; vertical-align: middle !important; color: #333 !important; text-transform: uppercase; font-size: 15px; line-height: 38px;">Não há conteúdo publicado para está seção do website.</p>
			</div>
			
			<?php } ?>

		</div>
	</div>
</section>