<?php
    include ("config/database.php");
    include ("funcao.php");
    require('vendor/autoload.php');

    $id_sessao = session_id();
  
    $pagarme = new PagarMe\Client('ak_test_WoLbpgy7s0r4HC75eVDaz2ffbdswJ7');

    $data = $_POST['data'];
    $valor = $_POST['valor'];

    $transactions = $pagarme->transactions()->get([
        'id' => $data['token'], 
    ]);

    $capturedTransaction = $pagarme->transactions()->capture([
        'id' => $data['token'],
        'amount' => $transactions->authorized_amount,
    ]);


    // if ($transactions->payment_method === 'boleto') {
    //     $transactionPaymentNotify = $pagarme->transactions()->collectPayment([
    //         'id' => $transactions->id,
    //         'email' => $transactions->customer->email,
    //     ]);
    // }    

    $id_usuario = $_SESSION["usuario_id"];
    $endereco = $_SESSION["endereco"];
    $id_endereco = $endereco['id'];
    $opcao_frete = $_SESSION["opcao_frete"];
    
    $data = json_encode((array)$capturedTransaction);
    
    $status='ANA';
    if ($capturedTransaction->payment_method === 'credit_card')
        $status='CON';

    $valorTotal = intval($capturedTransaction->authorized_amount) / 100;
    $valorFrete = intval($capturedTransaction->shipping->fee) / 100;

    if (!mysqli_query($conn, "INSERT INTO `ped-cadastro`(`id`, `cliente`, `endereco`, `valortotal`, `opcaofrete`, `valorfrete`, `formapagamento`, `quantidadeparcelas`, `urlboleto`, `codebarboleto`, `codrastreamentofrete`, `notafiscal`, `visualizado`, `status`, `created_at`) VALUES (0,'".$id_usuario."','".$id_endereco."','".$valorTotal."','".$opcao_frete."','".$valorFrete."','".$capturedTransaction->payment_method."','".$capturedTransaction->installments."','".$capturedTransaction->boleto_url."','".$capturedTransaction->boleto_barcode."','','',0,'".$status."', NOW())")) {
        echo("Error description: " . mysqli_error($conn));
    } else {
        $idPedido = mysqli_insert_id($conn);
        $carrinho =  mysqli_query($conn, "SELECT * FROM `ped-carrinho` WHERE `id_sessao`='".$id_sessao."' AND `status`='S' AND `deleted_at` IS NULL");
        $total_produtos = mysqli_num_rows($carrinho);
        
        while($row = mysqli_fetch_array($carrinho)) {
            mysqli_query($conn, "INSERT INTO `ped-item`(`id`, `status`, `produto`, `pedido`, `quantidade`, `valor_unitario`, `created_at`) VALUES (0,'S','".$row['id_produto']."','".$idPedido."', '".$row['quantidade']."', '".$row['valor_produto']."', NOW())");
        }

        $del = mysqli_query($conn, "UPDATE `ped-carrinho` SET `status`='N', `deleted_at`=NOW() WHERE `id_sessao`='".$id_sessao."'");
        unset($_SESSION["carrinhoTotal"]);
        unset($_SESSION["carrinho"]);
        unset($_SESSION["endereco_id"]);
        unset($_SESSION["opcao_frete"]);
        unset($_SESSION["frete"]);
        unset($_SESSION["endereco"]);
        echo $data;
    }
    
    
?>