<?php

ob_start();

if(session_status() == PHP_SESSION_NONE){
	session_start([
		"cookie_lifetime" => 3600,
		"read_and_close"  => true,
	]);
	session_name("LINSECOMMERCE");
}

ini_set("display_errors", 0);
ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ini_alter("date.timezone", "America/Sao_Paulo");

if(isset($exp[0]) && $exp[0] == "minha-conta") { 

	if(file_exists("config.php")) {
		require_once ("config.php");
	} else {
		die("Erro: arquivo de setup não encontrado.");
	}
		
?>

<section id="minha-conta" class="my-5">
	<div class="container">

	<?php
	
		//echo json_encode($_SESSION);
			
		/*
	
		if(file_exists("config.php")) {
			require_once ("config.php");
		} else {
			die("Erro: arquivo de setup não encontrado.");
		}
		
		*/

		
		if(!(isset($_SESSION["usuario_id"]) and isset($_SESSION["usuario_razao"]) and isset($_SESSION["usuario_cpfcnpj"]) and isset($_SESSION["usuario_email"]) and isset($_SESSION["usuario_token"]) and isset($_SESSION["usuario_status"]) and isset($_SESSION["usuario_time"]))) {

			session_destroy();
			header("Location: " . PATH . "/login/autenticar");
						
		} else {
			$time_expire = 7200;
			$time = $_SESSION["usuario_time"];
			$time_new = time();
			$time_off = $time_new - $time;

			if($time_off > $time_expire) {

				session_name('LINSECOMMERCE');
				session_start();
				session_unset();
				session_destroy();

				header("Location: " . PATH . "/login/autenticar");

			} else {
				
				$id = $_SESSION["usuario_id"];
				$razaosocial = $_SESSION["usuario_razao"];
				$cpfcnpj = $_SESSION["usuario_cpfcnpj"];
				$email = $_SESSION["usuario_email"];
				$token_acesso = $_SESSION["usuario_token"];
				$status = $_SESSION["usuario_status"];
													  
	?>
		
	<?php if(isset($exp[1]) && $exp[1] != "") { $exp[1] = $exp[1]; } else {$exp[1] = "pedidos"; } ?>
		
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<div class="menu">
					<ul>
						<li <?=($exp[1] == "pedidos" ? "class='active'" : "");?>><a href="<?php echo PATH ?>/minha-conta/pedidos/1">Meus pedidos</a></li>
						<li <?=($exp[1] == "dados" ? "class='active'" : "");?>><a href="<?php echo PATH ?>/minha-conta/dados/1">Meus dados</li>
						<li <?=($exp[1] == "enderecos" ? "class='active'" : "");?>><a href="<?php echo PATH ?>/minha-conta/enderecos/1">Meus endereços</li>
						<li <?=($exp[1] == "troca-devolucao" ? "class='active'" : "");?>><a href="<?php echo PATH ?>/minha-conta/troca-devolucao/1">Trocas e devoluções</li>
						<li><a href="<?php echo PATH ?>/logout.php?id=<?php echo $_SESSION["usuario_id"]; ?>">Sair</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<div class="">
					
					<?php if(isset($exp[1]) && $exp[1] == 'pedidos') { ?>
					
						<h1>Meus pedidos</h1>
						
						<?php 
							$sql = mysqli_query($conn, "SELECT 
								`p`.`id`,
								`p`.`valortotal`,
								`p`.`opcaofrete`,
								`p`.`valorfrete`,
								`p`.`formapagamento`,
								`p`.`quantidadeparcelas`,
								`p`.`urlboleto`,
								`p`.`codebarboleto`,
								`p`.`status`,
								`e`.`cep`,
								`e`.`logradouro`,
								`e`.`numero`,
								`e`.`complemento`,
								`e`.`bairro`,
								`e`.`cidade`,
								`e`.`estado`,
								`p`.`created_at`
							FROM 
								`ped-cadastro` as `p` 
							JOIN `cli-endereco` as `e` ON (`p`.endereco = `e`.id) 
							WHERE `p`.`cliente`='".$id."' AND `p`.`deleted_at` IS NULL ORDER BY `p`.`id` desc");

							if(mysqli_num_rows($sql) > 0) {
						?>
							<section id="meus-dados" class="my-4">
								<div class="container">
									<div class="row">
										<?php while($pedido = mysqli_fetch_array($sql)) { ?>
											<div class="col-12 lista">
												<div class="card" style="min-height: 300px">
													<div class="card-body">
														<h1 class="card-title" style="text-align: center">PEDIDO #<?php echo $pedido['id']?>, <?php echo date_format(date_create($pedido['created_at']), 'd/m/Y H:i:s');?></h1>
														<div class="row">
															<ul style="list-style: circle">
																<li><strong>Forma de Pagamento: </strong><?php echo $pedido['formapagamento'] ?></li>
																<li><strong>Parcelas: </strong><?php echo $pedido['quantidadeparcelas'] ?></li>
																<li><strong>Valor: </strong>R$ <?php echo number_format(($pedido['valortotal']), 2, ",", "."); ?></li>
																<li><strong>Frete: </strong><?php echo $pedido['opcaofrete'] ?></li>
																<li><strong>Valor frete: </strong>R$ <?php echo number_format(($pedido['valorfrete']), 2, ",", "."); ?></li>
															</ul>

															<ul style="list-style: circle">
																<li><strong>CEP: </strong><?php echo $pedido['cep'] ?></li>
																<li><strong>Cidade: </strong><?php echo $pedido['cidade'] ?>, <?php echo $pedido['estado'] ?></li>
																<li><strong>Endereço Principal: </strong><?php echo $pedido['principal'] ?></li>
																<li><strong>Bairro: </strong><?php echo $pedido['bairro'] ?></li>
																<li><strong>Complemento: </strong><?php echo $pedido['complemento'] ?></li>
															</ul>
														</div>
													</div>
													<div class="row justify-content-center">
														<div class="col-md-2 my-2">
															<form action="<?php echo PATH ?>/funcao.php" method="post">
																<input type="hidden" name="action" value="detalhespedido" required>
																<input type="hidden" name="id_pedido" value="<?php echo $pedido['id']?>" required>
																<button type="submit" name="enviar" class="btn btn-primary">DETALHES</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
							</section>
						<?php } else { ?>
							<section id="meus-dados" class="my-4">
								<div class="container">
									<h3>Sem Pedidos</h3>
								</div>
							</section>
						<?php } ?>

					
					<?php } else if (isset($exp[1]) && $exp[1] == 'dados') { ?>
					
						<h1>Meus dados</h1>

						<?php
							$sql =  mysqli_query($conn, "SELECT * FROM `cli-cadastro` WHERE `id`='".$id."' AND `status`='S' AND `deleted_at` IS NULL");
							
							$dados = mysqli_fetch_array($sql);
						?>

						<section id="meus-dados" class="my-4">
							<div class="card lista">
								<div class="card-body">
									<div class="container">
										<div class="row">
											<div class="col-12">
												<h3><?=($dados["tipopessoa"] == "PF" ? "".$dados["razaosocial"]." ".$dados["nomefantasia"]."" : $dados["razaosocial"]); ?></h3>
											</div>
											<div class="col-6">
												<span class="valor">
													<h4><?=($dados["tipopessoa"] == "PF" ? "CPF: ". $dados["cpfcnpj"]."" : "CNPJ: ". $dados["cpfcnpj"].""); ?></h4>
												</span>
											</div>
											<div class="col-6">
												<h4>Telefone: <?php echo $dados["telefone"]; ?></h4>
											</div>
											<div class="col-6">
												<h4>Email: <?php echo $dados["email"]; ?></h4>
											</div>
											<div class="col-6">
												<h4>Celular: <?php echo $dados["celular"]; ?></h4>
											</div>
										</div>
										<div class="row justify-content-end align-items-end">
											<div class="col-3">
												<a href="<?php echo PATH ?>/cadastro/<?php echo $dados["id"]; ?>" class="btn btn-comprar">Editar</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

					<?php } else if (isset($exp[1]) && $exp[1] == 'enderecos') { ?>
					
						<h1>Meus endereços</h1>

						<?php 
							$sql = mysqli_query($conn, "SELECT * FROM `cli-endereco` WHERE `cliente`='".$id."' AND `status`='S' AND `deleted_at` IS NULL");
							if(mysqli_num_rows($sql) > 0) {
						?>
							<section id="meus-dados" class="my-4">
								<div class="container lista">
									<div class="row">
										<?php while($endereco = mysqli_fetch_array($sql)) { ?>
											<div class="col-12 col-sm-6">
												<div class="card" style="min-height: 300px">
													<div class="card-body">
														<h1 class="card-title" style="text-align: center"><?php echo $endereco['logradouro']?>, <?php echo $endereco['numero']?></h1>
														<ul style="list-style: circle">
															<li><strong>CEP: </strong><?php echo $endereco['cep'] ?></li>
															<li><strong>Cidade: </strong><?php echo $endereco['cidade'] ?>, <?php echo $endereco['estado'] ?></li>
															<li><strong>Endereço Principal: </strong><?php echo $endereco['principal'] ?></li>
															<li><strong>Bairro: </strong><?php echo $endereco['bairro'] ?></li>
															<li><strong>Complemento: </strong><?php echo $endereco['complemento'] ?></li>
														</ul>
														<div class="row justify-content-center align-items-end">
															<a href="<?php echo PATH ?>/adicionar-endereco/<?php echo $endereco["id"]; ?>" class="btn btn-primary mx-1">
																<i class="fas fa-edit"></i>
																<span>Editar</span>
															</a>
															<form action="<?php echo PATH ?>/funcao.php" method="POST">
																<input type="hidden" name="action" value="deletar-endereco" required>
																<input type="hidden" name="idendereco" value="<?php echo $endereco['id']; ?>" required>
																<button type="submit" name="enviar" class="btn btn-danger mx-1">
																	<i class="fas fa-trash"></i>
																	<span>Excluir</span>
																</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
									<div class="row justify-content-end align-items-end my-3">
										<div class="col-3">
											<a href="<?php echo PATH ?>/adicionar-endereco" class="btn btn-comprar">Adicionar</a>
										</div>
									</div>
								</div>
							</section>

						<?php } else { ?>
							<section id="meus-dados" class="my-4">
								<div class="container">
									<h3>Sem Endereços Cadastrados</h3>
									<div class="row justify-content-end align-items-end">
										<div class="col-3">
											<a href="<?php echo PATH ?>/adicionar-endereco" class="btn btn-comprar">Adicionar</a>
										</div>
										
									</div>
								</div>
							</section>
						<?php } ?>

					<?php } else if (isset($exp[1]) && $exp[1] == 'troca-devolucao') { ?>
					
					<h1>Trocas e devoluções</h1>
					
					<?php } ?>
 				
				</div>
			</div>
		</div>
		
	<?php
		
			}

		}
		
	?>	
		
	</div>
</section>

<?php } ?>

<?php ob_end_flush(); ?>