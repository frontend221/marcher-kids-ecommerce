<?php 
	$categoria = mysqli_query($conn, "SELECT * FROM `prod-categoria` WHERE `status`='S' AND `deleted_at` IS NULL ORDER BY `nome` ASC");
	if(mysqli_num_rows($categoria) > 0) {
?>
	<section id="banner">
		<?php while($cat = mysqli_fetch_array($categoria)) { ?>
			<div class="imagem">
				<img src="<?php echo IMAGE ?>/banner/<?=($cat["imagem"] == "" ? "calcados.png" : $cat["imagem"]);?>" alt="<?=$cat["nome"];?>" class="img-fluid">
				<div class="banner-text">
					<h2><span class="right"><?php echo $cat["nome"] ?></span></h2>
					<h4><span class="right"><a href="<?php echo PATH ?>/categoria/<?php echo $cat["id"]; ?>/<?php echo slugit($cat["nome"]); ?>/1">Ver mais</a></span></h4>
				</div>
			</div>
		<?php } ?>
	</section>

<?php } ?>

<!-- <section id="ofertas">
	<div class="container">
		<div class="row">
			<div class="titulo">
				<h1>Ofertas</h1>
			</div>	
		</div>
		<div class="row">

			<div class="owl-carousel owl-theme">
				<?php for($i = 1; $i < 9; $i++) { ?>
				<div class="item">
					<img src="<?php echo IMAGE ?>/produto/<?=$i;?>.png" alt="" class="img-fluid">
					<span class="selo"><p>-10%</p></span>
					<h4 class="marca">Marca</h4>
					<h3 class="produto">Nome do Produto</h3>
					<h2 class="valor">R$ 54,90</h2>
				</div>
				<?php } ?>
			</div>
			
		</div>
	</div>
</section> -->

<section id="newsletter">
	<div class="container">
		<div class="row">
			<h1>Saiba nossas novidades!</h1>
			<form method="post" action="<?php echo PATH ?>/funcao">
				<input type="hidden" name="action" value="newsletter" required>
				<div class="form-group">
					<input type="email" class="form-control" name="email" required>
				</div>
				<button type="submit" name="enviar" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
	</div>
</section>