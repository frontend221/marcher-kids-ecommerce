<?php if(isset($exp[1]) && $exp[1] != "") { ?>

    <?php 

    $sql = mysqli_query($conn, "SELECT p.*, c.`id` AS idcategoria, c.`nome` AS nomecategoria FROM `prod-item` AS p INNER JOIN `prod-categoria` AS c ON p.`categoria`=c.`id` WHERE p.`id`='".$exp[1]."' AND p.`status`='S' AND p.`deleted_at` IS NULL");
    if(mysqli_num_rows($sql) == 1) {
        $prod = mysqli_fetch_array($sql);
        
        mysqli_query($conn, "INSERT INTO `prod-visitado`(`id`, `produto`, `data`, `ip`, `dispositivo`) VALUES (0,'".$prod["id"]."',NOW(),'".$_SERVER["REMOTE_ADDR"]."','".$_SERVER["HTTP_USER_AGENT"]."')");

    ?>

    <section id="item">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo PATH ?>/categoria/<?php echo $prod["idcategoria"]; ?>/<?php echo slugit($prod["nomecategoria"]); ?>/1"><?php echo $prod["nomecategoria"]; ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $prod["nome"]; ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                
                <form method="post" action="<?php echo PATH ?>/carrinho">
                                    
                    <div class="col-lg-12 col-md-12 col-12 itens">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-6">
                                <?php
                                $imagem = mysqli_query($conn, "SELECT * FROM `prod-imagem` WHERE `produto`='".$prod["id"]."' AND `status`='S' AND `deleted_at` IS NULL");
                                if(mysqli_num_rows($imagem) > 0) {
                                ?>
                                <ul id="imageGallery">
                                    <?php while($img = mysqli_fetch_array($imagem)) { ?>
                                    <li data-thumb="<?php echo IMAGE ?>/produto/<?php echo $img["imagem"]; ?>" data-src="<?php echo IMAGE ?>/produto/<?php echo $img["imagem"]; ?>">
                                        <img src="<?php echo IMAGE ?>/produto/<?php echo $img["imagem"]; ?>" class="img-fluid" alt="<?php echo $prod["nome"]; ?>">
                                    </li>
                                    <?php } ?>
                                </ul>
                                <?php
                                } else {
                                ?>
                                <img src="<?php echo IMAGE ?>/produto/default.png" alt="<?php echo $prod["nome"]; ?>" class="img-fluid">						
                                <?php
                                }
                                ?>					
                            </div>
                                                
                            <div class="col-lg-6 col-md-6 col-6 data">
                                                        
                                <h3><?php echo $prod["nome"]; ?></h3>
                                <h4>Código do produto: <?php echo $prod["referencia"]; ?></h4>

                                <h2>Valor</h2>
                                <?php if($prod["oferta"] == "S") { ?>
                                <span class="valor">
                                    <h4>R$ <?php echo number_format($prod["valor"], 2, ",", "."); ?></h4> <h3>R$ <?php echo number_format($prod["valor"] - ($prod["valor"] / 100 * $prod["desconto"]), 2, ",", "."); ?></h3>
                                </span>
                                <?php } else { ?>
                                <span class="valor">
                                    <h3>R$ <?php echo number_format($prod["valor"], 2, ",", "."); ?></h3>
                                </span>
                                <?php } ?>

                                <!-- seletor de quantidade -->
                                <h2>Quantidade</h2>
                                <div class="sp-quantity">
                                    <div class="sp-minus fff">
                                        <a class="ddd" href="#">-</a>
                                    </div>
                                    <div class="sp-input">
                                        <input type="text" class="quantity-input" name="quantidade" min="1" value="1" size="3" required>
                                    </div>
                                    <div class="sp-plus fff">
                                        <a class="ddd" href="#">+</a>
                                    </div>
                                </div>

                                <!-- seletor de cor -->
                                <h2>Cores</h2>
                                <div class="colors">
                                    <ul>
                                        <?php
                                        $cor = explode(",",$prod["cor"]);
                                        foreach ($cor as $c) {
                                            $cores = mysqli_query($conn, "SELECT * FROM `prod-cor` WHERE `id`='".$c."' AND `status`='S' AND `deleted_at` IS NULL ORDER BY `nome` ASC");
                                            if(mysqli_num_rows($cores) > 0) {
                                                while($prod_cor = mysqli_fetch_array($cores)) { 
                                        ?>
                                        <li>
                                            <label>
                                                <input type="radio" name="cor" value="<?php echo $prod_cor["id"]; ?>">
                                                <span class="swatch" style="background-color:<?php echo $prod_cor["hexa"]; ?>"></span> <?php echo $prod_cor["nome"]; ?>
                                            </label>
                                        </li>
                                        <?php 
                                                } 
                                            } else {
                                                echo "<li>O produto não possui categorização por cor.</li>";
                                            }
                                        }				
                                        ?>
                                    </ul> 
                                </div>

                                <!-- seletor de gramatura -->
                                <!-- <h2>Gramaturas</h2>
                                <div class="grammage">
                                    <ul>
                                        <?php
                                        $gramatura = explode(",", $prod["gramatura"]);
                                        foreach($gramatura as $g) {
                                            $gramaturas = mysqli_query($conn, "SELECT * FROM `prod-gramatura` WHERE `id`='".$g."' AND `status`='S' AND `deleted_at` IS NULL");
                                            if(mysqli_num_rows($gramaturas) > 0) {
                                                while($prod_gm = mysqli_fetch_array($gramaturas)) { 
                                        ?>
                                        <li>
                                            <input class="form-check-input" type="radio" name="gramatura" id="gramatura" value="<?php echo $prod_gm["id"]; ?>">
                                            <label class="form-check-label" for="gramatura"><?php echo $prod_gm["nome"]; ?></label>
                                        </li>						
                                        <?php 
                                                } 
                                            } else {
                                                echo "<p>O produto não possui categorização por gramatura.</p>";
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div> -->
                                
                                <input type="hidden" name="id_produto" value="<?php echo $prod["id"]; ?>" required>
                                <input type="hidden" name="nome_produto" value="<?php echo $prod["nome"]; ?>" required>
                                <input type="hidden" name="ref_produto" value="<?php echo $prod["referencia"]; ?>" required>
                                <input type="hidden" name="val_produto" value="<?php echo $prod["valor"]; ?>" required>
                                
                                <div class="botoes">
                                    <button type="submit" class="btn btn-comprar"><i class="fas fa-cart-plus"></i> Adicionar ao Carrinho</button>
                                </div>
                                
                            </div>

                            <?php if(isset($prod["descricao"]) && $prod["descricao"] != "") {  ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12 desc">
                                    <h3>Descrição</h3>
                                    <?php echo $prod["descricao"]; ?>
                                </div>
                            </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                    
                </form>

            </div>
        </div>
    </section>

    <?php } else { ?>

    <section id="produtos">
        <div class="container">
            <div class="row">
                <h1>Não há produtos nessa categoria</h1>
            </div>
        </div>
    </section>

    <?php } ?>

    <?php } else { ?>

    <section id="produtos">
        <div class="container">
            <div class="row">
                <h1>Não há produtos nessa categoria</h1>
            </div>
        </div>
    </section>

<?php } ?>
