<?php

	function Dispositivo($agent) {

		if(strpos($agent, "Opera") || strpos($agent, "OPR/")) {
	  		return "Opera";
		} else if(strpos($agent, "Edge")) {
	  		return "Edge";
    	} else if(strpos($agent, "Chrome")) {
	  		return "Chrome";
    	} else if(strpos($agent, "Safari")) {
	  		return "Safari";
    	} else if(strpos($agent, "Firefox")) {
	  		return "Firefox";
    	} else if(strpos($agent, "MSIE") || strpos($agent, "Trident/7")) {
	  		return "Internet Explorer";
		} else if(strpos($agente, "iPhone")) {
			return "iPhone";
		} else if(strpos($agent, "iPad")) {
			return "iPad";
		} else if(strpos($agent, "Android")) {
			return "Android";
		} else if(strpos($agent, "webOS")) {
			return "webOS";
		} else if(strpos($agent, "BlackBerry")) {
			return "BlackBerry";
		} else if(strpos($agent, "iPod")) {
			return "iPod";
		} else if(strpos($agent, "Symbian")) {  
			return "Symbian";
		} else {
      		return "Outro";
		}

  	}

?>