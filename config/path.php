<?php

	if ($_SERVER["HTTP_HOST"] == "localhost") {

		define("PATH",		"http://localhost/marcherteste");
		define("ASSETS",	"http://localhost/marcherteste/assets");
		define("IMAGE",		"http://localhost/marcherteste/upload");
		define("OUT",		"http://localhost/marcherteste/manutencao");
		
	} else if($_SERVER["HTTP_HOST"] == "marcherkids.com.br"){

		define("PATH",		"https://marcherkids.com.br/loja");
		define("ASSETS",	"https://marcherkids.com.br/loja/assets");
		define("IMAGE",		"https://marcherkids.com.br/loja/upload");
		define("OUT",		"https://marcherkids.com.br/loja/manutencao");
		
	} else if($_SERVER["HTTP_HOST"] == "www.marcherkids.com.br"){

		define("PATH",		"https://www.marcherkids.com.br/loja");
		define("ASSETS",	"https://www.marcherkids.com.br/loja/assets");
		define("IMAGE",		"https://www.marcherkids.com.br/loja/upload");
		define("OUT",		"https://www.marcherkids.com.br/loja/manutencao");

	}

?>